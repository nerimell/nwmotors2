var print;
var $window = $(window);
var body = $('body');
Actions.init();
function csrf(token) {
    csrf_token = token;
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': token }
    });
}
csrf(csrf_token);
if(!tzd) { setTz(); }
lazyLoad();
var css = ['/css/libs/jquery-confirm.min.css'];
for(var i = 0; i < css.length; i++) {
    defer(css[i]);
}


