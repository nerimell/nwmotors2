(function($) {
    var defaults = defaults = {
        cls: ''
    };

    $.fn.combobox = function(options) {
        this.each(function() {
            options = $.extend({}, defaults, options || {});
            var el = $(this);
            var selected = el[0].options[el[0].selectedIndex];
            var selectedText = selected.innerHTML.trim();
            var selectOptions = el.find('option');
            var ul = $('<ul></ul>');
            selectOptions.each(function() {
                var cls = (this.innerHTML.indexOf(selectedText) != -1) ? '' : 'hidden-option';
                $option = $('<li class="option '+cls+'" data-value="'+this.value+'">'+this.innerHTML+'</li>');
                $option.on('click', function() {
                    var wrapper = $(this).closest('.combobox-wrapper');
                    cbwrapper.find('.option').removeClass('active');
                    this.classList.add('active');
                    wrapper.find('.combobox-input').val(this.innerHTML);
                    wrapper.trigger('close');
                });
                ul.append($option);
            });
            var cbwrapper = $('<div class="combobox-wrapper"></div>');
            cbwrapper.on('change', function() {
                var val = $(this).find('.option.active');
                if(!val.length) { return; }
                val = val.attr('data-value');
                if(el.val() != val) {
                    el.val(val).trigger('change');
                }
            });
            cbwrapper.on('close', function() {
                this.classList.remove('active');
                cbwrapper.trigger('change');
                body.off('click.combobox');
            });
            cbwrapper.on('open', function() {
                this.classList.add('active');
                body.on('click.combobox', function(e) {
                var th = $(e.target);
                if(th.closest('.combobox-wrapper').length) {
                    return;
                }
                cbwrapper.trigger('close');
                });
            });
            var input = $('<input type="text" class="combobox-input '+options.cls+'" value="'+selected.innerHTML+'">');
            input.on('input', function() {
                var val = this.value;
                var ul = $(this).parent().find('ul');
                ul.find('li').each(function() {
                    (this.innerHTML.toLowerCase().indexOf(val.toLowerCase()) != -1) ? this.classList.remove('hidden-option') : this.classList.add('hidden-option');
                });
                cbwrapper.trigger('open');
            });
            var roll = function(val) {
                var selected = 0;
                ul.find('li').each(function() {
                    if(this.innerHTML.toLowerCase() == val.toLowerCase()) {
                        selected = 1;
                        input.val(this.innerHTML);
                        ul.find('li').each(function() {
                            this.classList.remove('active');
                        });
                        this.classList.add('active');
                        cbwrapper.trigger('change');
                        return false;
                    }
                });
                if(!selected) {
                    input.val(el[0].options[el[0].selectedIndex].innerHTML);
                }
            };
            input.on('blur', function() {
                roll(this.value);
            });
            input.on('keypress', function(e) {
                if(e.keyCode != 13) { return; }
                roll(this.value);
            });
            var toggler = $('<i class="mdi mdi-menu-down combobox-toggler"></i>');
            toggler.on('click', function() {
                var parent = $(this).parent();
                if(parent.hasClass('active')) {
                    parent.trigger('close');
                } else {
                    parent.trigger('open');
                }
            });
            cbwrapper.append(toggler);
            cbwrapper.append(ul);
            cbwrapper.append(input);
            el.after(cbwrapper);
            el.hide();
        });
    };

    $.fn.combobox.defaults = defaults;
})(window.jQuery);