<?php
namespace App\Helpers;

class Result {

    public $result = 0;
    public $display = '';
    public $action;
    public $token;

    function __construct() {
        $this->token = csrf_token();
    }
}