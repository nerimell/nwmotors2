<?php

namespace App\Helpers;
use Carbon\Carbon;

class DateFormatter {

  public static $now;
  public static $preorderingDate;

  /*
   *  timezone offset constant
   */

  static function reformatDate($inc, $at = false, $showYear = false) {
    self::getDate();
    if(!$inc) { return ''; }
    $date = @Carbon::createFromFormat('Y-m-d H:i:s', $inc)->addMinutes(tz);
    if(!$date) { return ''; }
    $outDay = '';
    $outYear = '';
    $outMonth = '';
    $incA = ($date->day < 10) ? '0' . $date->day : $date->day;
    $incB = (self::$now->day < 10) ? '0' . self::$now->day : self::$now->day;
    $dayA = $date->year.$date->month.$incA;
    $dayB = self::$now->year.self::$now->month.$incB;
    if($dayA == $dayB) {
        $at = $at ? 'в' : '';
        $outDay = 'Сегодня' .' '.$at.' '. self::getTimeFromDate($date);
    }

    if(($dayB-$dayA) === 1) {
        $at = $at ? 'в' : '';
        $outDay = 'Вчера' .' '.$at.' '. self::getTimeFromDate($date);
    }
    if(!$outDay) {
      if($date->year !== self::$now->year) {
        $outYear = $date->year;
        $outDay = $date->day;
        $outMonth = self::switchMonth($date->month);
      } else {
       $outDay = $date->day;
         $outMonth = self::switchMonth($date->month);
      }
    }
    if($showYear) { $outYear = $date->year; }

    return $outDay . ' ' . $outMonth . ' ' . $outYear;
  }

  static function datePlus($inc, $hours) {
      $date = @Carbon::createFromFormat('Y-m-d H:i:s', $inc)->addMinutes(tz);
      if(!$date) { return ''; }
      $date->addMinutes($hours*60);
      $result = self::reformatDate($date);
      $result .= ' '.$date->hour. ':'.$date->minute;
      return $result;
  }

  static function getPreorderingDate() {
      self::getDate();
      if(self::$preorderingDate) { return self::$preorderingDate; }
      $offerDate = Carbon::now()->addDays(18);
      self::$preorderingDate = self::reformatDate($offerDate, 0, 1);
      return self::$preorderingDate;
  }


  static function getDate() {
        if(self::$now) { return; }
        self::$now = Carbon::now()->addMinutes(tz);
  }


  static function getTimeFromDate($date) {
    $hour = ($date->hour < 10) ? '0'.$date->hour : $date->hour;
    $minute = ($date->minute < 10) ? '0'.$date->minute : $date->minute;
    return $hour . ':' . $minute;
  }

  static function switchMonth($m) {
    switch($m) {
      case 1: $m = 'января'; return $m;
      case 2: $m = 'февраля'; return $m;
      case 3: $m = 'марта'; return $m;
      case 4: $m = 'апреля'; return $m;
      case 5: $m = 'мая'; return $m;
      case 6: $m = 'июня'; return $m;
      case 7: $m = 'июля'; return $m;
      case 8: $m = 'августа'; return $m;
      case 9: $m = 'сентября'; return $m;
      case 10: $m = 'октября'; return $m;
      case 11: $m = 'ноября'; return $m;
      case 12: $m = 'декабря'; return $m;
    }

    return $m;
  }

}
