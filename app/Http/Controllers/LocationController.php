<?php

namespace App\Http\Controllers;

class LocationController extends FrontController {

    function change() {
        $result = new \Result;
        $result->result = 1;
        $result->cityId = \Html::$city->city_id;
        $result->cityName = \Html::$city->title;
        if(logged) {
            \DB::table('users')->where('id', logged)->update(['city' => \Html::$city->city_id]);
        }
        return \json_encode($result);
    }
}