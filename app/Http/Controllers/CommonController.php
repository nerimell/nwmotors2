<?php

namespace App\Http\Controllers;
use App\Models\ProductsModel;
use App\Models\ArticlesModel;
use App\Models\PagesModel;

class CommonController extends FrontController {

    function __construct() {
        $this->model = ProductsModel::getInstance();
    }

    function whatever($path) {
        // ссылка может быть какой-угодно /whatever/whatever-else/what-ever-even-else
        $page = $this->shopCategoryPage($path); if($page) { return $page; } // попробуем получить категорию магазина, т.к это наиболее вероятно
        $page = $this->staticPage($path); if($page) { return $page; } // попробуем получить статическую страницу
        abort(404);
    }

    function shopCategoryPage($path) {
        $shopCategory = $this->model->getProductCategoryByPath($path);
        if(!$shopCategory) {
            $shopCategory = $this->model->getProductCategoryByOldPath($path);
            if($shopCategory) { return redirect()->to($shopCategory->link); }
        }
        if(!$shopCategory) { return 0; }
        $this->page($shopCategory->category);
        $template = ($shopCategory->category->template && is_file('../resources/views/shop/category/'.$shopCategory->category->template.'.blade.php')) ? $shopCategory->category->template : 'default';
       // print_r($shopCategory->products); die;

        return $this->view('shop.category.'.$template, [
                'category' => $shopCategory->category,
                'products' => $shopCategory->products,
                'breadcrumbs' => @$shopCategory->category->breadcrumbs,
                'total' => $shopCategory->total,
                'perPage' => $shopCategory->perPage,
                'params' => $shopCategory->params,
                'activeCategories' => $shopCategory->activeCategories
        ])->render();
    }

    function articleCategory($path) {

    }

    function articlePage($path) {
        return 0;
    }

    function staticPage($path) {
        $pagesModel = new PagesModel;
        $page = $pagesModel->getPageByPath($path);
        if(!$page) { return 0; }
        $this->page($page->page);
        return $this->view('pages.default', [
            'breadcrumbs' => @$page->breadcrumbs,
            'activeLinks' => $page->activePages,
            'page' => $page->page
        ])->render();

    }


    function timezone() {
        $tz = formatTimezoneOffset(\Request::get('tz'));
        timezone($tz);
        return $tz;
    }


}
