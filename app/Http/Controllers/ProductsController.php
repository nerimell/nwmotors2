<?php
namespace App\Http\Controllers;
use App\Models\ProductsModel;
use App\Models\ShopModel;
use App\Models\DeliveryModel;

class ProductsController extends FrontController {

    function __construct() {
        $this->model = ProductsModel::getInstance();
    }

    function productPage($path) {
        $product = $this->model->getProductByPath($path);

        if(!$product) { return $this->redirectProduct($path); }
        $this->model->getProductConsistance();
        $this->page($this->model->product);
        $breadcrumbs = (count(@$this->model->categories) == 1) ? $this->model->categories[0]->breadcrumbs : [];
        if(!empty($breadcrumbs)) { $breadcrumbs[] = (object)['title' => $this->model->product->title, 'link' => false]; }
        $view = $this->model->product->template ? ((is_file('../resources/views/shop/product/'.$this->model->product->template.'.blade.php')) ? $this->model->product->template : 'default') : 'default';
        $cats = [];
        if(!empty($this->model->categories)) {
           foreach($this->model->categories as $x) {
               $cats[] = $x->category_id;
           }
        }
        return $this->view('shop.product.'.$view, [
            'product' => $this->model->product,
            'categories' => $this->model->categories,
            'breadcrumbs' => $breadcrumbs,
            'manufacturer' => $this->model->manufacturer,
            'partOfAnother' => $this->model->partOfAnother,
            'specificationTranslations' => $this->model->getSpecificationTranslations(),
            'specials' => ShopModel::getSpecials($this->model->product->product_id, $cats),
            'activeCategories' => $this->model->activeCategories
        ]);

    }

    function redirectProduct($path) {

        $link = \DB::table('products')
        ->where('products.old_link', $path)
        ->join('links', 'products.link_id', '=', 'links.id')
        ->first();
        if($link) { return redirect()->to($link->link.'-detail' ); }
        abort(404);
    }

    function search() {
        $products = $this->model->search();
        $metaTitle = (!empty($products->params['q'])) ? 'Поиск - ' .$products->params['q'] : 'Поиск ';
        $this->page((object)['meta_title' => $metaTitle]);
        return $this->view('shop.search', [
            'products' => $products->products,
            'params' => $products->params,
            'breadcrumbs' => $products->breadcrumbs,
            'perPage' => $products->perPage,
            'total' => $products->total
        ]);
    }
}
