<?php

namespace App\Http\Controllers;
use App\Models\PaymentsModel;

class PaymentController extends FrontController {

    function __construct() {
        $this->model = new PaymentsModel;
    }

    function getPaymentUrl() {
        return \json_encode($this->model->getPaymentUrl());
    }

    function error() {
        return $this->view('payment.error');
    }

    function payoutDone() {
        file_put_contents('../resources/dev/asd3.txt', \json_encode(\Request::all()));
        $view = view('api.yandex.payout')->render();
        return response($view, 200)->header('Content-Type', 'application/xml');
    }

    function transfer() {
        file_put_contents('../resources/dev/asd2.txt', \json_encode(\Request::all()));
        dd('dsa');
    }

    function payout() {
        file_put_contents('../resources/dev/asd.txt', \json_encode(\Request::all()));
        dd('dsa');
    }



}