<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\SettingsAdminModel;
class SettingsController extends BackController {


    function __construct() {
        $this->model = new SettingsAdminModel;
    }

    function settingsPage() {
        $this->page();
        $settings = $this->model->getSiteData();
        return view('admin.settings.index', compact('settings'));
    }
}