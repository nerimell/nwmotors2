<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\SettingsModel;

class BackController extends Controller {

    function page($arg = false) {
        if($arg) { $this->pageInfo($arg); }
        (new SettingsModel)->shareSiteData($this->pageInfo);
    }
}