<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\UsersAdminModel;

class UsersController extends BackController {

    public $model;

    function __construct() {
        $this->model = new UsersAdminModel;
    }

    function loginPage() {
        $this->page();
        return view('admin.users.login');
    }

    function asyncLogin() {
        return \json_encode($this->model->login());
    }
}