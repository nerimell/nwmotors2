<?php

namespace App\Http\Controllers;

use App\Models\PaymentsModel;
use App\Models\Deprecated\CategoriesModel;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;
use App\Models\UsersModel;
use App\Models\Schedules\Vkontakte;
use App\Models\Admin\SmmModel;
use App\Models\ProductsModel;
use App\Models\Deprecated\DevModel;
use App\Models\DeliveryModel;


// Быдлокод и одноразовые функции используемые при переносе,
class DevController Extends FrontController {

    public $catalogCategory = 444;
    public $rootPtr = '24554328031233';
    public $attempts = 0;
    public $error = 0;
    public $except = [
        169472,
        71550,
    ];



    function __construct() {
        define('debug', 1);
    }



    function aliasCategories() {
        $cats = \DB::table('outsource_ezpartscatalog_nodes')->whereNull('ignore')->whereNull('alias')->get();
        foreach($cats as $k => $v) {
            $this->attempts = 0;
            if(!$this->update($v)) {
                dd($this->error. $this->attempts);
            }
        }
    }


    function saveMenuToDev() {
        $cm = CategoriesModel::getInstance();
        $tree = $cm->getCategoryTree();
        $menu = [];
        foreach($tree as $k => $v) {
            if($v->category_id > 444) { continue; }
            $menu[] = $v;
        }
        foreach($menu as $k => $v) {
            if(empty($menu[$k]->childs)) { continue; }
            foreach($menu[$k]->childs as $child_k => $v) {
                unset($menu[$k]->childs[$child_k]->childs);
            }
        }
        file_put_contents('../resources/dev/devmenu.json', \json_encode($menu));
    }

    function dev() {
        $p = \DB::table('products')->whereNotNull('gifts')->get();
        foreach($p as $k => $v) {
            $gifts = \json_decode($v->gifts);
            if(empty($gifts)) { return; }
            if(in_array(13381, $gifts->products)) {
                foreach($gifts->products as $gk => $gv) {
                    if($gv == 13381) {
                        $gifts->products[$gk] = 734562;
                        \DB::table('products')->where('product_id', $v->product_id)->update(['gifts' => \json_encode($gifts)]);
                    }
                }
            }
        }
        die;
       return view('dev');

    }



    function handleCity($v) {
        $id = 0;
        foreach($v as $cityId => $title) {
            if($cityId > 0) { continue; }
            $id = $cityId;
        }
        if(!$id) { return; }
        $city = $v->{$id};
        preg_match('/\((.*?)\)/', $city, $matches);
        if(!empty($matches)) {
            $city = str_replace($matches[0], '', $city);
            $city = trim($city);
        }
        $currentCity = \DB::table('cities')->where('title', $city)->first();
        if(!$currentCity) { return; }
        foreach($v as $cityId => $title) {
            $city = $title;
            preg_match('/\((.*?)\)/', $city, $matches);
            if(!empty($matches)) {
                $city = str_replace($matches[0], '', $city);
                $city = trim($city);
            }
            \DB::table('cities')->where('title', $city)->where('region_id', $currentCity->region_id)
            ->update(['external_pecom_id' => $cityId]);
        }


    }




    function aliasCatTable() {
        $parts = \DB::table('categories')->whereNotNull('ptr')->get();
        foreach($parts as $k => $v) {
            $this->attempts = 0;
            if(!$this->update($v)) {
                dd($this->error. $this->attempts);
            }
        }
    }

    function truncate($v) {
        $position = strpos($v, ':');
        if(!$position) { return $v; }
        $v = substr($v, 0, $position);
        return $this->truncate($v);
    }

    function linkUnnecessary() {
        $unlinked = \DB::table('outsource_ezpartscatalog_nodes')->whereNull('parent_ptr')->get();
        if(empty($unlinked)) { return; }
        $res = [];
        foreach($unlinked as $x) {
            $res[] = $x->ptr;
        }
        $unlinked = $res;
        $parts = \DB::table('parser_ezpartscatalog_resources_of_parts')
        ->select('part_path_ptr')->take(1000000)->skip(9000000)->get();
        foreach($parts as $k => $v) {
            if(empty($unlinked)) { break; }
            $v = $this->truncate($v->part_path_ptr);
            $path = explode('/', $v);
            foreach($path as $k => $x) {
                if(in_array($x, $unlinked)) {
                    if($k > 0) {
                        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x)
                        ->update(['parent_ptr' => $path[$k-1]]);
                    }
                }
            }
        }


    }


    function extractNodes() {
        $this->nodesToCategories();
         $this->englishNodes();
         $this->rusificateNodes();
    }

    function nodesToCategories() {
        // total nodes 104248, без рутовой 104247
        $parts = \json_decode(file_get_contents('../resources/dev/alt_db_json/nodes_no_additional.json'));
        $existingNodes = \DB::table('outsource_ezpartscatalog_nodes')->get();
        $existingNodes = \arrayMap($existingNodes, 'ptr');
        if(!$parts) { \dd('json error'); }
        foreach($parts as $k => $v) {
            if($v->Ptr == '24554328449856') { print_r($v); }
            continue;
            if(isset($existingNodes[$v->Ptr])) { continue; }
            $i = $this->reformat($v);
            $ins = [
                'ptr' => $v->Ptr,
            ];
            if(!empty($v->Model_Ptr)) { $ins['model_ptr'] = $i->Model_Ptr; }
            if(!empty($v->Parent_Ptr)) { $ins['parent_ptr'] = $i->Parent_Ptr; }
            if(!empty($v->AdditionalProperties)) { $ins['additional_properties'] = $i->AdditionalProperties; }
            if(!empty($v->Order)) { $ins['ordering'] = $i->Order; }
            if(!empty($v->Root_Ptr)) { $ins['root_ptr'] = $i->Root_Ptr; }
            try {
                \DB::table('outsource_ezpartscatalog_nodes')->insert($ins);
            } catch(\Exception $e) {
                dd($v);
            }
        }
    }




   function devx() {

      die;
   }



   function cacheRegions() {
       $regions = \DB::table('regions')->get();
       foreach($regions as $x) {
           $cities = \DB::table('cities')->where('region_id', $x->region_id)
           ->orderBy('ordering', 'asc')->get();
           if(empty($cities)) { continue; }
           file_put_contents('cache/html/locations/regions/options/'.$x->region_id.'.html', view('cache.locations.region-options', compact('cities')));
       }
   }

    function cacheCities() {
        $regions = \DB::table('regions')->orderBy('ordering', 'asc')->get();
        $cities = \DB::table('cities')->where('main', true)->where('status', true)
            ->orderBy('ordering', 'asc')->get();
        file_put_contents('cache/html/locations/city-dialog-inner.html', view('cache.locations.city-dialog-inner', compact('cities', 'regions'))->render());
    }



    function updateAlias($x) {
        $alias = \Str::slug($x->title);
        $alias = trim($alias, '/');
        $alias = trim($alias, '-');
        $alias = str_replace('/', '-', $alias);
        $alias = $this->attempts ? $alias.'-'.$this->attempts : $alias;
        try {
            \DB::table('products')->where('product_id', $x->product_id)->update(['alias' => $alias]);
        } catch(\Exception $e) {
            $this->attempts++;
            $this->updateAlias($x);
        }
    }

    function deleteNodePrices() {
        $nodesProducts = \DB::table('products')
        ->join('outsource_ezpartscatalog_nodes', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
        ->get();
        foreach($nodesProducts as $prod) {
            \DB::table('products')->where('product_id', $prod->product_id)->update(['price' => 0, 'discount_price' => 0, 'price_data' => null]);
        }
    }

    function updateAvatars() {
        $nodesProducts = \DB::table('products')
            ->join('outsource_ezpartscatalog_nodes', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->get();
        foreach($nodesProducts as $x) {
            if(preg_match('/catalog/', $x->avatar)) {
                \DB::table('products')->where('product_id', $x->product_id)->update(['avatar' => str_replace('catalog/', '', $x->avatar)]);
            }
        }
    }

    function hideEmptyCategories() {
        $cats = \DB::table('categories')->where('status', 1)->whereNotNull('ptr')->get();
        foreach($cats as $x) {
            $ccats = \DB::table('categories')->where('parent', $x->category_id)->where('status', 1)->first();
            if($ccats) { continue; }
            $p = \DB::table('product_categories')->where('category_id', $x->category_id)->first();
            if($p) { continue; }
            \DB::table('categories')->where('category_id', $x->category_id)->update(['status' => false]);
        }
    }

    function handleDataGroupInfo() {
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->whereNotNull('data_group')
            ->where('data_group_unpacked', '<>', 1)
            ->whereRaw(\DB::raw("data_group_scanned > current_date - 1"))
            ->get();
        foreach($nodes as $x) {
            $rd = \json_decode($x->data_group);
            $parentProduct = \DB::table('products')->where('ptr', $x->ptr)->first();
            if(!empty($rd->error)) { continue; }
            if(empty($rd->value[0]->DataCache)) { continue; }
            foreach($rd->value[0]->DataCache as $k => $v) {
                $displayData = $v[1]; // данные для дисплея
                $partData = $v[3]; // данные о запчасти
                $status = $v[4];
                $part = \DB::table('products')->where('ptr', $partData->PartPtr)->first();
                if(!$part) { continue; }
                try {
                    \DB::table('product_consistance')
                    ->insert(['parent_product' => $parentProduct->product_id,
                    'child_product' => $part->product_id,
                    'ordering' => $k,
                    'consistance_quantity' => $displayData[5],
                    'scheme' => \json_encode([
                        'number' => $displayData[2],
                        'hint' => $displayData[7],
                        'status' => $status
                    ])
                    ]);
                } catch(\PDOException $e) { }
                $productClass = trim(trim($displayData[3]), '"');

                /*
                    $displayData[2];  номер на схеме
                    $displayData[3];  Класс запчасти
                    $displayData[4];  Артикул запчасти
                    $displayData[5];  Количество
                    $displayData[6];  название
                    $displayData[7];  Примечание
                    $partData->PartPtr;  Птр запчасти
                */
            }
        }
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group_unpacked' => 1]);

    }





    function deleteProductLinks() {
        \DB::table('links')->where('source', 'product')->delete();
    }

    function getLink($product) {
        $link = $this->attempts ? $product->link .'-'.$this->attempts : $product->link;
        try {
            $link_id = \DB::table('links')->insertGetId(['link' => trim($link, '/'), 'source' => 'product']);
        } catch(\Exception $e) {
            $this->attempts++;
            return $this->getLink($product);
        }
        return $link_id;

    }



    function j($path) {
        return \json_decode(file_get_contents('../resources/dev/'.$path));
    }

    function c($path, $a) {
        file_put_contents('../resources/dev/'.$path, \json_encode($a));
    }



    function linkProductNodesToCategories() {
        $products = \DB::table('products')->select('product_id', 'ptr', 'source_data')->whereNotNull('ptr')->get();
        foreach($products as $prod) {
            $data = \json_decode($prod->source_data);
            if(isset($data->code) || isset($data->owner_part_ptr)) { continue; }
            $source_part = \DB::table('outsource_ezpartscatalog_nodes')->select('parent_ptr')->where('ptr', $prod->ptr)->first();
            if(!$source_part) { continue; }
            $needle = \DB::table('categories')->select('category_id')->where('ptr', $source_part->parent_ptr)->first();
            if(!$needle) { continue; }
            \DB::table('product_categories')->insert(['product_id' => $prod->product_id, 'category_id' => $needle->category_id]);
        }
    }

    function createPartsFromOutsource() {
       $parts =  \DB::table('outsource_ezpartscatalog_parts')->get();
       foreach($parts as $x) {
        \DB::table('products')
        ->insert(
        ['title' => $x->title, 'alias' => $x->alias, 'ptr' => $x->ptr,
         'source_data' => \json_encode($x), 'status' => 1]);
        $part = \DB::table('products')->where('ptr', $x->ptr)->first()->product_id;
        \DB::table('product_codes')->insert(['product_id' => $part, 'product_code' => $x->code]);
       }
    }

    function pricesFromJoomlaProducts() {
        $prices = \json_decode(file_get_contents('../resources/dev/trash/j25_virtuemart_product_prices.json'));
        foreach($prices as $k => $v) {
            if(!\DB::table('products')->where('product_id', $v->virtuemart_product_id)->first()) { continue; }
            $data = [
                'price' => $v->product_price,
                'discount_value' => 0,
                'discount_type' => 0
            ];
            $price = $v->product_price;
            if($v->override) {
                $data['discount_value'] = $v->product_override_price;
                $data['discount_type'] = 1;
                $price = $v->product_override_price;
            }
            $price2 = $v->override ? $v->product_override_price : $v->product_price;
            \DB::table('products')
            ->where('product_id', $v->virtuemart_product_id)
            ->update(['price' => $price, 'discount_price' => $price2, 'price_data' => \json_encode($data)]);
        }
    }

    function linkOldCatalogProducts() {
        $categories = \json_decode(file_get_contents('../resources/dev/product_categories.json'));
        foreach($categories as $k => $x) {
            foreach($x as $cat) {
                if($cat == 123) {
                    \DB::table('product_categories')->insert(['product_id' => $k, 'category_id' => 444]);
                }
            }
        }
    }

    function importPrices() {
        $path = '../resources/dev/trash/prices.json';
        \DB::table('product_prices')->delete();
        $prices = \json_decode(file_get_contents($path));
        foreach($prices as $k => $v) {
            // product_id, price, price_data

            $v->price1 = str_replace(',', '', $v->price1);
            $v->price2 = str_replace(',', '', $v->price2);
            $products = \DB::table('product_codes')->where('product_code', $v->code)->get();
            if(empty($products)) { continue; }
            $data = [
                'price' => $v->price2,
                'discount_value' => $v->price1,
                'discount_type' => 1
            ];
            foreach($products as $prod) {
            $a = [
                'price' => $v->price2,
                'discount_price' => $v->price1,
                'price_data' => \json_encode($data)
            ];
            \DB::table('products')->where('product_id', $prod->product_id)->update($a);
            }
        }
    }

    function reformatPriceFile() {
        $path = '../resources/dev/prices.json';
        $prices = \json_decode(file_get_contents($path));
        foreach($prices as $k => $v) {
            $prices[$k]->price1 = str_replace('р.', '', $v->price1);
            $prices[$k]->price2 = str_replace('р.', '', $v->price2);
        }
        file_put_contents($path, \json_encode($prices));
    }
    /*
    function code($code, $ordering) {
        $code = trim($code);
        if(empty($code)) { return; }
        \DB::table('product_codes')->insert(['product_id' => $this->product->product_id, 'product_code' => $code, 'ordering' => $ordering]);
    }
    */




    function devxxx() {

        $parts = \DB::table('outsource_ezpartscatalog_parts')->select('title', 'code', 'ptr')->whereNull('alias')->get();
        foreach($parts as $k => $v) {
            $this->attempts = 0;
            $this->updatePart($v);
        }
    }

    function updatePart($v) {
        $alias = \Str::slug($v->title);
        $alias = 'tovar-'.$v->code.'-'.$alias;
        try {
            \DB::table('outsource_ezpartscatalog_parts')->where('ptr', $v->ptr)->update(['alias' => $alias]);
        } catch(\PDOException $e) {
            $this->attempts++;
            return $this->updatePart($v);
        }
        return 1;
    }

    function recreateParts() {
        $parts = \DB::table('outsource_ezpartscatalog_parts')->get();
        foreach($parts as $k => $v) {
            $this->attempts = 0;
        }
    }

    function insertPart() {

    }

    function linkPdf() {
            $schemes = \DB::table('products')->whereNotNull('source_data')->get();
            $hasfiles = 0;
            foreach($schemes as $x) {
                $data = \json_decode($x->source_data);
                $data2 = \json_decode($data->data);
                unset($data->data);
                print_r($data);
                echo '<br><br>';
                print_r($data2);
                 die;
                if(!@$data2->value[0]->ResourcePtr) { continue; }
                $filename = str_replace('.pdf', '', $data2->value[0]->FileName);
                $filename = $filename.'.jpg';
                if($filename == 'no_image.jpg') { continue; }
                $filename = 'catalog/'.$filename;
                \DB::table('products')->where('product_id', $x->product_id)->update(['avatar' => $filename]);
            }
    }

    function iterateIgnored($ptr) {
        $childs = \DB::table('outsource_ezpartscatalog_nodes')->where('parent_ptr', $ptr)->get();
        if(empty($childs)) { return; }
        foreach($childs as $k => $v) {
            $a = \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $v->ptr)->update(['ignore' => 1]);
            $this->ignored[] = $v->ptr;
            $this->count+= $a;
            $this->iterateIgnored($v->ptr);
        }
    }

    function cascadeNonParentCategories() {
        $this->cats = [];
        $cats = \DB::table('categories')->whereNotNull('ptr')->whereNull('parent')->get();
        foreach($cats as $x) {
            $this->cats[] = $x->category_id;
        }
        foreach($this->cats as $x) {
            $this->getChildsOfCat($x);
        }
        $this->cats = array_unique($this->cats);
        foreach($this->cats as $x) {
            \DB::table('categories')->where('category_id', $x)->delete();
        }
    }

    function testCategories() {
        $cats = \DB::table('outsource_ezpartscatalog_nodes')
        ->select('parent_ptr', 'title')
        ->where('parent_ptr', $this->rootPtr)
        ->get();
        dd($cats);
        foreach($cats as $k => $v) {
            if($v->ptr == $this->rootPtr) { continue; }
            if($v->parent_ptr == $this->rootPtr) {
                \DB::table('categories')
                    ->insert(['ptr' => $v->ptr,
                        'parent_ptr' => 444,
                        'title' => $v->title,
                        'menu_title' => $v->title
                    ]);
            }

        }
    }

    function updateExtractedCategories() {
        $cats = \DB::table('categories')
        ->select('ptr', 'parent_ptr', 'category_id')
        ->whereNotNull('ptr')
        ->get();
        $catsByPtr = \arrayMap($cats, 'ptr');
        foreach($cats as $k => $v) {
            if(!$v->parent_ptr || empty($v->parent_ptr)) { continue; }
            if(isset($catsByPtr[$v->parent_ptr]->category_id)) {
                $parent = $catsByPtr[$v->parent_ptr]->category_id;
            } else {
                if($v->parent_ptr == $this->rootPtr) {
                    $parent = $this->catalogCategory;
                } else {
                    $parent = false;
                }
            }
            if($parent === false) { continue; }
            $update = ['parent' => $parent];
            \DB::table('categories')->where('category_id', $v->category_id)
            ->update($update);
        }
    }

    function createCategoriesFromSource() {
        $sources = \DB::table('outsource_ezpartscatalog_nodes')->whereNull('is_final')->whereNull('ignore')->get();
        foreach($sources as $k => $v) {
            $this->attempts = 0;
            if(!$v->parent_ptr) { continue; }
            if($v->ptr == $this->rootPtr) { continue; }
            $this->go($v);
        }
    }

    function go($v) {
        $alias = $this->attempts ? $v->alias.'-'.$this->attempts : $v->alias;
        try {
            \DB::table('categories')
                ->insert(['ptr' => $v->ptr,
                    'parent_ptr' => $v->parent_ptr,
                    'title' => $v->title,
                    'menu_title' => $v->title,
                    'alias' => $alias,
                    'ordering' => $v->ordering
                ]);
        } catch(\PDOException $e) {
            $this->attempts++;
            return $this->go($v);
        }
        return 1;
    }

    function reformat($x) {
        foreach($x as $k => $v) {
            $v = trim($v);
            if(strtolower($v) == 'null') {
                unset($x->{$k});
                continue;
            }
            $x->{$k} = $v;
            if(empty($x->{$k})) { unset($x->{$k}); }
        }
        return $x;
    }


    function importParts() {

    }

    function updateParents() {

        $source = \json_decode(file_get_contents('../resources/dev/alt_db_json/node_links.json'));
        foreach($source as $k => $v) {
            $q = \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $v->Ptr);
            $v->Parent_Ptr = trim(str_replace('\r', '', $v->Parent_Ptr));
            if((strtolower($v->Parent_Ptr) == 'null')
             || strpos(strtolower($v->Parent_Ptr), 'n') !== false ||
              !$v->Parent_Ptr) {
                $q->update(['parent_ptr' => null]);
            } else {
                $q->update(['parent_ptr' => $v->Parent_Ptr]);
            }
        }
        die;

        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $this->rootPtr)->delete();


    }

    function getDoubles() {


        return $rproducts;
    }

    function devPage() {
        $rproducts = $this->getDoubles();
        die;

        return $this->view('dev', ['products' => $rproducts, 'code' => $this->currentCode]);
    }

    function handle() {
        $result = new \Result;
        $product_id = \Request::get('id');
        $code = \Request::get('code');
        if(!$product_id) { $result->display = 'Нет айди товара'; return $result; }
        if(!\anInt($product_id)) { $result->display = 'Не интовое значение ида товара'; return $result; }
        \DB::table('product_codes')->where('product_id', $product_id)->where('product_code', $code)->update(['temp' => 1]);
        $rproducts = $this->getDoubles();
        return \json_encode((object)['view' => $this->view('dev-products', ['products' => $rproducts, 'code' => $this->currentCode])->render()]);
    }


    function intoProduct() {
        $result = new \Result;
        $product_id = \Request::get('id');
        $code = \Request::get('code');
        $anotherId = \Request::get('anotherId');
        $anotherData = \Request::get('anotherData');
        if(!$product_id) { $result->display = 'Нет айди товара'; return $result; }
        if(!\anInt($product_id)) { $result->display = 'Не интовое значение ида товара'; return $result; }
        if(!$anotherId) { $result->display = invalidDataFormat; return $result; }
        if(!\anInt($anotherId)) { $result->display = invalidDataFormat; return $result; }
        $pm = ProductsModel::getInstance();
        $pm->getProductById($product_id);
        $a = clone $pm->product;
        $a->isMain = 1;
        $pm->getProductById($anotherId);
        $b = clone $pm->product;
        $b->isMain = 0;
        $this->mergeProducts($a, $b);
        $rproducts = $this->getDoubles();
        return \json_encode((object)['view' => $this->view('dev-products', ['products' => $rproducts, 'code' => $this->currentCode])->render()]);
    }

    function mergeProducts($a, $b) {
        $first = $a->isMain ? $a : $b;

    }

}
