<?php

namespace App\Http\Controllers;
use App\Models\CategoriesModel;

class PagesController extends FrontController {

    function mainPage() {
        $this->page();
        if(!\Cache::has('frontPage')) {
            $front = \json_decode(file_get_contents('../resources/data/index.json'));
            $cm = CategoriesModel::getInstance();
            $categories = [];
            foreach($front->categories as $k => $x) {
                $cat = $cm->getCategoryById($x);
                if($cat) { $categories[] = $cat; }
            }

            \Cache::forever('frontPage', \json_encode($categories));
        }
        return $this->view('pages.index', ['categories' => \json_decode(\Cache::get('frontPage'))]);
    }





}