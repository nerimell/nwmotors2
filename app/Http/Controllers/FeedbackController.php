<?php

namespace App\Http\Controllers;

class FeedbackController extends FrontController {


    function askPage() {
        $this->page((object)['meta_title' => 'Вопрос менеджеру']);
        $breadcrumbs = [(object)['title' => 'Вопрос менеджеру', 'link' => false]];
        return $this->view('feedback.chat', compact('breadcrumbs'));
    }

}