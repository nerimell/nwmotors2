<?php

namespace App\Http\Controllers;
use App\Models\ManufacturerModel;


class ManufacturerController extends FrontController {

    function __construct() {
        $this->model = new ManufacturerModel;
    }

    function manufacturerPage($alias) {
        $manufacturer = $this->model->getManufacturerByAlias($alias);
        if(!$manufacturer) { abort(404); }
        $this->page($manufacturer->manufacturer);
        return $this->view('shop.manufacturer', [
            'manufacturer' => $manufacturer->manufacturer,
            'products' => $manufacturer->products,
            'total' => $manufacturer->total
        ]);
    }
}
