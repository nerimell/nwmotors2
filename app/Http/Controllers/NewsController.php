<?php

namespace App\Http\Controllers;
use App\Models\NewsModel;

class NewsController extends FrontController {

    function __construct() {
        $this->model = new NewsModel;
    }

    function newsPage() {
       $this->model->getRecentArticles();
       $this->page((object)['meta_title' => 'Новости']);
       return $this->view('content.list', [
        'articles' => $this->model->articles,
        'breadcrumbs' => $this->model->breadcrumbs,
        'params' => $this->model->params,
        'perPage' => $this->model->perPage,
        'total' => $this->model->total,
        'baseUrl' => '/news',
        'activeLinks' => $this->model->activeLinks
       ]);
    }

    function postPage($alias) {
        $this->model->getArticleByAlias($alias);
        if(!$this->model->article) { abort(404); }
        $this->page($this->model->article);
        return $this->view('content.article', [
            'article' => $this->model->article,
            'breadcrumbs' => $this->model->breadcrumbs,
            'activeLinks' => $this->model->activeLinks
        ]);
    }

}