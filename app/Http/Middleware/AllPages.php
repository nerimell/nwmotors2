<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ShopModel;

class AllPages {

    public function handle($request, Closure $next){
        include_once('../resources/data/constants.php');
        $user = \Auth::user();
        define('user_group_id', @$user->user_group_id ? $user->user_group_id : 0);
        define('logged', @$user->id);
        define('tzd', \Cookie::has('tz') ? 1 : 0); // timezone defined
        getUserCity();
        $tz = timezone(\Cookie::get('tz'));
        define('tz', $tz);  // timezone offset
        ShopModel::calculateDiscount();
        return $next($request);
    }
}
