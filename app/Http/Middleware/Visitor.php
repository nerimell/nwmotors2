<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Visitor {

    public function handle($request, Closure $next, $guard = null){

        $user = \Auth::user();

        if($user) {
            return redirect()->to('/profile');

        }

        return $next($request);
    }
}
