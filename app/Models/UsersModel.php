<?php
namespace App\Models;

class UsersModel extends Model {

    public $fields = ['username', 'password', 'password2', 'email'];
    public $usertypes;
    public $extra;
    public $result;
    private $user;
    public $collected;
    public $required = 0;
    public static $_instance = null;

    private function __construct() {

    }

    static function getInstance() {
        if(self::$_instance != null) { return self::$_instance; }
        return self::$_instance = new self;
    }

    function login() {
        $result = new \Result;
        if(logged) {
            $result->result = 1;
            return $result;
        }
        if(!\Auth::attempt(['username' => \Request::get('username'), 'password' => \Request::get('password')])) {
            $result->display = 'Неправильный логин или пароль';
            return $result;
        }
        $user = \Auth::user();
        \DB::table('users')->where('id', $user->id)->update(['last_ip' => $_SERVER['REMOTE_ADDR']]);
        if($user->city) {
            $city = getCityById($user->city);
            \Cookie::queue(\Cookie::make('city', $city->city_id, time()+2678400));
            \Html::$city = $city;
        }
        $result->result = 1;
        return $result;
    }

    static function getUserById($id) {
        return \DB::table('users')->where('id', $id)->first();
    }

    function getUserTypes() {
        if($this->usertypes) { return $this->usertypes; }
        $usertypes = [
            (object)[
                'title' => 'Физическое лицо'
            ],
            (object)[
                'title' => 'Юридическое лицо',
                'fields' => [
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 3 ], 'type' => 'text', 'title' => 'Название компании', 'name' => 'company_name', 'length' => 100],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 18, 'max' => 22, 'numeric' => 1], 'type' => 'text', 'title' => 'Расчетный счет', 'name' => 'checking_account', 'length' => 22],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 6, 'blockeng' => 1], 'type' => 'text', 'title' => 'Юридический адрес', 'name' => 'legal_address', 'length' => 100],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 3], 'type' => 'text', 'title' => 'Наименование банка', 'name' => 'bank_name', 'length' => 50],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 10, 'max' => 12, 'blockeng' => 1, 'numeric' => 1], 'type' => 'text', 'title' => 'ИНН', 'name' => 'inn', 'length' => 35],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 9, 'max' => 9, 'numeric' => 1], 'type' => 'text', 'title' => 'КПП', 'name' => 'kpp', 'length' => 9],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 8, 'max' => 8, 'numeric' => 1], 'type' => 'text', 'title' => 'ОКПО', 'name' => 'okpo', 'length' => 8],
                    (object)['wrap' => 'col-xs-12 col-md-6', 'wrapMini' => '', 'required' => 1, 'dataset' => ['min' => 9, 'max' => 9, 'numeric' => 1],'type' => 'text', 'title' => 'БИК', 'name' => 'bik', 'length' => 35],
                    (object)['wrap' => 'col-xs-12 large-input-group correspond-row', 'wrapMini' => 'large-input-group correspond-row', 'required' => 1, 'dataset' => ['min' => 20, 'max' => 20, 'numeric' => 1], 'type' => 'text', 'title' => 'Корреспондентский счет', 'name' => 'correspond_account', 'length' => 35],
                    (object)['wrap' => 'col-xs-12 large-input-group', 'wrapMini' => 'large-input-group', 'required' => 1, 'type' => 'select', 'values' => ['Генеральный директор', 'Директор', 'Доверенное лицо'], 'title' => 'Руководитель организации', 'name' => 'company_director'],
                    (object)['wrap' => 'col-xs-12 large-input-group', 'wrapMini' => 'large-input-group', 'required' => 1, 'type' => 'select', 'values' => ['Устава', 'Доверенности'], 'title' => 'Действующий на основании', 'name' => 'company_director_based_on'],
                    (object)['wrap' => 'col-xs-12 large-input-group', 'wrapMini' => 'large-input-group', 'required' => 1, 'dataset' => ['min' => 6, 'blockeng' => 1], 'type' => 'text', 'title' => 'Ф.И.О руководителя или доверенного лица', 'name' => 'trusted_person', 'length' => 100]
                ]
            ]
        ];
        return $this->usertypes = $usertypes;
    }

    function getAdditionalDataById($id) {
        if(!$id) { return null; }
        return \json_decode(@\DB::table('users_data')->where('user_id', $id)->first()->data);
    }

    function getUser($id) {
        if(!$id) { return $this->getDefaultUserData(); }
        $userData = \Auth::user();
        $fields = $this->getAdditionalDataById($id);
        if(!empty($fields)) {
            foreach($fields as $k => $v) {
                $userData->{$k} = $v;
            }
        }
        $userData->city = getCityById($id);
        $userData->city = $userData->city ? $userData->city : \Html::$city;
        return $userData;
    }

    function completeRememberedUserData(&$userData) {
        if(!$userData) { $userData = $this->getDefaultUserData(); }

    }

    function getDefaultUserData() {
        $userData = (object)[
            'first_name' => \Cookie::get('first_name'),
            'last_name' => \Cookie::get('last_name'),
            'third_name' => \Cookie::get('third_name'),
            'city' => \Html::$city,
            'user_type' => \Cookie::get('user_type'),
            'phone' => \Cookie::get('phone'),
            'address' => \Cookie::get('address'),
            'post_index' => \Cookie::get('post_index'),
            'email' => \Cookie::get('email')
        ];
        $this->getUserTypes();
        foreach($this->usertypes[1]->fields as  $x) {
            $userData->{$x->name} = \Cookie::get($x->name);
        }
        return $userData;
    }




    function getUserDiscountByGroup($id) {
        if(empty($ids)) { return; }
        return \DB::table('user_groups')
            ->select('user_groups_discounts.*')
            ->whereIn('user_groups.user_group_id', $id)
            ->leftJoin('user_groups_discounts', 'user_groups.user_group_id', '=', 'user_groups_discounts.user_group_id')
            ->get();
    }

    function register() {
        $this->result = new \Result;
        $this->collectData();
        if(!$this->validateRegistration()) { return $this->result; }
        if(!$this->createUser()) { return $this->result; }
        \Auth::attempt(['username' => $this->data['username'], 'password' => $this->data['password']]);
        $this->result->result = 1;
        return $this->result;
    }

    function createUser() {
        $insert = $this->data;
        $insert['password'] = \Hash::make($this->data['password']);
        try {
            \DB::table('users')->insert($insert);
        } catch(\PDOException $e) {
            $msg = substr($e->getMessage(), 93, 18);
            if($msg == 'users_username_key') {
                $this->result->display = 'Пользователь с таким логином уже зарегистрирован';
            } else {
                $this->result->display = 'Пользователь с таким емайлом уже зарегистрирован';
            }
            return 0;
        }
        return 1;
    }

    function saveSettings() {
        $this->result = new \Result;
        $this->extra = \Request::get('extra');
        $this->fields = $this->extra ? ['first_name', 'last_name', 'city', 'post_index', 'third_name', 'phone', 'address', 'user_type'] : ['old_password', 'password', 'password2', 'reset_password'];
        $this->collectData();
        if(!$this->validateProfileData()) { return $this->result; }
        if($this->extra) {
            \DB::table('users')->where('id', $this->user->id)->update(['phone' => $this->data['phone'], 'city' => $this->data['city'], 'first_name' => $this->data['first_name'], 'last_name' => $this->data['last_name'], 'user_type' => $this->data['user_type']]);
            unset($this->data['city']);
            unset($this->data['first_name']);
            unset($this->data['last_name']);
            unset($this->data['user_type']);
            unset($this->data['phone']);
            \DB::table('users_data')->where('user_id', $this->user->id)->delete();
            \DB::table('users_data')->insert(['user_id' => $this->user->id, 'data' => \json_encode($this->data)]);
            $this->result->display = 'Контактная информация сохранена';
        } else {
            if($this->data['reset_password']) {
                \DB::table('users')->where('id', $this->user->id)->update(['password' => \Hash::make($this->data['password'])]);
            }
            $this->result->display = 'Информация об аккаунте сохранена';
        }
        $this->result->city = \Html::$city;
        $this->result->result = 1;
        return $this->result;
    }

    function validateRequiredProfileData() {
        // first_name
        $this->getUserTypes();
        if(empty($this->data['first_name'])) { $this->result->display = 'Введите ваше имя'; return 0; }
        $length = mb_strlen($this->data['first_name']);
        if($length < 2) { $this->result->display = 'Имя не может быть кароче 2х символов'; return 0; }
        if($length > 35) { $this->result->display = 'Длина имени не дожна превышать 35 символов'; return 0; }
        if(preg_match('/[A-z]/',$this->data['first_name'], $matches)) { $this->result->display = 'Введите имя на русском языке'; return 0; }
        // last_name
        if(empty($this->data['last_name'])) { $this->result->display = 'Введите вашу фамилию'; return 0; }
        $length = mb_strlen($this->data['last_name']);
        if($length < 2) { $this->result->display = 'Фамилия не может быть кароче 2х символов'; return 0; }
        if($length > 35) { $this->result->display = 'Длина фамилии не дожна превышать 35 символов'; return 0; }
        if(preg_match('/[A-z0-9]/',$this->data['last_name'], $matches)) { $this->result->display = 'Введите фамилию на русском языке'; return 0; }
        // third_name
        if(empty($this->data['third_name'])) { $this->result->display = 'Укажите ваше отчество'; return 0; }
        $length = mb_strlen($this->data['third_name']);
        if($length < 3) { $this->result->display = 'Отчество не может быть кароче 3х символов'; return 0; }
        if($length > 35) { $this->result->display = 'Длина отчества не дожна превышать 35 символов'; return 0; }
        if(preg_match('/[A-z0-9]/',$this->data['third_name'], $matches)) { $this->result->display = 'Введите отчество на русском языке'; return 0; }
        // phone
        if(!@$this->data['phone']) { $this->result->display = 'Введите ваш номер телефона'; return 0; }
        if(!validatePhone($this->data['phone'])) { $this->result->display = 'Некорректный формат номера телефона'; return 0; }

        // post_index
        if(!@$this->data['email']) { $this->result->display = 'Укажите ваш емайл'; return 0; }
        if(!validateEmail($this->data['email'])) { $this->result->display = 'Некорректный емайл адрес'; return 0; }

        if(empty($this->data['post_index'])) { $this->result->display = 'Укажите почтовый индекс'; return 0; }
        if(!\anInt($this->data['post_index'])) { $this->result->display = 'Некорректный почтовый индекс'; return 0; }
        if(mb_strlen($this->data['post_index']) != 6) { $this->result->display = 'Некорректный почтовый индекс'; return 0; }
        if(!isset($this->usertypes[$this->data['user_type']])) { $this->result->display = 'Неправильный тип аккаунта'; return 0; }
        if($this->data['user_type'] == 1) { return $this->validateRequiredUserType1(); }

        return 1;
    }

    function validateRequiredUserType1() {
        $fields = &$this->usertypes[1]->fields;
        // company_name
        $length = mb_strlen($this->data['company_name']);
        if(empty($this->data['company_name'])) { $this->result->display = 'Укажите название компании'; return 0; }
        if($length < 2) { $this->result->display = 'Название компании не может быть менее 2х символов'; return 0; }
        if($length > 50) { $this->result->display = 'Длина названия компании не должна превышать 50 символов'; return 0; }
        if(preg_match('/[A-z]/',$this->data['third_name'], $matches)) { $this->result->display = 'Введите адрес на русском языке'; return 0; }
        // checking_account
        if(empty($this->data['checking_account'])) { $this->result->display = 'Укажите номер рассчетного счета'; return 0; }
        $length = mb_strlen($this->data['checking_account']);
        if($length < 18 || $length > 22) { $this->result->display = 'Некорректная длина номера рассчетного счета'; return 0; }
        // legal_address
        if(empty($this->data['legal_address'])) { $this->result->display = 'Укажите юридический адрес'; return 0; }
        $length = mb_strlen($this->data['legal_address']);
        if($length < 6) { $this->result->display = 'Юридический адрес не может содержать менее 6 символов'; return 0; }
        if($length > 100) { $this->result->display = 'Юридический адрес не может содержать более 100 символов'; return 0; }
        if(preg_match('/[A-z]/',$this->data['legal_address'], $matches)) { $this->result->display = 'Введите юридический адрес на русском языке'; return 0; }
        // bank_name
        if(empty($this->data['bank_name'])) { $this->result->display = 'Укажите название банка'; return 0; }
        $length = mb_strlen($this->data['bank_name']);
        if($length < 3) { $this->result->display = 'Название банка должно содержать не менее 2-х символов'; return 0; }
        if($length > 100) { $this->result->display = 'Название банка не может содержать более 100 символов'; return 0; }
        // inn
        if(empty($this->data['inn'])) { $this->result->display = 'Введите ИНН'; return 0; }
        $length = mb_strlen($this->data['inn']);
        if($length < 10) { $this->result->display = 'ИНН должен содержать не менее 10 цифр'; return 0; }
        if($length > 12) { $this->result->display = 'ИНН должен содержать не более 12 цифр'; return 0; }
        if(!checkLongInt($this->data['inn'])) { $this->result->display = 'ИНН должен содержать только цифры'; return 0; }
        // kpp
        if(empty($this->data['kpp'])) { $this->result->display = 'Введите КПП'; return 0; }
        $length = mb_strlen($this->data['kpp']);
        if($length != 9) { $this->result->display = 'КПП должен содержать 9 цифр'; return 0; }
        if(!checkLongInt($this->data['kpp'])) { $this->result->display = 'КПП должен содержать только цифры'; return 0; }
        // bik
        if(empty($this->data['bik'])) { $this->result->display = 'Введите БИК'; return 0; }
        $length = mb_strlen($this->data['bik']);
        if($length != 9) { $this->result->display = 'БИК должен содержать 9 цифр'; return 0; }
        if(!checkLongInt($this->data['bik'])) { $this->result->display = 'БИК должен содержать только цифры'; return 0; }
        // okpo
        if(empty($this->data['okpo'])) { $this->result->display = 'Укажите ОКПО'; return 0; }
        $length = mb_strlen($this->data['okpo']);
        if($length != 8) { $this->result->display = 'ОКПО должно содержать 8 цифр'; return 0; }
        if(!checkLongInt($this->data['okpo'])) { $this->result->display = 'ОКПО должно содержать только цифры'; return 0; }
        // correspond_accont = 20
        if(empty($this->data['correspond_account'])) { $this->result->display = 'Укажите номер корреспондентского счета'; return 0; }
        $length = strlen($this->data['correspond_account']);
        if($length != 20) { $this->result->display = 'Корреспондентский счет должен содержать 20 символов'; return 0; }
        if(!checkLongInt($this->data['correspond_account'])) { $this->result->display = 'ОКПО должно содержать только цифры'; return 0; }
        // company director

        if(!isset($this->usertypes[1]->fields[9]->values[@$this->data['company_director']])) { $this->result->display = 'Некорректные данные о руководителе организации'; return 0; }
        // company_director_based_on
        if(!isset($this->usertypes[1]->fields[10]->values[@$this->data['company_director_based_on']])) { $this->result->display = 'Некорректные данные о руководителе организации'; return 0; }
        // trusted_person
        if(empty($this->data['trusted_person'])) { $this->result->display = 'Укажите Ф.И.О руководителя или доверенного лица'; return 0; }
        $length = mb_strlen($this->data['trusted_person']);
        if($length < 6) { $this->result->display = 'Ф.И.О руководителя / доверенного лица должно быть не менее 6 символов'; return 0; }
        if($length > 100) { $this->result->display = 'Ф.И.О руководителя / доверенного лица не может содержать более 100 символов'; return 0; }
        if(preg_match('/[A-z0-9]/',$this->data['trusted_person'], $matches)) { $this->result->display = 'Введите Ф.И.О руководителя / доверенного лица на русском языке'; return 0; }

        return 1;
    }



    function validateProfileData() {
        $this->user = \Auth::user();
        if($this->extra) {
            $this->getUserTypes();
            $usertype = @$this->data['user_type'] ? @$this->data['user_type'] : \Request::get('user_type');
            $usertype = $usertype ? $usertype : 0;
            $this->getUserTypes();
            if(!isset($this->usertypes[$usertype])) { $this->result->display = 'Неправильный тип аккаунта'; return 0; }
            $fieldsLength = [
                'first_name' => (object)['length' => 20, 'name' => 'Имя'],
                'last_name' => (object)['length' => 20, 'name' => 'Фамилия'],
                'third_name' => (object)['length' => 20, 'name' => 'Отчество'],
                'city' => (object)['int' => 1, 'name' => 'Город'],
                'post_index' => (object)['length' => 6, 'name' => 'Почтовый индекс'],
                'address' => (object)['length' => 70, 'name' => 'Адрес'],
            ];

            if(!empty($this->data['city'])) {
                if(!\anInt($this->data['city'])) { $this->result->display = 'Неверный формат данных о городе'; return 0; }
                \Html::$city = getCityById($this->data['city']);
                if(!\Html::$city) { $this->result->display = 'Город не существует'; return 0; }
            }
            if(!empty($this->usertypes[$usertype]->fields)) {
                foreach($this->usertypes[$usertype]->fields as $x) {
                    $this->fields[] = $x->name;
                }
            }
            if(!$this->collected) {
                $this->collectData();
            }
            foreach($this->data as $k => $x) {
                if(!isset($fieldsLength[$k])) { continue; }
                if(!@$fieldsLength[$k]->length) { continue; }
                if(mb_strlen($x) > $fieldsLength[$k]->length) { $this->result->display = 'Значение поля `'.$fieldsLength[$k]->name.'` не должно превышать '.$fieldsLength[$k]->length.' символов'; return 0; }
            }
            if(!empty($this->usertypes[$usertype]->fields)) {
                foreach($this->usertypes[$usertype]->fields as $x) {

                    if($x->type == 'text') {
                        if(mb_strlen($this->data[$x->name]) > $x->length) {
                            $this->result->display = 'Значение поля `'.$x->title.'` не должно превышать '.$x->length.' символов'; return 0;
                        }
                        continue;
                    }
                    if($x->type == 'select') {
                        if(!$this->data[$x->name]) { $this->data[$x->name] = 0; }
                        if(!isset($x->values[$this->data[$x->name]])) { $this->result->display = 'Некорректное значение для поля `'.$x->title.'`'; return 0; }
                    }
                }
            }

            return 1;
        }
        if($this->data['reset_password']) {
            if(!trim($this->data['old_password'])) { $this->result->display = 'Введите старый пароль'; return 0; }
            if(!$this->data['password'] || empty($this->data['password'])) { $this->result->display = 'Введите новый пароль'; return 0; }
            if(mb_strlen($this->data['password']) < 6) { $this->result->display = 'Новый пароль слишком короткий'; return 0; }
            if(mb_strlen($this->data['password']) > 25) { $this->result->display = 'Новый пароль слишком длинный'; return 0; }
            if(!$this->validatePassword()) { return 0; }
            if(!\Hash::check($this->data['old_password'], $this->user->password)) {
                $this->result->display = 'Неправильный пароль'; return 0;
            }
            unset($this->data['old_password']);
        }
        return 1;
    }

    function validateRegistration() {
        foreach($this->data as $k => $v) {
            $this->data[$k] = trim($this->data[$k]);
        }
        if(empty($this->data['username'])) { $this->result->display = 'Укажите имя пользователя'; return 0; }
        if(!$this->data['username']) { $this->result->display = 'Некорректное имя пользователя'; return 0; }
        if(!$this->data['email']) { $this->result->display = 'Укажите емайл'; return 0; }
        if(!validateEmail($this->data['email'])) { $this->result->display = 'Некорректный емайл'; return 0; }
        if(!$this->data['password'] || empty($this->data['password'])) { $this->result->display = 'Введите пароль'; return 0; }
        if(mb_strlen($this->data['password']) < 6) { $this->result->display = 'Ваш пароль слишком короткий'; return 0; }
        if(mb_strlen($this->data['password']) > 25) { $this->result->display = 'Ваш пароль слишком длинный'; return 0; }
        if(!$this->validatePassword()) { return 0; }
        if(!\Request::get('confirm')) { $this->result->display = 'Подтвердите ознакомление с правилами пользования сайтом'; return 0; }
        return 1;
    }

    function validatePassword() {
        if($this->data['password'] != $this->data['password2']) { $this->result->display = 'Неверное подтверждение пароля'; return 0; }
        unset($this->data['password2']);
        return 1;
    }


}
