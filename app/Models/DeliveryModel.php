<?php

namespace App\Models;


class DeliveryModel {

    public $volume;
    public $weight;
    public $products;
    public $deliveryVariants;
    public $deliveryLinks;
    public static $shopPoint;
    public static $city;
    public $params;
    public $bot;
    public static $_instance = null;


    private function __construct() {
        $this->params = self::getDefaultParams();
    }

    static function getInstance() {
        if(self::$_instance != null) { return self::$_instance; }
        return self::$_instance = new self;
    }

    function init($city = 0) {
        self::$city = $city ? $city : \Html::$city;
        if(self::$city->default_shop_point != self::$city->city_id) {
            $shopPoint = self::$city->default_shop_point ? self::$city->default_shop_point : defaultShopPoint;
            self::$shopPoint = getCityById($shopPoint);
        } else {
            self::$shopPoint = self::$city;
        }
        $this->getVariants();
    }

    function getStoredDeliveries() {
        $response = \DB::table('delivery_costs')->where('city_from', self::$shopPoint->city_id)
            ->where('city_to', self::$city->city_id)
            ->whereRaw(\DB::raw('delivery_costs.updated_at > (current_date - 30)'))->first();
        if(!$response) { return 0; }
        $this->deliveryVariants->pecom->response = \json_decode($response->pecom_response);
        $this->deliveryVariants->jd->response = \json_decode($response->jd_response);
        return 1;
    }

    function formatResponses() {
        foreach($this->deliveryVariants as &$x) {
            $x->priceinfo = (object)['total' => 0, 'terminal' => (object)['truck' => 0, 'air' => 0], 'door' => (object)['truck' => 0, 'air' => 0]];
        }
        $this->formatPecomResponse();
        $this->formatJdResponse();
    }

    function formatJdResponse() {
        if(!@$this->deliveryVariants->jd->response) { return; }
        $p = &$this->deliveryVariants->jd->response;

        if(!@$p->im->auto || !@$p->im->delivery || !@$p->im->cash->price) { return; }
        if(!is_numeric($p->im->auto) || !is_numeric($p->im->delivery) || !is_numeric($p->im->cash->price)) { return; }
        $this->deliveryVariants->jd->priceinfo->terminal->truck = (int)($p->im->delivery+$p->im->cash->price);
        $this->deliveryVariants->jd->priceinfo->total += $p->im->delivery+$p->im->cash->price;
        $this->deliveryVariants->jd->priceinfo->door->truck += (int)($p->im->auto+$p->im->cash->price);
        $this->deliveryVariants->jd->priceinfo->total += $p->im->auto+$p->im->cash->price;

    }

    function formatPecomResponse() {
        if(!@$this->deliveryVariants->pecom->response) { return; }
        $p = $this->deliveryVariants->pecom->response;
        if(!@$p->deliver[2] || !is_numeric(@$p->deliver[2]) || !@$p->ADD_3->{2} || !is_numeric(@$p->ADD_3->{2}) || !@$p->ADD_1->{2} || !is_numeric(@$p->ADD_1->{2})) { return; }
        if(@$p->auto[2]) {
            $this->deliveryVariants->pecom->priceinfo->terminal->truck = (int)($p->auto[2]+$p->ADD_1->{2}+$p->ADD_3->{2});
            $this->deliveryVariants->pecom->priceinfo->total+= $this->deliveryVariants->pecom->priceinfo->terminal->truck;
            if(@$p->deliver[2]) {
                $this->deliveryVariants->pecom->priceinfo->door->truck = (int)($p->auto[2]+$p->ADD_1->{2}+$p->ADD_3->{2}+$p->deliver[2]);
                $this->deliveryVariants->pecom->priceinfo->total+= $this->deliveryVariants->pecom->priceinfo->door->truck;
            }
        }
        if(@$p->avia[2]) {
            $this->deliveryVariants->pecom->priceinfo->terminal->avia = (int)($p->avia[2]+$p->ADD_1->{2}+$p->ADD_3->{2});
            $this->deliveryVariants->pecom->priceinfo->total+= $this->deliveryVariants->pecom->priceinfo->terminal->avia;
            if(@$this->deliver[2]) {
                $this->deliveryVariants->pecom->priceinfo->terminal->avia = (int)($p->avia[2]+$p->ADD_1->{2}+$p->ADD_3->{2}+$p->deliver[2]);
                $this->deliveryVariants->pecom->priceinfo->total+= $p->avia[2]+$p->ADD_1->{2}+$p->ADD_3->{2}+$p->deliver[2];
            }
        }

    }

    function getDeliveries() {
        $this->init();
        foreach($this->deliveryVariants as $k => $x) {
            if(!$x->fn) { continue; }
            eval('$this->'.$x->fn.'();');
        }
        $this->formatResponses();

        return $this->deliveryVariants;
    }

    function storeDelivery() {
            $resp = \DB::table('delivery_costs')->where('city_from', self::$shopPoint->city_id)
                ->where('city_to', self::$city->city_id)
                ->update(['updated_at' => date('Y-m-d H:i:s'),
                 'pecom_response' => \json_encode(@$this->deliveryVariants->pecom->response),
                 'jd_response' => \json_encode(@$this->deliveryVariants->jd->response)
                 ]);
            if(!$resp) {
            \DB::table('delivery_costs')
                ->insert(['updated_at' => date('Y-m-d H:i:s'),
                    'pecom_response' => \json_encode(@$this->deliveryVariants->pecom->response),
                    'jd_response' => \json_encode(@$this->deliveryVariants->jd->response),
                    'city_from' => self::$shopPoint->city_id,
                    'city_to' => self::$city->city_id
                ]);
            }

    }

    function getVariants() {
        if($this->deliveryVariants) { return $this->deliveryVariants; }
        return $this->deliveryVariants = (object)[
            'pecom' => (object)['fn' => 'getPecomCost'],
            'jd' => (object)['fn' => 'getJdCost'],
            'bl' => (object)['fn' => 0],
        ];
    }

    function getDeliveryLinks() {
        $this->deliveryLinks = [
            9 => (object)['link' => 0, 'title' => 'Самовывоз'],
            1 => (object)['link' => 0, 'title' => 'Бесплатная доставка до терминала транспортной компании'],
            2 => (object)['link' => @$this->deliveryVariants->pecom->priceinfo->terminal->truck, 'title' => 'Доставка `ПЭК` до терминала компании', 'address' => 0],
            3 => (object)['link' => @$this->deliveryVariants->pecom->priceinfo->door->truck, 'title' => 'Доставка `ПЭК` до двери', 'address' => 0],
            4 => (object)['link' => @$this->deliveryVariants->pecom->priceinfo->terminal->avia, 'title' => 'Авиадоставка `ПЭК` до терминала компании', 'address' => 0],
            5 => (object)['link' => @$this->deliveryVariants->jd->priceinfo->terminal->truck, 'title' => 'Доставка `ЖелДорЭкспедиция` до терминала', 'address' => 0],
            6 => (object)['link' => @$this->deliveryVariants->bl->priceinfo->terminal->truck, 'title' => 'Доставка `Деловые линии` до терминала', 'address' => 0],
            8 => (object)['link' => @$this->deliveryVariants->bl->priceinfo->door->truck, 'title' => 'Доставка `Деловые линии` до двери', 'address' => 1]
        ];
    }

    function getBusinessLineCost() {

    }

    function getPecomResponse() {
        $deliveryInfo = [
            'places' => [
                [(float)$this->params['length'],(float)$this->params['width'],(float)$this->params['height'],(float)$this->params['volume'],(float)$this->params['weight'], 1] // параметры товара
            ],
            'take' => [
                'town' => self::$shopPoint->external_pecom_id,
                'tent' => 0,
                'gidro' => 0,
                'speed' => 0,
                'manjp' => 0,
                'moscow' => 0
            ],
            'deliver' => [
                'town' => self::$city->external_pecom_id,
                'tent' => 0,
                'gidro' => 0,
                'speed' => 0,
                'manjp' => 0,
                'moscow' => 0
            ],
            'plombir' => 1,
            'pal' => 0,
            'strah' => 50,
            'pallets' => 0,
            'night' => 0,
            'ashan' => 0
        ];
        $this->initBot();
        curl_setopt($this->bot, CURLOPT_URL, 'http://calc.pecom.ru/bitrix/components/pecom/calc/ajax.php?'.http_build_query($deliveryInfo));
        $this->deliveryVariants->pecom->response = \json_decode(curl_exec($this->bot));

        return $this->deliveryVariants->pecom->response;
    }

    function initBot() {
        if($this->bot) { return; }
        $this->bot = curl_init();
        if(proxy) {
            curl_setopt($this->bot, CURLOPT_PROXY, proxy);
        }
        curl_setopt($this->bot, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 5);
    }

    function getJdResponse() {
        $this->initBot();
        $data = [
            'FromCity' => self::$shopPoint->external_jd_id,
            'ToCity' => self::$city->external_jd_id,
            'Weight' => @$this->params['weight'] ? $this->params['weight'] : defaultWeight,
            'Volume' => @$this->params['volume'] ? $this->params['volume'] : (defaultWidth*defaultHeight*defaultLength)/6,
            'DeclaredPrice' => null,
            'CashUse' => 'y',
            'CashPrice' => @$this->params['price'] ? $this->params['price'] : 0,
            'add' => 1
        ];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->bot, CURLOPT_URL, 'https://www.jde.ru/ajax/calcinternet.html');
        $this->deliveryVariants->jd->response = \json_decode(curl_exec($this->bot));
        return $this->deliveryVariants->jd->response;
    }

    function getPecomCost() {
        if(empty(self::$shopPoint->external_pecom_id) || empty(self::$city->external_pecom_id)) { return; }
        $this->getPecomResponse();
    }

    function getJdCost() {
        if(empty(self::$shopPoint->external_jd_id) || empty(self::$city->external_jd_id)) { return; }
        $this->getJdResponse();
    }

    static function getDefaultParams($weight = defaultWeight, $length = defaultLength, $height = defaultHeight, $width = defaultWidth, $volume = 0.3, $price = defaultPrice) {
        return  [
            'weight' => $weight,
            'length' => $length,
            'height' => $height,
            'width' => $width,
            'volume' => $volume,
            'price' => $price
        ];
    }

    function calculateCosts($params) {
        $this->params['weight'] = $params->weight;
        $this->params['length'] = $params->length;
        $this->params['height'] = $params->height;
        $this->params['width'] = $params->width;
        $this->params['volume'] = ($params->width*$params->length*$params->height)/6;
        $this->params['price'] = $params->final_price;
        return $this->getDeliveries();
    }

    static function reformatProductParams(&$params) {
        $params->weight = @$params->weight ? @$params->weight/100 : defaultWeight;
        $params->height = @$params->height ? @$params->height/100 : defaultHeight;
        $params->length = @$params->length ? @$params->length/100 : defaultLength;
        $params->width = @$params->width ? @$params->width/100 : defaultWidth;
    }

    function calculateProductDeliveries() {
        $this->result = new \Result;
        $this->result->product_id = \Request::get('product_id');

        if(!$this->result->product_id || !\anInt($this->result->product_id)) {
            $this->params = self::getDefaultParams();
        } else {
            $params = \DB::table('products')->where('product_id', $this->result->product_id)
            ->select(\DB::raw("weight, height, length, width, ".ProductsModel::getCoalesce()))
            ->first();
            if($params) {
                self::reformatProductParams($params);
            } else {
                $params = self::getDefaultParams();
            }
            calculateProductPrice($params);
            $this->params = self::getDefaultParams($params->weight, $params->length, $params->height, $params->width, ($params->length*$params->height*$params->width)/6, $params->priceinfo['final_price']);
        }

        $this->result->result = 1;
        $this->result->costs = $this->getDeliveries();
        return $this->result;
    }

}

