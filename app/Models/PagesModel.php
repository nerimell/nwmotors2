<?php

namespace App\Models;

class PagesModel extends Model {

    public $page;
    public $breadcrumbs;
    public $activePages;

    function getPageByPath($path) {
        $segments = explode('/', $path);
        if(count($segments) > 1) { return 0; }
        $alias = $segments[0];
        $this->page = \DB::table('pages')->where('alias', $alias)->where('status', 1)->first();
        if(!$this->page) { return 0; }
        $this->page->link = '/'.$this->page->alias;
        $this->breadcrumbs = [(object)['link' => $this->page->link, 'title' => $this->page->breadcrumbs_title ? $this->page->breadcrumbs_title : $this->page->title]];
        $this->activePages = [$this->page->link];
        return $this;
    }


}