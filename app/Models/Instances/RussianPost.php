<?php

class RussianPost {

    public $calculationEntity;
    public $costCalculationEntity;
    public $productPageState;

    function __construct() {
        $this->default();
    }







    function default() {
        $this->calculationEntity = (object)[
            'origin' => (object)[
                'country' => 'Россия',
                'region' => '',
                'district' => '',
                'city' => ''
            ],
            'destination' => (object)[
                'country' => 'Россия',
                'region' => '',
                'district' => '',
                'city' => ''
            ],
            'sendingType' => 'PACKAGE'
        ];

        $this->costCalculationEntity = (object)[
            'postingType' => 'VPO',

        ];
    }

}
/*
[postingType] => VPO
[zipCodeFrom] => 190031
            [zipCodeTo] => 190031
            [postalCodesFrom] => Array
(
)

[postalCodesTo] => Array
(
)

[weightRange] => Array
(
    [0] => 2000
                    [1] => 5000
                )

            [wayForward] => EARTH
[postingKind] => PARCEL
[postingCategory] => WITH_DECLARED_VALUE
[parcelKind] => STANDARD
[declaredValue] => 50000
*/