<?php

namespace App\Models\Instances;

class Cart {

    public $total = 0;
    public $totalDiscount = 0;
    public $products = [];
    public $complexes = [];
    public $instances = [];
    public $currency;
    public $display;
    public $totals;

    function __construct() {
      $this->totals = new \stdClass;
      $this->totals->products = 0;
      $this->totals->base_price = 0;
      $this->totals->final_price = 0;
      $this->totals->discount = 0;
      $this->totals->coupon_discount = 0;
      $this->totals->final_discount = 0;
      $this->totals->instances = 0;
      $this->totals->gifts = 0;
      $this->totals->base_discount = 0;
      $this->totals->width = 0;
      $this->totals->height = 0;
      $this->totals->length = 0;
      $this->totals->weight = 0;

      $this->display = new \stdClass;
  
      $this->currency = new \stdClass;
      $this->currency->sign = ' р.';
    }
}
