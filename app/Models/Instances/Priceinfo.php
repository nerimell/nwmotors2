<?php

namespace App\Models\Instances;

class Priceinfo {

    public $basePrice = 0;
    public $finalPrice = 0;
    public $discountValue = 0;
    public $couponDiscount = 0;
    public $discountPercent = 0;

    function __construct() {

    }

    static function validatePriceinfo($priceinfo) {
        $fields = ['coupon_discount', 'discount', 'discount_value', 'final_price', 'price'];
        //dd($priceinfo);
        foreach($fields as &$v) {
            if(!\anInt(@$priceinfo->{$v})) {  return 0; }
        }
        return 1;
    }

}