<?php

namespace App\Models\Deprecated;

class CategoriesModel extends Model {

    static public $_instance = null;
    public $category;
    public $parents;
    public $childs;
    public $categories;
    public $categoriesById;
    public $categoryTree;
    public $link;
    public $result;
    public $menuonly = 0;

    private function __construct() {

    }


    public static function getInstance() {
        if(!is_null(self::$_instance)) { return self::$_instance; }
        self::$_instance = new self();
        return self::$_instance;
    }

    function getCategoryByAlias($alias) {
        return \DB::table('categories')->where('status', 1)
        ->where('alias', $alias)->first();
    }

    function getSimpleCategoryById($id) {
        return \DB::table('categories') //->where('status', 1)
        ->where('category_id', $id)->first();
    }

    function getCategoriesByIds($ids) {
        if(empty($ids)) { return []; }
        $result = [];
        foreach($ids as $id) {
            $cat = $this->getCategoryById($id);
            if($cat) { $result[] = $cat; }
        }
        return $result;
    }

    function getCategoryParentsByStarterId($id) {
        if(!$id) { return 1; }
        $this->parents = [];
        if(!$this->iterateCategory($id)) { return 0; }
        $this->parents = array_reverse($this->parents);
        return 1;
    }

    function getCategoryById($id) {
          $this->category = $this->getSimpleCategoryById($id);
          if(!$this->category) { return 0; }
          if(!$this->getCategoryParentsByStarterId($this->category->parent)) { return 0; }
          if(!empty($this->parents)) {
            foreach($this->parents as $k => $x) {
              $this->parents[$k]->alias = $this->handleCatName($x->title);
            }
          }
          $this->category->alias = $this->handleCatName($this->category->title);
          $this->category->parents = $this->parents;
          $this->category->childs = $this->getCategoryChildsById($this->category->category_id);
          $this->linkCategories();

        return $this->category;

    }


    function handleCatName($title) {
        $r = str_replace('/', '-', $title);
        return \Str::slug($r);
    }




    function getCategoryByPath($path) {
        $path = trim($path, '/');
        $key = 'categoryByPath_'.$path;
        if(!\Cache::has($key)) {
            $segments = explode('/', $path);
            $segmentsCount = count($segments);
            $this->category = $this->getCategoryByAlias($segments[$segmentsCount-1]);
            if(!$this->category) { return 0; }
            if(!$this->getCategoryParentsByStarterId($this->category->parent)) { return 0; }
            $this->category->parents = $this->parents;
            $this->category->childs = $this->getCategoryChildsById($this->category->category_id);
            $this->linkCategories();
            \Cache::forever('categoryById_'.$this->category->category_id, \json_encode($this->category));
            \Cache::forever($key, \json_encode($this->category));
        }
        $this->category = \json_decode(\Cache::get($key));
        return 1;
    }



    function linkCategories() {
        if(empty($this->category)) { return; }
        $this->category->breadcrumbs = [];
        if(empty($this->category->parents)) {
            $this->category->link = '/'.$this->category->alias;
        } else {
            $this->category->link = '/';
            foreach($this->category->parents as $k => $v) {
                $this->category->link.= $v->alias.'/';
                $this->category->parents[$k]->link = rtrim($this->category->link, '/');
                $this->category->breadcrumbs[] = (object)['link' => $v->link, 'title' => $v->title];
            }
            $this->category->link.=$this->category->alias;
        }
        $this->category->breadcrumbs[] = (object)['link' => $this->category->link, 'title' => $this->category->title];
        if(!empty($this->category->childs)) {
            foreach($this->category->childs as $k => $v) {
                $this->category->childs[$k]->link = $this->category->link.'/'.$v->alias;
            }
        }
    }

    function getCategoryChildsById($id) {
        return \DB::table('categories')->where('status', 1)->orderBy('ordering', 'asc')->where('parent', $id)->get();
    }

    function iterateCategory($id) {
        if(!$id) { return 1; }
        $cat = $this->getSimpleCategoryById($id);
        if(!$cat) { return 0; }
        $this->parents[] = $cat;
        if($cat->parent) {  return $this->iterateCategory($cat->parent); }
        return 1;
    }

    function categoryToResult($id) {
        if(empty($this->categoriesById->{$id})) { return; }
        $this->result[] = $this->categoriesById->{$id};
        if($this->categoriesById->{$id}->parent) {
            $this->categoryToResult($this->categoriesById->{$id}->parent);
        }
        return;
    }


    function getCategoryTree() {
        if($this->categoryTree) { return $this->categoryTree; }
        $this->getAllCategories();
        $postfix = $this->menuonly ? '_menuonly' : '';
        $key = 'categoryTree'.$postfix;
        $key2 = 'categories_by_id'.$postfix;
        if(!\Cache::has($key) || !\Cache::has($key2)) {
            foreach($this->categories as $k => $x) {
                $this->categories[$k]->childs = [];
                $this->categoriesById->{$x->category_id} = &$this->categories[$k];
            }

            foreach($this->categories as $k => $x) {
                $this->categories[$k]->link = $this->getLinkFromTree($this->categories[$k]->category_id);
                if(!$x->parent) {
                    $this->categoryTree[] = &$this->categories[$k];
                    continue;
                }
                if(isset($this->categoriesById->{$x->parent})) {
                    $this->categoriesById->{$x->parent}->childs[] = &$this->categories[$k];
                }
            }
            $clone = $this->categoriesById;
            \Cache::forever($key, \json_encode($this->categoryTree));
            \Cache::forever($key2, \json_encode($clone));
        }
        $this->categoriesById = \json_decode(\Cache::get($key2));
        return \json_decode(\Cache::get($key));
    }






    function getLinkFromTree($id) {
        $this->link = [$this->categoriesById->{$id}->alias];
        if($this->categoriesById->{$id}->parent) {
            $this->iterateLink($this->categoriesById->{$id}->parent);
        }
        return '/'.\implode('/', array_reverse($this->link));
    }

    function iterateLink($id) {
        $this->link[] = $this->categoriesById->{$id}->alias;
        if($this->categoriesById->{$id}->parent) {
            $this->iterateLink($this->categoriesById->{$id}->parent);
        }
    }

    function getAllCategories() {
        if($this->categories) { return $this->categories; }
        $postfix = $this->menuonly ? '_menuonly' : '';
        $key = 'categories'.$postfix;
        if(!\Cache::has($key)) {
            $this->categories = \DB::table('categories')
            ->select('category_id', 'title', 'subtitle', 'avatar', 'status', 'parent', 'alias', 'menu_avatar', 'css', 'ordering', 'menu_title',
            'hide_in_menu')
            ->orderBy('ordering', 'asc')
            ->where('status', true);
            if($this->menuonly) { $this->categories->where('hide_in_menu', 0); }
            $this->categories = $this->categories->get();
            \Cache::forever($key, \json_encode($this->categories));
        }
        return \json_decode(\Cache::get($key));
    }

    function getMenuCategories() {

    }


}
