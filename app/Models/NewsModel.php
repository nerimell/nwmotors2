<?php

namespace App\Models;

class NewsModel extends Model {

    public $breadcrumbs = [];
    public $article;
    public $articles;
    public $params;
    public $total;
    public $data;
    public $perPage = 10;
    public $activePages = [];
    public $activeLinks;

    function getArticleByAlias($alias) {
        $this->article = \DB::table('articles')->where('alias', $alias)->first();
        if(!$this->article) { return 0; }
        $this->article->link = '/news/'.$this->article->alias;
        $this->activeLinks = ['/news'];
        $this->breadcrumbs = [(object)['title' => 'Новости', 'link' => '/news'], (object)['title' => $this->article->title, 'link' => false]];
        return $this;
    }

    function getRecentArticles() {
        $this->collectParams();
        $this->articles = \DB::table('articles');
        $this->total = $this->articles->count();
        $this->articles = $this->articles->take($this->perPage)
            ->skip(($this->params['p']-1)*$this->perPage)
            ->get();
        $this->activeLinks = ['/news'];
        if(!$this->articles) { return 0; }
        foreach($this->articles as $k => $article) {
            $this->articles[$k]->link = '/news/'.$article->alias;
        }
        return $this;

    }

    function collectParams() {
        $this->params = [];
        $this->params['p'] = \Request::get('p');
    }

}