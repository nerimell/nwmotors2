<?php
namespace App\Models;

class SettingsModel {

    public $website;
    public $pageInfo = [];
    function getSiteData() {
        return \json_decode(file_get_contents(public_path().'/../resources/data/settings.json'));
    }

    function shareSiteData($pageInfo = false) {
        if($pageInfo) { $this->pageInfo = $pageInfo; }
        $this->website = $this->getSiteData();
        //dd($this->website);
        $this->website = $this->mergeSiteData($this->website, $this->pageInfo);
        $user = \Auth::user();
        if($user) {
            define('protectedView', '-logged');
        } else {
            define('protectedView', '');
        }
        $footer = json_decode(file_get_contents('../resources/data/footer.json'));
        \View::share(['website' => $this->website, 'user' => $user, 'footer' => $footer]);
    }

    function mergeSiteData($a, $b) {
        $a->meta_title = (!empty($b->meta_title)) ? strip_tags($b->meta_title).' - '.$a->title : $a->title;
        $a->meta_description = (!empty($b->meta_description)) ? strip_tags($b->meta_description) : $a->meta_description;
        $a->meta_keywords = (!empty($b->meta_keywords)) ? strip_tags($b->meta_keywords) : $a->meta_keywords;
        return $a;
    }

}