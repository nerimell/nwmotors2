<?php

namespace App\Models\Schedules;
use App\Models\ProductsModel;

class Vkontakte extends Schedule {


    private $bot;
    private $access_token;
    private $owner_id;
    private $group_id;
    private $inited = false;
    private $proxy = 0;
    public $offset;
    public $productsModel;
    public $realLink;

    function dev($settings) {
        $this->proxy = 'https://ama:365478@192.168.1.3:3128/';
        $this->init($settings);
        $this->uploadPost();
    }

    function init($settings) {
        $this->bot = curl_init();
        curl_setopt($this->bot, CURLOPT_RETURNTRANSFER, 1);
        $this->getUserAgents();
        $agent = array_rand($this->agents, 1);
        curl_setopt($this->bot, CURLOPT_USERAGENT, $agent);
        curl_setopt($this->bot, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->bot, CURLOPT_SSL_VERIFYHOST, 2);
        $this->owner_id = '-' . @$settings->owner_id;
        $this->group_id = @$settings->owner_id;
        $this->access_token = @$settings->access_token;
        if($this->proxy) { curl_setopt($this->bot, CURLOPT_PROXY, $this->proxy); }
        $this->inited = true;

        include_once(public_path().'/../resources/data/constants.php');
    }

    function getTagsArray() {
        return [
            '#лодки_ПВХ',
            '#лодки_флинк',
            '#мнев',
            '#лодки_мнев',
            '#моторы_меркури',
            '#mercury_мотор',
            '#лодочный_мотор',
            '#мотор_меркрузер',
            '#меркрузер',
            '#Mercruiser',
            '#Volvo_Penta',
            '#запчасти_лодочный_мотор',
            '#купить_лодку_пвх',
            '#запчасти_меркури'
        ];
    }

    function getTags() {
        $a = $this->getTagsArray();
        shuffle($a);
        $final = [$a[0], $a[1], $a[2]];
        return $final;
    }

    public function getUserAgents() {
        $doc = simplexml_load_file("resources/schedules/agents.xml");
        return $this->agents = (array)$doc->agent;
    }


    function console($arg) {
        echo $arg.PHP_EOL;
    }

    function uploadPost() {

        if(!$this->inited) { $this->init(); }
        $product = $this->getVkontakteProduct();
        calculateProductPrice($product);
        $this->console('step 1');
        if(!$product) { return false; }
        $this->console('step 2');
        $url = 'https://api.vk.com/method/photos.getWallUploadServer?';
        $qs = 'group_id='.$this->group_id.'&access_token='.$this->access_token;
        curl_setopt($this->bot, CURLOPT_URL, $url.$qs);
        $server = \json_decode(curl_exec($this->bot));
       
        $server = @$server->response->upload_url;
        if(!$server) { return 0; }
        $this->console('step 3');
        curl_setopt($this->bot, CURLOPT_POST, 1);

        $post_params = array(
            "photo" => new \CURLFile($product->avatar),
        );
        $headers = array("Content-Type:multipart/form-data");
        curl_setopt($this->bot, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->bot, CURLOPT_URL, $server);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post_params);
        $photos = \json_decode(curl_exec($this->bot));
        $this->console('step 4');

        $url = 'https://api.vk.com/method/photos.saveWallPhoto?';
        $qs = 'group_id='.$this->group_id.'&server='.$photos->server.'&hash='.$photos->hash.'&photo='.$photos->photo.'&access_token='.$this->access_token;
        curl_setopt($this->bot, CURLOPT_URL, $url.$qs);
        $uploaded = \json_decode(curl_exec($this->bot));
        $this->console('step 5');
        if(!@$uploaded->response[0]->id) { echo 'no response id<br>'; return 0; }
        $url = 'https://api.vk.com/method/wall.post?';
        $out = $this->buildArticle($product);
        curl_setopt($this->bot, CURLOPT_URL, $url);
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 15);
        curl_setopt($this->bot, CURLOPT_POST, 1);
        $postfields = [
            'owner_id' => $this->owner_id,
            'from_group' => 1,
            'access_token' => $this->access_token,
            'attachments' => $uploaded->response[0]->id,
            'message' => $out,
            'title' => $product->title
        ];
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $postfields);
        $this->console('step 7');
        curl_exec($this->bot);
        $status = curl_getinfo($this->bot, CURLINFO_HTTP_CODE);
        try {
            \DB::table('products_smm')->insert(['product_id' => $product->product_id, 'vk' => 1]);
        } catch(\PDOException $e) {
            \DB::table('products_smm')->where('product_id', $product->product_id)->update(['vk' => 1]);
        }
    }

    function buildArticle($product) {
        $out = '';
        $product->description = strip_tags($product->description);
        $descLength = mb_strlen($product->description);
        if($descLength > 350) {
            $product->description = mb_substr($product->description, 0, 350).'...';
        }
        $out.= $product->title.PHP_EOL.PHP_EOL;
        $out.= $product->description.PHP_EOL.PHP_EOL;
        $out.= 'Цена: '.number_format($product->priceinfo['final_price']).' рублей'.PHP_EOL.PHP_EOL;
        $out.= 'http://nwmotors.ru/'.$product->old_link.'-detail'.PHP_EOL.PHP_EOL;
        $out.= join(' ', $this->getTags()).PHP_EOL;
        return $out;
    }

    function prepareProduct() {
        $prod = $this->queryproduct();
        if(!$prod) { return 0; }
        if(!$this->productsModel->getProductById($prod)) { return 0; }
        $this->productsModel->product->avatar = '../storage/public/i/nwmotors/products/resized/'.$this->productsModel->product->avatar;
        return $this->productsModel->product;
    }

    function getVkontakteProduct() {
        $this->offset = 0;
        $this->productsModel = ProductsModel::getInstance();
        return $this->prepareProduct();
    }

    function queryProduct() {
        if($this->offset > 5000) { return 0; }
        $prod = \DB::table('products')
        ->select('product_id', 'products.old_link', 'products.avatar')
        ->whereNotNull('products.old_link')
        ->where('products.status', true)
        ->whereNotNull('products.avatar')
        ->skip($this->offset)
        ->first();
        if(!$prod) { return 0; }
        $avatar = '../storage/public/i/nwmotors/products/'.$prod->avatar;
        if(!is_file($avatar)) {
            $this->offset++;
            return $this->queryProduct();
        }
        if(\DB::table('products_smm')->where('product_id', $prod->product_id)->where('vk', 1)->first()) {
            $this->offset++;
            return $this->queryProduct();
        }
        return $prod->product_id;
    }








}

// https://oauth.vk.com/authorize?client_id=6160099&scope=wall,offline,groups,photos&response_type=token&v=5.68&state=123456