<?php

namespace App\Models\Schedules\EzPartsCatalogParser;

class EzPartsCatalogParser {

    public $bot;
    private $bots = [];
    private $threadBot;
    public $defaultHeaders;
    private $agents;
    private $cookie;
    public $lastHeaders;
    public $errors = [];
    public $productErrors = [];
    public $noProductResources = [];
    public $data = [];
    public $lastProxy;
    public $ignoreExists = 1;
    public $proxy = 1;
    public $cookiepath;
    public $cookiesTmp;
    public $proxies = [
        '192.99.222.207:8080'
    ];


    function __construct() {
        $this->proxy = 1;
        $this->init();
        $this->initBot($this->bot);
        $this->cookiepath = public_path().'/../resources/schedules/cookies/EzPartsParser.txt';
        $this->cookiesTmp = public_path().'/../resources/schedules/cookies/tmp/ezPartsParser/';
    }

    function auth(&$bot) {
        curl_setopt($bot, CURLOPT_URL,
  'https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmerica&company=Guest&NA_KEY=NA_KEY_VALUE&langIF=eng&langDB=eng');
        curl_setopt($bot, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate, sdch, br',
            'Accept-Language: ru,en-US;q=0.8,en;q=0.6',
            'Connection: keep-alive',
            'Host: public-mercurymarine.sysonline.com',
            'Referer: https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmerica&company=Guest&NA_KEY=NA_KEY_VALUE&langIF=eng&langDB=eng',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'
        ]);

        curl_setopt($bot, CURLOPT_HTTPGET, 1);
        curl_setopt($bot, CURLOPT_ENCODING , "gzip");
        $auth = curl_exec($bot);

    }

    function init() {
        $this->bot = curl_init();
        $this->agents = (array)simplexml_load_file(public_path().'/../resources/schedules/agents.xml');
        $this->cookie = realpath($this->cookiepath);
    }

    function initBot(&$bot) {
        curl_setopt($bot, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($bot, CURLOPT_FOLLOWLOCATION, 1);
        $this->setRandomCookie($bot);
        curl_setopt($bot ,CURLOPT_TIMEOUT, 100000);
        curl_setopt($bot, CURLOPT_TIMEOUT, 20);
        curl_setopt($bot, CURLOPT_CONNECTTIMEOUT ,0);
        $this->getProxy($bot);
    }

    function getSerials() {
        $serials = \DB::table('outsource_ezpartscatalog_serials')
        ->orderBy('id', 'desc')
        ->take(1000)->skip(rand(0, 750000))->whereNull('response_status')->get();
        $this->init();
        $this->initBot($this->bot);
        $this->auth($this->bot);
        foreach($serials as $k => $v) {
            $this->getSerialNodes($v);
        }
    }



    function getSerialNodes($v) {
        $v->serial .='\r';
        $v->serial = trim(str_replace('\r', '', $v->serial));
        $post = '{"serialGroupPtr":"25005299597313","serialNumber":"'.$v->serial.'","serialGroupName":"All serials"}';
        $opts = ['X-AjaxPro-Method' => 'getSerialResult', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $decoded = \json_decode($response);
        \DB::table('outsource_ezpartscatalog_serials')
            ->where('id', $v->id)
            ->update(['serial' => $v->serial, 'response' => $response, 'response_status' => @$decoded->value->Serials ? 1 : 0]);
    }

    function setHeaders($opts = false, $bot) {
        $this->defaultHeaders = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, sdch, br',
            'Accept-Language' => 'en,en-GB;q=0.8,en;q=0.6',
            'Connection' => 'keep-alive',
            'Content-Type' => 'text/plain; charset=UTF-8',
            'Host' => 'public-mercurymarine.sysonline.com',
            'Origin' => 'https://public-mercurymarine.sysonline.com',
            'Referer' => 'https://public-mercurymarine.sysonline.com/Default.aspx?sysname=NorthAmericacompany=GuestNA_KEY=NA_KEY_VALUElangIF=englangDB=eng',
            'User-Agent' => $this->getAgent(),
        ];
        $this->lastHeaders = $opts ? $this->mergeHeaders($opts) : $this->defaultHeaders;
        $headers = [];
        foreach($this->lastHeaders as $k => $v) {
            $headers[] = $k.': '.$v;
        }
        curl_setopt($bot, CURLOPT_HTTPHEADER, $headers);
    }

    function mergeHeaders($opts) {
        $headers = $this->defaultHeaders;
        foreach($opts as $k => $v) {
            $headers[$k] = $v;
        }
        return $headers;

    }

    function getAgent() {
        return $this->agents[array_rand($this->agents, 1)][0];
    }

    function getProxy(&$bot) {
        if(!$this->proxy) { return; }
        $this->lastProxy = $this->proxies[array_rand($this->proxies)];
        curl_setopt($bot, CURLOPT_PROXY, 'https://'.$this->lastProxy.'/');
    }

    function request($opts, &$bot) {
        $this->setHeaders($opts, $bot);
        $this->getProxy($bot);
        $response = curl_exec($bot);
        $status = curl_getinfo($bot, CURLINFO_HTTP_CODE);
        if($status == 200) {
            return $response;
        }
        return false;
    }

    function getPartByCode($code, $bot) {
        $post = json_encode(['searchCondition' => [$code, 'PartsByCode', null, null, false]]);
        $opts = ['X-AjaxPro-Method' => 'getSearchResult', 'Accept' => '*/*'];
        curl_setopt($bot, CURLOPT_POST, 1);
        curl_setopt($bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($bot ,CURLOPT_ENCODING , "gzip");
        $response = \json_decode($this->request($opts, $bot));
        if(!$response || !@count($response->value->PartsByCode)) { $this->productErrors[$code] = 1; return false; }
        $product = 0;
        foreach($response->value->PartsByCode as $x) {
            if($x[1][0] != $code) { continue; }
            $product = new \stdClass;
            $product->ptr = $x[0];
            $product->code = $x[1][0];
            $product->title = $x[1][1];
        }
        if(!$product) { $this->productErrors[$code] = 1; return false; }
        return $product;
    }

    function setOptPartQuery($code, $bot) {
        $post = json_encode(['searchCondition' => [$code, 'PartsByCode', null, null, false]]);
        $opts = ['X-AjaxPro-Method' => 'getSearchResult', 'Accept' => '*/*'];
        curl_setopt($bot, CURLOPT_POST, 1);
        curl_setopt($bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($bot ,CURLOPT_ENCODING , "gzip");
        $this->setHeaders($opts, $bot);
    }

    function getResourcesOfPart($partId) {
        $post = json_encode(['partPtr' => $partId, "curModelPtr" => null]);
        $opts = ['X-AjaxPro-Method' => 'getPartUsingInResources', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        if(!$response || !@count(@$response->value->DataCache)) { $this->noProductResources[$partId] = 1; return false; }
        return $response;
    }

    function getResourcesOfParts() {
        $parts = \DB::table('parser_ezpartscatalog_parts')->whereNull('resources_extracted')->get();
        $this->auth($this->bot);
        foreach($parts as $x) {
            $resources = $this->getResourcesOfPart($x->ptr);
            if(!$resources) {
                \DB::table('parser_ezpartscatalog_parts')->where('ptr', $x->ptr)->update(['resources_extracted' => date('Y-m-d H:i:s'), 'error' => 1]);
                continue;
            }
            foreach($resources->value->DataCache as $resource) {
                $obj = ['part_ptr' => $x->ptr, 'part_path_ptr' => $resource[1][0][1]];
                try {
                    \DB::table('parser_ezpartscatalog_resources_of_parts')->insert($obj);
                } catch(\PDOException $e) {
                    echo $e->getMessage(); die;
                    continue;
                }
                \DB::table('parser_ezpartscatalog_parts')->where('ptr', $x->ptr)->update(['resources_extracted' => date('Y-m-d H:i:s')]);
            }
        }
    }

    function createTmpCookie() {
        $filename = date('H-i-s');
        $path = $this->cookiesTmp.$filename.'.txt';
        @unlink($path);
        $file = fopen($path, 'w+');
        fclose($file);
        chmod($path, 0777);
        return $path;
    }

    function clear() {
        $files = scandir($this->cookiesTmp);
        $files = array_diff($files, ['.', '..']);
        foreach($files as $x) {
            @unlink($this->cookiesTmp.$x);
        }
    }

    function setRandomCookie($bot) {
        $tmpCookie = realpath($this->createTmpCookie());
        curl_setopt($bot, CURLOPT_COOKIEFILE, $tmpCookie);
        curl_setopt($bot, CURLOPT_COOKIEJAR, $tmpCookie);
    }

    function getPartsByCodes($filepath) {
        $partCodes = \json_decode(file_get_contents($filepath));
        $existing = $this->ignoreExists ? \arrayMap(\DB::table('parser_ezpartscatalog_parts')->get(), 'code') : 0;
        foreach($partCodes as $code) {
            if($this->ignoreExists && isset($existing[$code])) { continue; }
            // for multithread
            if($this->ignoreExists && \DB::table('parser_ezpartscatalog_parts')->where('code', $code)->first()) { continue; }
            $part = $this->getPartByCode($code, $this->bot);
            if(!$part) { continue; }
            try {
                \DB::table('parser_ezpartscatalog_parts')->insert((array)$part);
            } catch(\PDOException $e) {
                echo $e->getMessage(); echo '<br>';
            }
        }


        $this->clear();

    }

    function getDataByPartCodes($filepath) {
        $this->auth($this->bot);
        $this->getPartsByCodes($filepath);
    }

    function translateParts() {
        $json = \json_decode(file_get_contents(public_path().'/../resources/schedules/ezPartsParser/partsRus.json'));
        if(!$json) { return; }
        foreach($json as $k => $v) {
           \DB::table('parser_ezpartscatalog_parts')->where('ptr', $v->Part_Ptr)->update(['title' => $v->Description]);
        }
    }

    function devx() {
        $this->auth($this->bot);
        $schemes = \DB::table('products')
            ->select('products.product_id', 'outsource_ezpartscatalog_nodes.*')
            ->join('outsource_ezpartscatalog_nodes', 'outsource_ezpartscatalog_nodes.ptr', '=', 'products.ptr')
            //->whereRaw(\DB::raw('resource_data_parsed > current_date - 1'))
           // ->where('resource_data', '<>', '[]')
            //->take(10000)
            ->get();
        $i = 0;
        if(empty($schemes)) { return; }
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 20);
        foreach($schemes as $x) {
            $isParent = \DB::table('product_consistance')->where('parent_product', $x->product_id)->get();
            if ($isParent->count()) {
                continue;
            }
            $i++;
            //$this->parseNodeResourceData($x);
           // continue;
            $this->parseNodeDataGroups($x);

        }
        echo $i;
    }

    function parseNodeResourceData($x) {
        $post = '{"nodePtr":"'.$x->ptr.'"}';
        $opts = ['X-AjaxPro-Method' => 'getLoadableResourcesInfo', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxTreeControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode(@$response->value);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['resource_data' => $update, 'resource_data_parsed' => date('Y-m-d H:i:s'), 'resource_data_http_code' => 334]);
        return $response;
    }

    function findEmptySchemes() {
        $schemes = \DB::table('products')
            ->join('outsource_ezpartscatalog_nodes', 'outsource_ezpartscatalog_nodes.ptr', '=', 'products.ptr')
            ->get();
        $i = 0;
        foreach($schemes as $x) {
            $isParent = \DB::table('product_consistance')->where('parent_product', $x->product_id)->get();
            if($isParent->count()) { continue; }
            $i++;
            echo $x->product_id;
            echo '<br>';
        }
        echo $i;
    }


    function parseNodesResourceData() {
        $this->auth($this->bot);
        $schemes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->where('outsource_ezpartscatalog_nodes.ptr', '24554328205155')
            ->get();
        if(empty($schemes)) { return; }
        curl_setopt($this->bot, CURLOPT_TIMEOUT, 20);
        foreach($schemes as $x) {
            $this->parseNodeResourceData($x);
        }
    }

    function parseDataGroups() {
            $this->auth($this->bot);
            $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id', 'outsource_ezpartscatalog_nodes.data_group',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code', 'outsource_ezpartscatalog_nodes.resource_data')
                ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
                ->get();
            foreach($nodes as $x) {
                $dg = \json_decode($x->data_group);
                if(!empty($dg->value[0]->DataCache)) { continue; }
                if(!empty($dg->error)) { continue; }
                $this->parseNodeDataGroups($x);
            }
            $i = 0;
            echo $i;
    }

    function parseHotSpots() {
        $this->auth($this->bot);
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
        ->select('products.ptr', 'products.avatar', 'products.product_id',
            'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code',
            'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->get();
        foreach($nodes as $x) {
            $this->parseHotSpotsByNode($x);
        }
    }

    function parseHotSpotsByNode($x) {
        $resourceData = \json_decode($x->resource_data);
        if(!@$resourceData[0]->ResourcePtr) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['hotspot_scanned' => date('Y-m-d H:i:s')]);
            return;
        }
        $post = json_encode(['resourcePtr' => (string)$resourceData[0]->ResourcePtr]);
        $opts = ['X-AjaxPro-Method' => 'getSchematicOptions', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxPartsGridControl,App_Web_5o1fpeuv.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode($response);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group' => $update, 'data_group_scanned' => date('Y-m-d H:i:s'), 'data_group_http_code' => curl_getinfo($this->bot, CURLINFO_HTTP_CODE)]);
        return $response;
    }

    function parseNodeDataGroups($x) {
        $resourceData = \json_decode($x->resource_data);
        if(!@$resourceData[0]->ResourcePtr) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group_scanned' => date('Y-m-d H:i:s'), 'data_group_http_code' => 999]);
            return;
        }
        //dd($resourceData);
        $post = json_encode(['resourcePtr' => (string)$resourceData[0]->ResourcePtr]);
        $opts = ['X-AjaxPro-Method' => 'getDataGroups', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxPartsGridControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        $update = \json_encode($response);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group' => $update, 'data_group_scanned' => date('Y-m-d H:i:s'), 'data_group_http_code' => curl_getinfo($this->bot, CURLINFO_HTTP_CODE)]);
        return $response;
    }

    function unpackDataGroups() {
        $dg = \DB::table('outsource_ezpartscatalog_nodes')->where('data_group_http_code', 200)
            ->get();
        foreach($dg as $x) {
            $dataGroup  = \json_decode($x->data_group);
            if(!empty($dataGroup->error)) { continue; }
            if(empty($dataGroup->value[0]->DataCache)) { continue; }
            $values = $dataGroup->value[0]->DataCache;
            dd($values);
        }

    }

    function handleDataGroupInfo() {
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->whereNotNull('data_group')
            ->where('ptr', '24554331832806')
            ->get();

        foreach($nodes as $x) {
            $rd = \json_decode($x->data_group);
            $parentProduct = \DB::table('products')->where('ptr', $x->ptr)->first();
            if(!$parentProduct) { continue; }
            if(!empty($rd->error)) { continue; }
            if(empty($rd->value[0]->DataCache)) { continue; }
            foreach($rd->value[0]->DataCache as $k => $v) {
                $displayData = $v[1]; // данные для дисплея
                $partData = $v[3]; // данные о запчасти
                $status = $v[4];
                if($partData->PartPtr != '24588687801626') {
                    continue;
                }
                $part = \DB::table('products')->where('ptr', $partData->PartPtr)->first();
                if(!$part) { continue; }
                echo $part->product_id.PHP_EOL;
                try {
                    \DB::table('product_consistance')
                        ->insert(['parent_product' => $parentProduct->product_id,
                            'child_product' => $part->product_id,
                            'ordering' => $k,
                            'consistance_quantity' => $displayData[5],
                            'scheme' => \json_encode([
                                'number' => $displayData[2],
                                'hint' => $displayData[7],
                                'status' => $status
                            ])
                        ]);
                } catch(\PDOException $e) {
                    dd($e->getMessage());
                 }
                /*
                    $displayData[2];  номер на схеме
                    $displayData[3];  Класс запчасти
                    $displayData[4];  Артикул запчасти
                    $displayData[5];  Количество
                    $displayData[6];  название
                    $displayData[7];  Примечание
                    $partData->PartPtr;  Птр запчасти
                */
            }
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['data_group_unpacked' => 1]);
        }



    }

    function getSchematicImages() {
        $this->i = 0;
        $this->auth($this->bot);
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')
            ->select('products.ptr', 'products.avatar', 'products.product_id',
                'outsource_ezpartscatalog_nodes.resource_data_http_code', 'outsource_ezpartscatalog_nodes.data_group_http_code',
                'outsource_ezpartscatalog_nodes.resource_data')
            ->join('products', 'products.ptr', '=', 'outsource_ezpartscatalog_nodes.ptr')
            ->get();
        foreach($nodes as $x) {
            if(empty($x->avatar)) { continue; }
            if(!is_file('../../nwstorage/public/i/nwmotors/products/catalog/'.$x->avatar)) {
                $this->getSchematicImage($x);
            }
        }
        echo 'done';
    }

    function getSchematicImage($x) {
        $jrd = \json_decode($x->resource_data);
        if(empty($jrd[0]->ResourceVersionPtr)) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_http_code' => 999]);
            return;
        }
        // {"schematicPtr":"24558623283447","schematicScale":100}
        $post = json_encode(['schematicPtr' => $jrd[0]->ResourcePtr, 'schematicScale' => 100]);
        $opts = ['X-AjaxPro-Method' => 'getSchematicImage', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSchematicControl,App_Web_rxddsmh4.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $update = \json_encode($response);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_http_code' => curl_getinfo($this->bot, CURLINFO_HTTP_CODE), 'avatar_data' => $update]);
        $preg = preg_match("/('(.*?)')/s", $response, $matches);
        if(!count($matches)) { return; }
        $first = trim($matches[2], "'");
        curl_setopt($this->bot, CURLOPT_HTTPGET, 1);
       // curl_setopt($this->bot, CURLOPT_HEADER, 1);
        curl_setopt($this->bot,CURLOPT_POSTFIELDS, null);
        $url = 'https://public-mercurymarine.sysonline.com/ajaximage/'.$first.'.ashx';
        curl_setopt($this->bot, CURLOPT_URL, $url);
        curl_setopt($this->bot ,CURLOPT_ENCODING , null);
        $opts = ['X-Requested-With' => 'XMLHttpRequest', 'Accept' => 'image/webp,image/*,*/*;q=0.8'];
        $response = $this->request($opts, $this->bot);
        echo $url;
        echo '<br>';
        file_put_contents(public_path().'/../resources/dev/resource_avatars/'.$first.'.jpg', $response);
        \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(['avatar_downloaded' => $first.'.jpg']);
        print_r($jrd);
        echo '<br><br>';
        $this->i++;
        if($this->i == 4) {
            //dd($response);
        }
        return $response;
        // отправляем ресурс версион птр
    }




    function getNodeHotSpots($x) {
        $data = \json_decode($x->data);
        if(empty($data->value[0]->ResourcePtr)) { $this->i++;  }
        return;
        $post = json_encode(['resourcePtr' => $pa, "curModelPtr" => null]);
        $opts = ['X-AjaxPro-Method' => 'getHotSpots', 'Accept' => '*/*'];
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxSearchControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
        $response = \json_decode($response);
        if(!$response || !@count(@$response->value->DataCache)) { $this->noProductResources[$partId] = 1; return false; }
        return $response;
    }

    function getNodeResources() {
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')->whereNull('ignore')->whereNull('data')
        ->whereNull('data_scanned')->where('is_final', 1)->orderBy('ptr', 'desc')
        //->where('template', '24708946853889')
        ->get();
        $this->auth($this->bot);

        foreach($nodes as $x) {
            \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $x->ptr)->update(
            ['data' => $this->getNodeResource($x), 'data_scanned' => date('Y-m-d H:i:s')]);
        }
    }

    function getNodeResource($node) {
        $post = json_encode(['nodePtr' => $node->ptr]);
        $opts = ['X-AjaxPro-Method' => 'getLoadableResourcesInfo'];
        curl_setopt($this->bot, CURLOPT_POST, 1);
        curl_setopt($this->bot, CURLOPT_POSTFIELDS, $post);
        curl_setopt($this->bot, CURLOPT_URL, 'https://public-mercurymarine.sysonline.com/ajaxpro/Controls_AjaxTreeControl,App_Web_r5ymwjnn.ashx');
        curl_setopt($this->bot ,CURLOPT_ENCODING , "gzip");
        $response = $this->request($opts, $this->bot);
       // print_r($response); die;
        $response = \json_decode($response);
        return $response;
    }


}

