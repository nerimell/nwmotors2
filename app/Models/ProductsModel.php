<?php

namespace App\Models;
use App\Models\ManufacturerModel;

class ProductsModel extends Model {

    // common
    public $params;
    public $page;
    public $total;
    protected $q;
    public $path;
    public $simplePath;
    public $breadcrumbs;

    // products
    public $product;
    public $products;
    public $productAlias;
    public $namespaces;
    public $manufacturer;
    public $partOfAnother;
    public $priceTable = "products.priceinfo->'default'";
    // categories
    public $categories;
    public $category;
    public $pathElements;
    public $activeCategories = [];
    public $cm;
    public $allowedOrdering = [];
    public static $_instance = null;


    private function __construct() {
        $this->cm = CategoriesModel::getInstance();
    }

    public static function getInstance() {
        if(is_object(self::$_instance)) { return self::$_instance; }
        return self::$_instance = new self;
    }


    function getProductById($id) {
        $this->queryProduct();
        $this->product->where('products.product_id', $id);
        $this->product = $this->product->first();
        if(!$this->product) { return 0; }
        $this->completeProductData();
        return 1;
    }

    static function getProductCodesById($id) {
        $codes = \DB::table('product_codes')->select('product_code')->where('product_id', $id)->get();
        if(!$codes) { return null; }
        $result = [];
        foreach($codes as $x) {
            $result[] = $x->product_code;
        }
        return $result;
    }



    function queryProduct() {
        $this->product = \DB::table('products')
            ->select(\DB::raw("products.*, ".self::getCoalesce()." as priceinfo, links.link"))
            ->join('links', 'links.id', '=', 'products.link_id');
            if(!defined('debug')) { $this->product->where('products.status' ,1); }
    }

    function getProductComplexes($id) {
        // retrieve complexes
        $key = 'complexes_of_'.$id;
        if(!\Cache::has($key)) {
            $sqlComplexes = \DB::select("SELECT products.title, products.gifts, products.avatar, products.product_id, ".self::getCoalesce()." as priceinfo, links.link, 
            product_complex_consistance.complex_id, product_complex_consistance.ordering, product_complexes.discount,
            products.width, products.height, products.length, products.weight
            FROM products, links, product_complex_consistance, product_complexes
            WHERE products.link_id = links.id AND product_complex_consistance.product_id = products.product_id AND 
             product_complexes.complex_id = product_complex_consistance.complex_id AND products.product_id IN 
            (SELECT product_id FROM product_complex_consistance WHERE complex_id IN
            (SELECT DISTINCT(complex_id) FROM product_complex_consistance WHERE product_id = ".$id."))");
            $complexes = self::handleSqlComplexes($sqlComplexes, $id);
            \Cache::put($key, 120, \json_encode($complexes));
            return $complexes;
        }
        return \json_decode(\Cache::get($key));
    }


    static function handleSqlComplexes($sqlComplexes, $idIgnore = 0) {
        if(empty($sqlComplexes)) { return 0; }
        $complexesById = [];
        $total = count($sqlComplexes);
        for($i = 0; $i < $total; $i++) {
            if(!isset($complexesById[$sqlComplexes[$i]->complex_id])) { $complexesById[$sqlComplexes[$i]->complex_id] = []; }
            $complexesById[$sqlComplexes[$i]->complex_id][$sqlComplexes[$i]->ordering] = &$sqlComplexes[$i];
        }
        foreach($complexesById as $complexId => $complexArr) {
            $total = count($complexArr);
            if($total < 2) { unset($complexesById[$complexId]); continue; }
            $title = '';
            $products = [];
            $gifts = [];
            $width = 0;
            $height = 0;
            $length = 0;
            $weight = 0;
            for($i = ($total-1); $i >= 0; --$i) {
                $plus = $i ? ' + ' : '';
                $product = array_shift($complexArr);
                handleProductLink($product);
                $product->avatar = \Html::productThumbnailPath($product->avatar);
                $product->giftsData = \json_decode(@$product->gifts);
                if(!empty($product->giftsData)) {
                    array_merge($gifts, $product->giftsData->products);
                }
                if($product->product_id != $idIgnore) {
                    $title.=$product->title.$plus;
                    $products[] = (object)['product_id' => $product->product_id, 'title' => $product->title, 'link' => $product->link, 'priceinfo' => $product->priceinfo, 'discount' => $product->discount, 'avatar' => $product->avatar];
                } else {
                    $titleLength = mb_strlen($title);
                    $lastChars = mb_substr($title, $titleLength - 3, $titleLength);
                    if($lastChars == ' + ') {
                        $title = mb_substr($title, 0, $titleLength - 3);
                    }
                    $products[] = (object)['product_id' => $product->product_id, 'title' => $product->title, 'gifts' => [], 'giftsData' => $product->giftsData, 'link' => $product->link, 'priceinfo' => $product->priceinfo, 'discount' => $product->discount, 'avatar' => $product->avatar, 'ignore' => 1];
                }
                $width += $product->width ? $product->width : defaultWidth;
                $height += $product->height ? $product->height : defaultHeight;
                $length += $product->length ? $product->length : defaultLength;
                $weight += $product->weight ? $product->weight : defaultWeight;
            }

            if(!empty($gifts)) {
                $gifts = ProductsModel::getGiftsByIds($gifts);
                ProductsModel::linkGiftsWithInstances($gifts, $products);
            }
            $complexesById[$complexId] = (object)['width' => $width, 'height' => $height, 'length' => $length, 'weight' => $weight, 'title' => $title, 'products' => $products, 'complex_id' => $complexId, 'productsCount' => $total];
            calculateComplexPrice($complexesById[$complexId]);
        }
        $complexes = [];
        $ids = [];
        $i = 0;
        foreach($sqlComplexes as $k => $x) {
            if(!isset($complexesById[$x->complex_id]) || in_array($x->complex_id, $ids)) { continue; }
            $ids[] = $x->complex_id;
            $complexes[$i] = $complexesById[$x->complex_id];
            $i++;
        }
        return $complexes;
    }

    static function linkGiftsWithInstances(&$gifts, &$instances) {
        if(empty($gifts)) { return; }
        $total = count($instances);
        if(!$total) { return; }
        $giftsByIds = [];
        foreach($gifts as $k => $x) {
            $giftsByIds[$x->product_id] = $gifts[$k];
        }
        for($i = 0; $i < $total; $i++) {
            if(empty($instances[$i]->giftsData->products)) { continue; }
            $instances[$i]->gifts = [];
            $instances[$i]->giftsById = [];
            foreach($instances[$i]->giftsData->products as $gdk =>  $x) {
                if(!@$giftsByIds[$x]) { unset($instances[$i]->giftsData->$gdk); continue; }
                $instances[$i]->giftsById[$x] = $giftsByIds[$x];
                $instances[$i]->gifts[] = &$instances[$i]->giftsById[$x];
            }
        }
    }


    static function getGiftsByIds($ids) {
        if(empty($ids)) { return 0; }
        return \DB::table('products')
            ->select(\DB::raw("products.title, products.avatar, links.link, products.product_id, ".self::getCoalesce()." as priceinfo"))
            ->where('products.status', true)
            ->join('links', 'products.link_id', '=', 'links.id')
            ->whereIn('products.product_id', $ids)
            ->get();
    }

    static function getProductGifts(&$product) {
        $product->giftsData = \json_decode($product->gifts);
        $product->gifts = [];
        $product->giftsById = [];
        if(empty(@$product->giftsData->products)) { return; }
        $gifts = self::getGiftsByIds($product->giftsData->products);
        $total = $gifts->count();
        if(!$total) { return; }
        $product->giftsById = [];
        for($i = 0; $i < $total; $i++) {
            $product->giftsById[$gifts[$i]->product_id] = &$gifts[$i];
        }
        $product->gifts = [];
        foreach($product->giftsData->products as $x) {
            if(empty($product->giftsById[$x])) { continue; }
            handleProductLink($product->giftsById[$x]);
            calculateProductPrice($product->giftsById[$x]);
            $product->gifts[] = &$product->giftsById[$x];
        }
    }


    function completeProductData() {
        $this->product->codes = self::getProductCodesById($this->product->product_id);
        $this->product->manufacturer = ManufacturerModel::getSimpleManufacturer($this->product->manufacturer_id);
        $this->product->complexes = $this->product->sells ? $this->getProductComplexes($this->product->product_id) : 0;
        self::getProductGifts($this->product);
        $categoryIds = \DB::table('product_categories')->where('product_id', $this->product->product_id)->get();
        $this->categories = 0;
        $this->product->data = \json_decode($this->product->data);
        $this->product->display = new \stdClass;
        $this->product->display->width = $this->product->width ? (float)$this->product->width . ' ' .yardstick. ' ': '-';
        $this->product->display->length = $this->product->length ? (float)$this->product->length . ' ' .yardstick. ' ' : '-';
        $this->product->display->height = $this->product->height ? (float)$this->product->height . ' ' .yardstick. ' ' : '-';
        if(!$categoryIds->count()) {
            $this->categories = [];
        } else {
            if($categoryIds->count() > 1) {
                $catIds = [];
                foreach($categoryIds as $x) { $catIds[] = $x->category_id; }
                $this->categories = $this->cm->getCategoriesByIds($catIds);
            } else {
                if(!$this->cm->getCategoryById($categoryIds[0]->category_id)) { return 0; }
                $this->categories = [$this->cm->category];
                $this->activeCategories[] = $this->cm->category->category_id;
                $this->arrayToActiveCategories([@$this->cm->category->parents]);

            }
        }
        return 1;
    }

    function arrayToActiveCategories($arr) {
        foreach($arr as $x) {
            if(empty($x)) { continue; }
            foreach($x as $xx) {
                $this->activeCategories[] = $xx->category_id;
            }
        }
    }

    function getProductConsistance() {
        if(empty($this->product->product_id)) { return; }
        $this->getChildProducts();
        $this->getParentProducts();
    }

    function getSpecificationTranslations() {
        return [
            'width' => 'Ширина',
            'height' => 'Высота',
            'length' => 'Длина',
            'weight' => 'Вес'
        ];
    }

    function getChildProducts() {
        $key = 'child_products_of_'.$this->product->product_id;

        if(!\Cache::has($key)) {
            $this->product->child_products = \DB::table('product_consistance')
                ->select(\DB::raw(self::getCoalesce(). " as priceinfo, 
                products.title, products.quantity, product_consistance.scheme, products.product_class,
                    product_consistance.consistance_quantity, product_consistance.ordering, products.avatar,
                    products.hide_link, products.product_id, links.link"))
                ->join('products', 'product_consistance.child_product', '=', 'products.product_id')
                ->join('links', 'products.link_id', '=', 'links.id')
                ->where('product_consistance.parent_product', $this->product->product_id)
                ->orderBy('product_consistance.ordering', 'asc')
                ->get();
            $totalParts = $this->product->child_products->count();
            if(!$totalParts) { $this->product->child_products = []; return; }
            $productIds = [];
            for($i = 0; $i < $totalParts; $i++) {
                $productIds[] = $this->product->child_products[$i]->product_id;
                handleProductLink($this->product->child_products[$i]);
                calculateProductPrice($this->product->child_products[$i]);
            }
            $codes = \DB::table('product_codes')->whereIn('product_id', $productIds)->get();
            if($codes->count()) {
                $codesById = [];
                foreach($codes as $x) {
                    if(empty($codesById[$x->product_id])) { $codesById[$x->product_id] = []; }
                    $codesById[$x->product_id][] = $x->product_code;
                }
                for($i = 0; $i < $totalParts; $i++) {
                    $this->product->child_products[$i]->codes = isset($codesById[$this->product->child_products[$i]->product_id]) ? $codesById[$this->product->child_products[$i]->product_id] : [];
                }
            }
            \Cache::put($key, \json_encode($this->product->child_products), 120);
        } else {
            $this->product->child_products = \json_decode(\Cache::get($key));
        }
    }

    function getParentProducts() {
        $key = 'parent_products_of_'.$this->product->product_id;
        if(!\Cache::has($key)) {
            $this->product->parent_products = \DB::table('product_consistance')
                ->select(\DB::raw('DISTINCT products.title, products.quantity, products.product_id, links.link, products.hide_link'))
                ->join('products', 'product_consistance.parent_product', '=', 'products.product_id')
                ->join('links', 'products.link_id', '=', 'links.id')
                ->where('product_consistance.child_product', $this->product->product_id)
                ->get();
            $totalParts = $this->product->parent_products->count();
            if(!$totalParts) { $this->product->parent_products = []; return; }
            $productIds = [];
            $productsById = [];
            for($i = 0; $i < $totalParts; $i++) {
                $this->product->parent_products[$i]->categories = [];
                handleProductLink($this->product->parent_products[$i]);
                $productIds[] = $this->product->parent_products[$i]->product_id;
                $productsById[$this->product->parent_products[$i]->product_id] = &$this->product->parent_products[$i]->categories;
            }
            $this->parentProductsCategories = \DB::table('categories')
                ->select('categories.title', 'links.link', 'product_categories.product_id')
                ->join('product_categories', 'product_categories.category_id', '=', 'categories.category_id')
                ->join('links', 'links.id', '=', 'categories.link_id')
                ->where('categories.status', true)
                ->whereRaw(\DB::raw("product_categories.product_id IN (".join(',',$productIds).")"))
                ->get();
            if(empty($this->parentProductsCategories)) { return; }
            foreach($this->parentProductsCategories as $k => $cat) {
                handleCategoryLink($this->parentProductsCategories[$k]);
                array_push($productsById[$cat->product_id], $this->parentProductsCategories[$k]);
            }
            \Cache::put($key, \json_encode($this->product->parent_products), 120);
        } else {
            $this->product->parent_products = \json_decode(\Cache::get($key));
        }
    }


    function getProductByPath($path) {
        $this->queryProduct();
        $this->product->where('links.link', $path);
        $this->product = $this->product->first();
        if(!$this->product) { return 0; }
        $this->completeProductData();
        return 1;
    }

    static function getCoalesce($table = 0,  $cast = 0) {
        $c = $cast ? '::'.$cast : '';
        $t = $table ? $table : 'products';
        $cityId = @\Html::$city->city_id ? \Html::$city->city_id : defaultCity;
        $regionId = @\Html::$city->region_id ? \Html::$city->region_id : 0;
        return "COALESCE(".$t.".priceinfo->'cities'->'".$cityId."',
         ".$t.".priceinfo->'regions'->'".$regionId."',
         ".$t.".priceinfo->'default')".$c;
    }

    static function getCompareCoalesce($table = 0, $cast = 0) {
        $c = $cast ? '::'.$cast : '';
        $t = $table ? $table : 'products';
        $cityId = @\Html::$city->city_id ? \Html::$city->city_id : defaultCity;
        $regionId = @\Html::$city->region_id ? \Html::$city->region_id : 0;
        return "COALESCE(".$t.".priceinfo->'cities'->'".$cityId."'->>'discount_price', ".$t.".priceinfo->'cities'->'".$cityId."'->>'price',
         ".$t.".priceinfo->'regions'->'".$regionId."'->>'discount_price', ".$t.".priceinfo->'cities'->'".$regionId."'->>'price',
          ".$t.".priceinfo->'default'->>'discount_price', ".$t.".priceinfo->'default'->>'price')".$c;
    }



    function select($arr = false) {
        $this->q->selectRaw(\DB::raw("products.title, products.sells, ". self::getCoalesce(). " as priceinfo, products.product_id, products.gifts, products.avatar, products.quantity, links.link"));
    }

    function searchByTitle() {

        $this->q = \DB::table('products')
            ->where('products.status', true);
            if(mb_strlen($this->params['q']) > 2) {
                $this->q->whereRaw(\DB::raw("products.tsvector_title @@ to_tsquery('russian', '".str_replace(' ', '+', $this->params['q']).":*')"));
            } else {
                $this->q->where('products.title', 'LIKE', '%'.$this->params['q'].'%');
            }
            $this->select();
    }

    function searchBySerial() {
        $this->q = \DB::table('products')
            ->whereRaw(\DB::raw("products.product_id IN (SELECT DISTINCT serials.product_id FROM serials WHERE serials.serial ='".$this->params["q"]."')"))
            ->where('products.status', true);
            $this->select();
    }

    function searchByCode() {
        $this->q = \DB::table('products')
            ->whereRaw(\DB::raw("products.product_id IN (SELECT DISTINCT product_codes.product_id FROM product_codes WHERE product_codes.product_code LIKE '%".$this->params["q"]."%')"))
            ->where('products.status', true);
        $this->select();
    }


    function joinSearchPrices() {
        if($this->params['minp'] !== false || $this->params['maxp'] !== false) {
          $this->q->where('sells', 1);
            if(!ShopModel::$discountCalculated) { ShopModel::$discountCalculated = 0; }
            if($this->params['minp'] !== false) {
                $this->q->whereRaw(\DB::raw("(".self::getCompareCoalesce("products",'float').")/100*(100-".ShopModel::$discountCalculated.") >= '".(float)$this->params['minp']."'"));
            }
            if($this->params['maxp'] !== false) {
                $this->q->whereRaw(\DB::raw("(".self::getCompareCoalesce("products",'float').")/100*(100-".ShopModel::$discountCalculated.") <= '".(float)$this->params['maxp']."'"));
            }
        }
    }


    function getSearchCatsArr($id) {
        $this->categories = [];
        $this->iterateSearchCat($id);
    }

    function iterateSearchCat($id) {
        $this->categories[] = $id;
        $cats = \DB::table('categories')->select('category_id')->where('parent', $id)->get();
        if(!empty($cats)) {
          foreach($cats as $x) {
            $this->iterateSearchCat($x->category_id);
          }
        }
    }

    function search() {
        $this->collectParams();
        $this->reformatParams();
        if($this->params['q']) {
            switch($this->params['ns']) {
                case 2: $this->searchBySerial(); break;
                default: $this->searchByTitle(); break;
            }
        } else {
            $this->q = \DB::table('products')
                ->where('products.status', true);
            $this->select();
        }

        $this->joinSearchPrices();
        if($this->params['ssc']) {
            $this->q->where('products.scheme', false);
        }

        $this->total = $this->q->count();
            $this->products = $this->q
            ->take($this->perPage)
            ->orderByRaw("products.sells desc, products.avatar asc, products.ordering asc")
            ->skip(($this->params['p']-1)*$this->perPage)
             ->join('links', 'links.id', '=', 'products.link_id')
             ->get();

        //dd($this->products);
        $this->completeProductsData($this->products);
        $this->breadcrumbs = $this->params['q'] ? [(object)['title' => 'Поиск', 'link' => false], (object)['title' => $this->params['q'], 'link' => false]] : [];
        return $this;
    }

    function getProductCategoryByPath($path) {
        $this->collectCategoryParams();
        if(!$this->cm->getCategoryByPath($path)) { return 0; }
        $this->category = &$this->cm->category;
        $this->activeCategories[] = $this->category->category_id;
        if(!empty($this->category->parents)) {
            foreach($this->category->parents as $x) {
                $this->activeCategories[] = $x->category_id;
            }
        }

        if(!@$this->category->data->hide_products) {
            if(!@$this->category->data->hide_subproducts) {
                $this->getSubProducts($this->category->searchIn);
            } else {
                $this->getCategoryProducts($this->category->category_id);
            }
        }
        return $this;
    }

    function getProductCategoryByOldPath($path) {
      $this->category = \DB::table('categories')
      ->select('links.link')
      ->where('prev_link', $path)
      ->where('status', 1)
      ->join('links', 'links.id', '=', 'categories.link_id')
      ->first();
      return $this->category;
    }

    function getProductCategoryById($id) {
        $this->collectCategoryParams();
        $this->category = $this->cm->getCategoryById($id);
        if(!$this->category) { return 0; }
        $this->category->parents = $this->cm->parents;
        $this->category->childs = $this->cm->childs;
        $this->activeCategories[] = $this->category->category_id;
        if(!empty($this->category->parents)) {
            foreach($this->category->parents as $x) {
                $this->activeCategories[] = $x->category_id;
            }
        }
        $this->getSubProducts($this->category->category_id);
        return $this;
    }

    function getSubProducts($ids) {
        $this->q = \DB::table('product_categories')
        ->whereIn('product_categories.category_id', $ids)
        ->join('products', 'product_categories.product_id', '=', 'products.product_id')
        ->where('products.scheme', false)
        ->where('products.status', true);
        $this->total = $this->q->count();

        $orderBy = $this->reformatOrdering(@$this->category->data->default_ordering);
        if($this->total) {
        $take = $this->perPage < $this->total ? $this->perPage : $this->total;
        $this->products = $this->q
            ->select(\DB::raw("products.title, products.sells, ".self::getCoalesce()." as priceinfo, products.product_id, links.link, products.avatar, products.quantity, products.gifts"))
            ->offset(($this->params['p']-1)*$this->perPage)
            ->join('links', 'links.id', '=', 'products.link_id')
            ->orderByRaw($orderBy)
            ->take($take)
            ->get();
        }
        if(!$this->products) { return 0; }
        $this->completeProductsData($this->products);
        return 1;
    }

    function reformatOrdering($ordering) {
        $defaultOrdering = "products.sells desc, products.avatar asc, products.ordering asc";
        if(!$ordering) { return $defaultOrdering; }
        $ordering =  str_replace('coalesceprice', self::getCoalesce(), $ordering);
       
        return $ordering;
    }

    function getCategoryProducts($id) {
        $this->q = \DB::table('products')
            ->where('products.status', true)
            ->where('product_categories.category_id', $id)
            ->select('products.product_id')
            ->join('product_categories', 'product_categories.product_id', '=', 'products.product_id');
        $this->total = $this->q->count();
        if(!$this->total) { $this->products = 0; }
        $this->products = $this->q
            ->select(\DB::raw("products.title, products.sells, ".self::getCoalesce()." as priceinfo, products.product_id, links.link, products.avatar, products.quantity, products.gifts"))
            ->orderByRaw($this->reformatOrdering(@$this->category->data->default_ordering))
        ->join('links', 'links.id', '=', 'products.link_id')
        ->offset(($this->params['p']-1)*$this->perPage)
        ->take($this->perPage)
        ->get();
        if(!$this->products) { return 0; }
        if(!$this->category) {
            $this->category = $this->getProductCategoryById($id);
        }
        $this->completeProductsData($this->products);
        return 1;
    }

    function completeProductsData(&$products) {
        if(empty($products)) { return; }
        $productIds = []; $productsById = [];
        $total = count($products);
        $giftIds = [];
        for($i = 0; $i < $total; $i++) {
            $products[$i]->giftsData = \json_decode($products[$i]->gifts);
            if(!empty($products[$i]->giftsData->products)) {
                foreach($products[$i]->giftsData->products as $giftId) {
                    $giftIds[] = $giftId;
                }
            }
            $productIds[] = $products[$i]->product_id;
            handleProductLink($products[$i]);
            if(!isset($productsById[$products[$i]->product_id])) {
                $productsById[$products[$i]->product_id] = (object)['codes' => []];
            }
            $products[$i]->codes = &$productsById[$products[$i]->product_id]->codes;
            $products[$i]->gifts = (object)['products' => []];
        }
        $productIds = array_unique($productIds);
        $productCodes = \DB::table('product_codes')->whereIn('product_id', $productIds)->get();
        if(!empty($productCodes)) {
            foreach($productCodes as $x) {
                $productsById[$x->product_id]->codes[] = $x->product_code;
            }
        }
        if(!empty($giftIds)) {
            $gifts = \DB::table('products')
            ->select(\DB::raw(self::getCoalesce()." as priceinfo, products.product_id, products.avatar"))
            ->where('products.status', 'true')
            ->whereIn('product_id', $giftIds)->get();
            $giftsById = [];
            if(!empty($gifts)) {
                foreach($gifts as $k => $x) {
                    $giftsById[$x->product_id] = &$gifts[$k];
                }
                for($i = 0; $i < $total; $i++) {
                    if(empty($products[$i]->giftsData->products)) { continue; }
                    $products[$i]->gifts->maxGiftPrice = 0;
                    foreach($products[$i]->giftsData->products as $x) {
                        if(!isset($giftsById[$x])) { continue; }
                        $products[$i]->gifts->products[] = &$giftsById[$x];
                        calculateProductPrice($giftsById[$x]);
                        if($giftsById[$x]->priceinfo['final_price'] > $products[$i]->gifts->maxGiftPrice) {
                            $products[$i]->gifts->maxGiftPrice = $giftsById[$x]->priceinfo['final_price'];
                        }
                    }
                }
            }
        }


    }







    function collectParams() {
        $this->params = [];
        $this->params['p'] = (int)\Request::get('p');
        $this->params['p'] = $this->params['p'] < 1 ? 1 : $this->params['p'];
        $this->params['q'] = mb_substr(trim(\Request::get('q')), 0, 40);
        $this->params['ns'] = \Request::get('ns');
        $this->params['ca'] = \Request::get('ca');
        $this->params['minp'] = \Request::get('minp');
        $this->params['maxp'] = \Request::get('maxp');
        $this->params['o'] = \Request::get('o');
        $this->params['ssc'] = (int)\Request::get('ssc');
    }

    function reformatParams() {
        if(!in_array($this->params['ns'], $this->getAvailableNamespaces())) {
            $this->params['ns'] = 1;
        }
        if(!\anInt($this->params['minp']) || !$this->params['minp']) { $this->params['minp'] = false; }
        if(!\anInt($this->params['maxp']) || !$this->params['maxp']) { $this->params['maxp'] = false; }
        if(empty($this->params['ca']) || !\anInt($this->params['ca'])) { unset($this->params['ca']); }
        $this->getAllowedOrdering();
        if(!in_array($this->params['o'], $this->getAllowedOrdering())) { unset($this->params['o']); }
        $this->offset = $this->params['p']*$this->perPage;
    }

    function getAllowedOrdering() {
        if($this->allowedOrdering) { return $this->allowedOrdering; }
        return $this->allowedOrdering = [
            1 => ['price', 'asc'],
            2 => ['price', 'desc'],
            3 => ['title', 'asc'],
            4 => ['title', 'desc']
        ];
    }

    function getAvailableNamespaces() {
        if($this->namespaces) { return $this->namespaces; }
        return $this->namespaces = [1, 2, 3];
    }

    function collectCategoryParams() {
        $this->params = [];
        $this->params['p'] = (int)\Request::get('p');
        $this->params['p'] = $this->params['p'] < 1 ? 1 : $this->params['p'];
    }
}
