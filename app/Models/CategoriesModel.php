<?php

namespace App\Models;

class CategoriesModel extends Model {

    static public $_instance = null;
    public $category;
    public $parents;
    public $childs;
    public $categories;
    public $categoriesById;
    public $categoryTree;
    public $link;
    public $result;
    public $menuonly = 0;

    private function __construct() {

    }


    public static function getInstance() {
        if(!is_null(self::$_instance)) { return self::$_instance; }
        self::$_instance = new self();
        return self::$_instance;
    }

    function getCategoryByAlias($alias) {
        $cat = \DB::table('categories')->where('status', 1)
        ->where('alias', $alias)
        ->first();
        if(!$cat) { return 0; }
        return $cat;
    }

    function getSimpleCategoryById($id) {
        $cat = \DB::table('categories')
        ->select('links.link', 'categories.*')
        ->join('links', 'categories.link_id', '=', 'links.id')
        ->where('status', 1)
        ->where('category_id', $id)
        ->first();
        if(!$cat) { return 0; }
        $cat->link = '/'.trim($cat->link, '/');
        $cat->linkHandled = 1;
        return $cat;
    }

    function getCategoriesByIds($ids) {
        if(empty($ids)) { return []; }
        $result = [];
        foreach($ids as $id) {
            $cat = $this->getCategoryById($id);
            if($cat) { $result[] = $cat; }
        }
        return $result;
    }

    function getCategoryParentsByStarterId($id) {
        if(!$id) { return 1; }
        $this->parents = [];
        if(!$this->iterateCategory($id)) { return 0; }
        $this->parents = array_reverse($this->parents);
        return 1;
    }

    function getCategoryById($id) {
        $key = 'categoryById_'.$id;
        if(!\Cache::has($key)) {
            $this->category = $this->getSimpleCategoryById($id);
            if(!$this->category) { return 0; }
            if(!$this->getCategoryParentsByStarterId($this->category->parent)) { return 0; }
            $this->category->parents = $this->parents;
            $this->category->childs = $this->getCategoryChildsById($this->category->category_id);
            $this->category->breadcrumbs = $this->buildBreadcrumbs();
            \Cache::forever($key, \json_encode($this->category));
        }
        return $this->category = \json_decode(\Cache::get($key));
    }

    function buildBreadcrumbs() {
        $breadcrumbs = [];
        if(!empty($this->parents)) {
            foreach($this->parents as $x) {
                $breadcrumbs[] = (object)['link' => $x->link, 'title' => $x->title];
            }
        }
        if(!empty($this->category)) {
            $breadcrumbs[] = (object)['link' => $this->category->link, 'title' => $this->category->title];
        }
        return $breadcrumbs;
    }

    function getCategoryByPath($path) {
        $path = trim($path, '/');
        $key = 'categoryByPath_'.$path;
        if(!\Cache::has($key)) {
            $this->category = \DB::table('links')
            ->select('links.link', 'categories.*')
            ->join('categories', 'categories.link_id', '=', 'links.id')
            ->where('links.link', $path)
            ->where('categories.status', 1)
            ->first();
            if(!$this->category) { return 0; }
            $this->category->link = '/'.trim($this->category->link, '/');
            if(!$this->getCategoryParentsByStarterId($this->category->parent)) { return 0; }
            $this->category->parents = $this->parents;
            $this->category->childs = $this->getCategoryChildsById($this->category->category_id);
            $this->category->breadcrumbs = [];
            if(!empty($this->category->parents)) {
                foreach($this->category->parents as $k => $v) {
                    $this->category->breadcrumbs[] = (object)['link' => $v->link, 'title' => $v->title];
                }
            }
            $this->category->breadcrumbs[] = (object)['link' => $this->category->link, 'title' => $this->category->title];
            $this->category->data = \json_decode($this->category->data);

            if(!@$this->category->data->hide_subproducts) {
                $this->getSearchInIds();
            } else {
                $this->category->searchIn = [];
            }
            \Cache::forever('categoryById_'.$this->category->category_id, \json_encode($this->category));
            \Cache::forever($key, \json_encode($this->category));
        }
        $this->category = \json_decode(\Cache::get($key));
        return 1;
    }

    function getSearchInIds() {
        $this->category->searchIn = [];
        $this->category->searchIn[] = $this->category->category_id;
        if(empty($this->category->childs)) { return; }
        foreach($this->category->childs as &$x) {
            $this->category->searchIn[] = $x->category_id;
            $this->getChildIdsById($x->category_id);
        }
    }


    function getChildIdsById($id) {
        $ids = \DB::table('categories')->where('parent', $id)->where('status', true)->select('category_id')->get();
        if(!$ids) { return; }
        foreach($ids as $x) {
            $this->category->searchIn[] = $x->category_id;
            $this->getChildIdsById($x->category_id);
        }
    }


    function iterateChilds($id) {

    }

    function getCategoryChildsById($id) {
        $cats = \DB::table('categories')
        ->select('categories.*', 'links.link')
        ->join('links', 'links.id', '=', 'categories.link_id')
        ->where('status', 1)
        ->orderBy('ordering', 'asc')
        ->where('parent', $id)
        ->get();
        if(!$cats) { return $cats; }
        foreach($cats as $k => $v) {
            $cats[$k]->link = '/'.trim($v->link, '/');
        }
        return $cats;
    }

    function iterateCategory($id) {
        if(!$id) { return 1; }
        $cat = $this->getSimpleCategoryById($id);
        if(!$cat) { return 0; }
        $this->parents[] = $cat;
        if($cat->parent) {  return $this->iterateCategory($cat->parent); }
        return 1;
    }

    function categoryToResult($id) {
        if(empty($this->categoriesById->{$id})) { return; }
        $this->result[] = $this->categoriesById->{$id};
        if($this->categoriesById->{$id}->parent) {
            $this->categoryToResult($this->categoriesById->{$id}->parent);
        }
        return;
    }


    function getCategoryTree() {
        if($this->categoryTree) { return $this->categoryTree; }
        $this->getAllCategories();
        $postfix = $this->menuonly ? '_menuonly' : '';
        $key = 'categoryTree'.$postfix;
        $key2 = 'categories_by_id'.$postfix;
        if(!\Cache::has($key) || !\Cache::has($key2)) {
            foreach($this->categories as $k => $x) {
                $this->categories[$k]->childs = [];
                $this->categoriesById->{$x->category_id} = &$this->categories[$k];
            }

            foreach($this->categories as $k => $x) {
                $this->categories[$k]->link = $this->getLinkFromTree($this->categories[$k]->category_id);
                if(!$x->parent) {
                    $this->categoryTree[] = &$this->categories[$k];
                    continue;
                }
                if(isset($this->categoriesById->{$x->parent})) {
                    $this->categoriesById->{$x->parent}->childs[] = &$this->categories[$k];
                }
            }
            $clone = $this->categoriesById;
            \Cache::forever($key, \json_encode($this->categoryTree));
            \Cache::forever($key2, \json_encode($clone));
        }
        $this->categoriesById = \json_decode(\Cache::get($key2));
        return \json_decode(\Cache::get($key));
    }






    function getLinkFromTree($id) {
        $this->link = [$this->categoriesById->{$id}->alias];
        if($this->categoriesById->{$id}->parent) {
            $this->iterateLink($this->categoriesById->{$id}->parent);
        }
        return '/'.\implode('/', array_reverse($this->link));
    }

    function iterateLink($id) {
        $this->link[] = $this->categoriesById->{$id}->alias;
        if($this->categoriesById->{$id}->parent) {
            $this->iterateLink($this->categoriesById->{$id}->parent);
        }
    }

    function getAllCategories() {
        if($this->categories) { return $this->categories; }
        $postfix = $this->menuonly ? '_menuonly' : '';
        $key = 'categories'.$postfix;
        if(!\Cache::has($key)) {
            $this->categories = \DB::table('categories')
            ->select('category_id', 'title', 'subtitle', 'avatar', 'status', 'parent', 'alias', 'menu_avatar', 'css', 'ordering', 'menu_title',
            'hide_in_menu')
            ->orderBy('ordering', 'asc')
            ->where('status', true);
            if($this->menuonly) { $this->categories->where('hide_in_menu', 0); }
            $this->categories = $this->categories->get();
            \Cache::forever($key, \json_encode($this->categories));
        }
        return \json_decode(\Cache::get($key));
    }

    function getMenuCategories() {

    }


}
