<?php

namespace App\Models;

class PaymentsModel {

    public $paymentMethods;
    public $order;
    public $settings;
    public $hash;

    function getPaymentMethods() {
        if($this->paymentMethods) { return $this->paymentMethods; }
        return $this->paymentMethods = [

        ];
    }

    function sendOrder($order) {
        $this->order = $order;
        return $this->redirectToYandexMoney();
    }

    function getPaymentUrl() {
        $this->result = new \Result;
        $this->result->hash = $this->hash ? $this->hash : \Request::get('hash');
        if(!$this->result->hash) { $this->result->display = invalidDataFormat; return $this->result; }
        $sm = ShopModel::getInstance();
        $this->result->order = $sm->getOrderByHash($this->result->hash);
        if(!$this->result->order) { $this->result->display = 'Такого заказа не существует. Попробуйте обновить страницу'; return $this->result; }
        $this->order = &$this->result->order;
        $this->result->link = $this->makePaymentLink();
        $this->result->result = 1;
        return $this->result;
    }

    function makePaymentLink() {
        return $this->makeYandexPaymentLink();
    }

    function getSettings() {
        $this->settings = (new SettingsModel)->getSiteData();
    }

    function makeYandexPaymentLink() {
        $this->getSettings();
        $params = [
            'shopId' => $this->settings->yandexMoney->shopId,
            'scid' => $this->settings->yandexMoney->scid,
            'seller_id' => $this->settings->yandexMoney->seller_id,
            'paymentType' => '',
            'sum' => $this->order->price . '.00',
            'customerNumber' => $this->order->order_id
        ];
        foreach($this->settings->yandexMoney as $k => $v) {
            $params[$k] = $v;
        }
        return "https://demomoney.yandex.ru/eshop.xml?".http_build_query($params);

    }

}