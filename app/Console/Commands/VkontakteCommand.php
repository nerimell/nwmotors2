<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\Schedules\Vkontakte;

class VkontakteCommand extends Command
{

    protected $signature = 'VkontakteCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $settings = \json_decode(file_get_contents('resources/data/smm.json'));
        $scheduleModel = new Vkontakte;
        foreach($settings->vkontakte as $vk) {
            $scheduleModel->init($vk);
            $scheduleModel->uploadPost();
        }
    }
}
