<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;


class DevCommand extends Command
{

    protected $signature = 'DevCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $nodes = \DB::table('outsource_ezpartscatalog_nodes')->get();
        $nodesByPtr = [];
        $requiresParent = [];
        foreach($nodes as $k => $v) {
            $nodesByPtr[$v->ptr] = $v;
            if(!empty(trim($v->parent_ptr))) {
                $requiresParent[$v->parent_ptr] = 1;
            }
        }
        foreach($nodes as $k => $v) {
            if(empty($requiresParent[$v->ptr])) {
                \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $v->ptr)->update(['is_final' => 1]);
            } else {
                \DB::table('outsource_ezpartscatalog_nodes')->where('ptr', $v->ptr)->update(['is_final' => 0]);
            }
        }
    }



}
