<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;

class ParseResourceDataCommand extends Command
{

    protected $signature = 'ParseResourceDataCommand';
    protected $description;


    public function __construct() {
        parent::__construct();
    }


    public function handle() {
        $this->getSerials();
    }


    function getSerials() {
        $parser = new EzPartsCatalogParser();
        $parser->parseNodesResourceData();
    }
}



