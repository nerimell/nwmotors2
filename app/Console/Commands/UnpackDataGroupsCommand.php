<?php

namespace App\Console\Commands;


use App\Models\Schedules\EzPartsCatalogParser\EzPartsCatalogParser;
use Illuminate\Console\Command;
use App\Http\Controllers\DevController;

class UnpackDataGroupsCommand extends Command
{

    protected $signature = 'UnpackDataGroupsCommand';
    protected $description;


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(){
       $cls = new EzPartsCatalogParser();
       $cls->handleDataGroupInfo();
    }


}


