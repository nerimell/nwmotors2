<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Http\Controllers\DevController;

class UnpackSerialsCommand extends Command
{

    protected $signature = 'UnpackSerialsCommand';
    protected $description;


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(){
       $this->unpackSerials();
    }

    function handleSerials(){
        $serials = \DB::table('outsource_ezpartscatalog_serials')
            ->orderBy('id', 'desc')
            ->where('response_status', 1)
            ->whereNull('status')
            ->get();
        foreach ($serials as $k => $v) {
            $response = \json_decode($v->response);
            $serials = @$response->value->Serials;
            if (!$serials || empty($serials)) {
                $this->serialStatus($v->id, 0);
            }
            foreach ($serials as $serial) {
                $this->handleSerial($serial, $v);
            }
            $this->serialStatus($v->id, 1);
        }
    }

    function serialStatus($id, $status){
        \DB::table('outsource_ezpartscatalog_serials')->where('id', $id)->update(['status' => $status]);
    }

    function handleSerial($serialObj, $v){
        $path = @$serialObj[1][1][1];
        if (!$path) {
            return 0;
        }
        $ptrs = explode('/', $path);
        $lastptr = array_pop($ptrs);
        if (!$lastptr) {
            return 0;
        }
        $cat = \DB::table('categories')->where('ptr', $lastptr)->first();
        if (!$cat) {
            return 0;
        }
        $products = \DB::table('product_categories')->where('category_id', $cat->category_id)->get();
        if (empty($products)) {
            return 0;
        }
        foreach ($products as $x) {
            $data = [
                'ordering' => $serialObj[0],
                'product_id' => $x->product_id,
                'serial' => $v->serial
            ];
            try {
                \DB::table('serials')->insert($data);
            } catch (\PDOException $e) {

            }
        }
        return 1;
    }

    function unpackSerials(){
        $this->handleSerials();
    }
}


