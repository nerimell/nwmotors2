<?php

namespace App\Console;

use App\Console\Commands\VkontakteCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\VkontakteCommand::class,
        Commands\ExternalProductsCommand::class,
        Commands\FtsCommand::class,
        Commands\DoublesCommand::class,
        Commands\UnpackSerialsCommand::class,
        Commands\ParseSerialsCommand::class,
        Commands\ParseResourceDataCommand::class,
        Commands\UnpackDataGroupsCommand::class,
        Commands\DevCommand::class
    ];



    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */



    protected function schedule(Schedule $schedule){
        include_once(public_path().'/../resources/data/constants.php');
        //$schedule->command('VkontakteCommand')->weekdays()->twiceDaily('11:00', '15:00'); //->mondays()->thursdays()->wednesdays()->tuesdays()->fridays();
       // $schedule->command('ExternalProductsCommand')->everyMinute();
        //$schedule->command('UnpackSerialsCommand')->hourly();
        $schedule->command('ParseSerialsCommand')->hourly();
    }



    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
