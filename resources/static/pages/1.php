<p>Наша компания входит в состав <a href="http://www.truck.ru" target="_blank" rel="nofollow">Гудвил холдинга</a>
    работающего на Российском рынке с 1992 года, мы имеем огромный опыт и знания и с удовольствием поможем Вам, если Вы
    ищете новый двигатель Mercury/Mercruiser, поворотно-откидную колонку Mercury/Mercruiser или запчасти
    Mecury/Quicksilver для ремонта и обслуживания двигателей и колонок Mercruiser, Mercury, Volvo Penta. <br> <br>
    NWMotors поставляет широкий спектр новых морских двигателей без навесного оборудования которые являются нашей
    основной специализацией, а так же новых морских двигателей с навесным оборудованием Mercruiser и запчастей
    Mercury/Quiksilver к ним. Морские двигатели без навесного оборудования (часто их называют short block, long-block)
    подходят для замены размороженных, перегретых и изношенных судовых двигателей: MerCruiser, Volvo Penta, Crusader,
    Chris Craft, PleasureCraft, Seamaxx, Indmar. Эти двигатели различаются по объему двигателя, и подходят на замену как
    карбюраторных, так и впрысковых MPI, мы поставляем все двигатели объмом: <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-3-0l-i4-gm-detail">3.0L
        (181cid)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-4-3l-v6-gm-detail">4.3L
        (262)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-5-0l-v8-gm-detail">5.0L
        (305)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-5-7l-v8-gm-detail">5.7L
        (350)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-6-2l-v8-gm-detail">6.2L
        (377)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-7-4l-v8-detail">454
        (7.4)</a>, <a
        href="/dvigateli-mercruiser-volvo-penta-bez-navesnogo-oborudovaniya-long-block-short-block/dvigatel-mercruiser-8-1l-v8-gm-detail">496
        (8.1L)</a>, 8.2L (502).</p>
        <div class="invisible-delimeter"></div>
<p><?php echo \Html::picture('pages/clip_image002.jpg'); ?></p>
<div class="invisible-delimeter"></div>
<div class="invisible-delimeter"></div>
<h2>Поставка двигателей для катеров</h2>
<p>Двигатели в полной комплектации, поставляемые нами:</p>
<p class="m-b-0">
<strong>карбюраторные версии TKS</strong>: <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-3-0l-l4-135-l-s-detail">Mercruiser3.0L</a>,
<a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-4-3-v6-190-l-s-detail">Mercruiser 4.3L</a></p>
<p class="m-b-0"><strong>впрысковые MPI:</strong>
 <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-3-0l-l4-135-l-s-86-detail">Mercruiser3.0L MPI</a>,
 <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-4-3l-mpi-detail">Mercruiser 4.3L MPI</a>,
 <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-5-0l-mpi-260-l-s-detail">Mercruiser 5.0L MPI</a>,
 <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-350-mag-mpi-300-l-s-detail">Mercruiser 5.7L 350 MAG MPI</a>,
 <a href="/stacionarnyie-dvigateli-mercruiser-volvo-penta/dvigatel-mercruiser-350-mag-mpi-300-l-s-110-detail">Mercruiser 6.2L 377 MAG MPI</a>
</p>
<p class="m-b-0"><strong>дизельные QSD: </strong>
<a href="/dizelnye-dvigateli-mercruiser-qsd-i-mercruiser-tdi/mercruiser-qsd-2-0l-130l-s-detail">MerCruiserQSD 2.0-130</a>,
<a href="/dizelnye-dvigateli-mercruiser-qsd-i-mercruiser-tdi/mercruiser-qsd-2-0l-130l-s-2492014-02-21-14-51-03-detail">MerCruiser QSD 2.0-150</a>,
<a href="/dizelnye-dvigateli-mercruiser-qsd-i-mercruiser-tdi/mercruiser-qsd-2-0l-130l-s-2492014-02-21-14-51-03-251-detail">MerCruiser QSD 2.0-170</a>,
 MerCruiser QSD 2.0-170 DTS, MerCruiser QSD 2.8-220, MerCruiser QSD 4.2-270 DTS, MerCruiser QSD 4.2-320, MerCruiser QSD 4.2-350.</p>
<div class="invisible-delimeter"></div>
<p><?php echo \Html::picture('pages/clip_image004.jpg'); ?></p>
<div class="inivisible-delimeter"></div>
<p>У нас лучшие цены и крупнейший склад двигателей Mercury/Mercruiser, деталей для их замены, обслуживания и ремонта.
    Бесплатная доставка двигателей во все регионы России, гарантия на все двигатели и поворотно-откидные колонки.
    Стоимость на нашем сайте – это стоимость, по которой Вы получите новый двигатель с гарантией в Вашем регионе.</p>
<p>Так же на нашей сервисной станции Вы можете обслужить, отремонтировать или заменить двигатель на катере.</p>
<div class="invisible-delimeter"></div>
<p><span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Наши реквизиты</span></p>
<p><strong>Общество с ограниченной ответственностью «Марин Трейдинг»</strong><br> ИНН 7820335600 КПП 782001001<br>
    ОГРН 1147847038987<br> 196608, Санкт-Петербург, город Пушкин, ул. Автомобильная, д. 4, лит. А3<br> Р/счет №
    40702810102100002581<br>Санкт-Петербургский филиал АО "Нордеа Банк" <br>БИК 044030868<br>К/счет 30101810900000000868<br>
    ОКПО 33152392<br> e-mail:
    <a href="mailto:info@nwmotors.ru">info@nwmotors.ru</a>

</p>
