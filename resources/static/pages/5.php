

<p class="alpha-text attention-panel" >
    <i class="mdi mdi-alert-circle"></i>Все товары на нашем сайте вы можете оплатить без комиссий — вы платите только за
    товар, <span class="red underline">комиссии платежных систем мы оплатим сами!</span>
</p>
<div id="payments-list">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#payment-type1">Банковские карты</a></li>
        <li><a data-toggle="tab" href="#payment-type2">Альфа-Клик</a></li>
        <li><a data-toggle="tab" href="#payment-type3">Сбербанк онлайн</a></li>
        <li><a data-toggle="tab" href="#payment-type4">Наличные</a></li>
        <li><a data-toggle="tab" href="#payment-type5">Яндекс деньги</a></li>
        <li><a data-toggle="tab" href="#payment-type6">Вебмани</a></li>
        <li><a data-toggle="tab" href="#payment-type7">Баланс мобильного</a></li>
    </ul>
    <div class="tab-content">
        <div id="payment-type1" class="tab-item tab-pane fade in active">
            <p class="tab-item-content-title">Банковские карты</p>
            <p>Вы можете оплатить заказ картами Visa (включая Electron), MasterCard и Maestro. Подходят карты,
                выпущенные в любой точке мира. При этом вы надежно защищены от мошенничества — платежи проходят по
                технологии 3-D Secure (подтверждение паролями на странице банка).</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>С одной карты можно списать максимум 250 000 рублей за один раз и 500 000 рублей в месяц.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи по банковским картам возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/bankcard_math.pdf" target="_blank">Как заплатить с банковской карты</a> (.pdf, 157
                Кб).</p>
            <div class="payment-avatar"><img src="/images/payments/cards.png"></div>
        </div>

        <div id="payment-type2" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">Альфа-клик</p>
            <p>Если у Вас есть счет в Альфа-банке, вы можете оплатить заказ через систему Альфа-Клик.</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>15 000 рублей за один раз.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/alfa.pdf" target="_blank">Как заплатить с помощью Альфа-клик</a> (.pdf, 938 Кб).</p>
            <div class="payment-avatar"><img src="/images/payments/alpha.png"></div>
        </div>

        <div id="payment-type3" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">Сбербанк онлайн</p>
            <p>Если у Вас есть счет в Сбербанке, вы можете оплатить заказ через систему Сбербанк Онлайн.</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>Минимум 10 рублей.</p>
            <p>Максимум в сутки: 10 000 рублей через мобильный банк и 100 000 рублей через интернет-банк.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/sber.pdf" target="_blank">Как заплатить с помощью Сбербанк Онлайн</a> (.pdf, 2,5 Мб).
            </p>
            <div class="payment-avatar"><img src="/images/payments/i_sber.png"></div>
        </div>

        <div id="payment-type4" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">Наличные</p>
            <p>Вы можете оплатить наличными прямо на сайте NWmotors.ru. Вы оформляете заказ и получаете код платежа. По
                этому коду можно внести наличные через терминал или в салоне связи (больше <a
                    href="https://money.yandex.ru/pay/doc.xml?id=526209" target="_blank">170 тысяч пунктов</a> по всей
                России).</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>Минимум 10 рублей.</p>
            <p>Максимальный размер платежа наличными — 15 000 рублей за один раз. В некоторых салонах связи и терминалах
                лимиты могут быть ниже.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи наличными возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/cash_meth.pdf" target="_blank">Как заплатить наличными через терминал</a> (.pdf, 156
                Кб).</p>
            <p><a href="https://money.yandex.ru/pay/doc.xml?id=526209" target="_blank">Все пункты оплаты можно
                    посмотреть здесь</a>.</p>
            <div class="payment-avatar"><img src="/images/payments/i_cash.png"></div>
        </div>

        <div id="payment-type5" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">Яндекс деньги</p>
            <p>Вы можете оплатить заказ как один из 18 млн пользователей Яндекс.Денег — из электронного кошелька или с помощью привязанной к нему банковской карты. Яндекс.Деньги — крупнейший сервис электронных платежей в России и один из самых популярных в странах СНГ. При этом открыть кошелек может пользователь из любой страны.</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>Владелец идентифицированного кошелька может потратить до 250 000 рублей за один раз, анонимный пользователь — до 15 000 рублей.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи через Яндекс.Деньги возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/yamoney_meth.pdf" target="_blank">Как заплатить Яндекс.Деньгами</a> (.pdf, 158 Кб).</p>
            <div class="payment-avatar"><img src="/images/payments/i_wallet.png"></div>
        </div>

        <div id="payment-type6" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">ВебМани</p>
            <p>Вы можете совершить платеж из рублевого кошелька популярной системы международных расчетов WebMoney.</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>Из WM-кошелька можно списать максимум 15 000 рублей за один раз и 150 000 рублей в месяц.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи Webmoney возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/wmpay.pdf" target="_blank">Как заплатить из WM-кошелька</a> (.pdf, 188 Кб).</p>
            <div class="payment-avatar"><img src="/images/payments/i_webmoney.png"></div>
        </div>

        <div id="payment-type7" class="tab-item tab-pane fade">
            <p class="tab-item-content-title">Баланс мобильного</p>
            <p>Вы можете оплатить заказ с баланса мобильных номеров Билайн, МегаФон, МТС, TELE2 и Ростелеком. При этом Вы получите SMS от оператора с просьбой подтвердить списание суммы платежа с баланса.</p>
            <div class="invisible-delimeter"></div>
            <p class="tab-item-content-title">Лимиты</p>
            <p>Максимальные суммы:</p>
            <ul>
                <li>- для абонентов Билайн и Мегафон — 14 000 рублей за один раз, 15 000 рублей в сутки, 40 000 рублей в месяц;</li>
                <li>- для абонентов TELE2, МТС и Ростелеком — 14 999 рублей за один раз, 30 000 рублей в сутки, 40 000 рублей в месяц.</li>
            </ul>
            <br>
            <p>Обратите внимание: прием оплаты с корпоративных номеров любых операторов невозможен.</p>
            <p class="tab-item-content-title">Возврат денег</p>
            <p>Платежи со счетов мобильных возвращаются по согласованию с менеджером магазина.</p>
            <p class="tab-item-content-title">Инструкция</p>
            <p><a href="/media/doc/mob_math.pdf" target="_blank">Как заплатить со счета мобильного</a> (.pdf, 352 Кб).</p>
            <div class="payment-avatar"><img src="/images/payments/i_mobile.png"></div>
        </div>


    </div>
</div>
<div class="font-12 soft-color">
    <p>Сервис приема платежей на нашем сайте предоставлен компанией Яндекс.Деньги. Яндекс.Деньги работает по <a
            href="http://www.cbr.ru/credit/gubzi_docs/spisok.pdf" target="_blank">Стандарту Банка России</a> (СТО БР
        ИББС) и имеет сертификат соответствия международному стандарту PCI DSS (безопасность индустрии платежных карт).
        Сервис приема платежей компании Яндекс.Деньги прошел проверку Роскомнадзора на соответствие требованиям по
        защите персональных данных.</p>
    <p>Яндекс.Деньги используют собственное программное обеспечение. Все операции с банковскими картами производятся по
        технологии 3-D Secure: это своего рода двойная защита, когда банк просит пользователя подтвердить платеж
        специальным паролем, получить который может только владелец карты.</p>
    <p>Оплачивая товары у нас на сайте, Вы полностью переносите процесс оплаты на защищенный сервис компании
        Яндекс.Деньги.</p>
</div>
