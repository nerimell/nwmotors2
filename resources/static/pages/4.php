<div id="service-page">


    <h1>Официальный сервис-центр NWMotors</h1>
    <p>Наш сервисный центр осуществляет весь спектр услуг по механической обработке деталей стационарных двигателей
        MerCruiser, Volvo Penta, OMC, Indmar, Crusader.</p>
    <ul>
        <li>Расточка блока цилиндров;</li>
        <li>Шлифовка коленчатых валов;</li>
        <li>Фрезеровка и шлифовка ГБЦ;</li>
        <li>Ремонт ГБЦ;</li>
        <li>Опрессовка ГБЦ, блоков;</li>
        <li>Опрессовка впускных и выпускных коллекторов</li>
        <li>Ремонт, окраска корпусов лодок и катеров из стеклопластика</li>
    </ul>
    <div class="invisible-delimeter"></div>
    <h1><strong>Стоимость и примерные сроки выполнения работ</strong></h1>
    <h2><strong>Расточка блоков цилиндров</strong></h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th><strong> Название </strong></th>
            <th width="20%">
                <strong>Кол-во цилиндров</strong>
            </th>
            <th width="20%">
                <strong>Стоимость</strong>
            </th>
            <th width="20%">
                <strong>Срок выполнения</strong>
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.<br> Volvo Penta 3.0 GL/GS</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>5970</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 4.3 L/LX 4.3 MPI.<br> Volvo Penta 4.3 GL/GXi</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>8950</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.0 L / 5.0 MPI.<br> Volvo Penta 5.0 L<br> Indmar 5.0 L<br>Crusader 5.0 L</p>
            </td>
            <td>
                <p>8&nbsp;</p>
            </td>
            <td>
                <p>11850</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.7L / MAG 350<br> Volvo Penta 5.7 L<br> Indmar 5.7 L<br> Crusader 5.7 L</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>11850</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 6.2 L MPI / MAG 377<br> Indmar 6.2 L<br> Crusader 6.2 L</p>
            </td>
            <td>
                <p>8 &nbsp;</p>
            </td>
            <td>
                <p>11850</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 7.4 L / 454 MPI<br> MerCruiser 8.1 L / 496 MPI<br> MerCruiser 8.2 L / 502 MPI</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>14670</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Работы включают мойку двигателя<strong><br clear="all"> </strong></p>
    <h2><strong>Шлифовка коленчатого вала</strong></h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th><strong> Название </strong></th>
            <th width="20%">
                <strong>Кол-во цилиндров</strong>
            </th>
            <th width="20%">
                <strong>Стоимость&nbsp;</strong>
            </th>
            <th width="20%">
                <strong>Срок выполнения</strong>
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.<br> Volvo Penta 3.0 GL/GS</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>6890</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 4.3 L/LX 4.3 MPI.<br> Volvo Penta 4.3 GL/GXi</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>9650 &nbsp;</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.0 L / 5.0 MPI.<br> Volvo Penta 5.0 L<br> Indmar 5.0 L<br>Crusader 5.0 L</p>
            </td>
            <td>
                <p>8&nbsp;</p>
            </td>
            <td>
                <p>10900 &nbsp;</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.7L / MAG 350<br> Volvo Penta 5.7 L<br> Indmar 5.7 L<br> Crusader 5.7 L</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>10900</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 6.2 L MPI / MAG 377<br> Indmar 6.2 L<br> Crusader 6.2 L</p>
            </td>
            <td>
                <p>8 &nbsp;</p>
            </td>
            <td>
                <p>10900 &nbsp;</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 7.4 L / 454 MPI<br> MerCruiser 8.1 L / 496 MPI<br> MerCruiser 8.2 L / 502 MPI</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>12100</p>
            </td>
            <td>
                <p>5-7 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Работы включают мойку коленчатого вала</p>
    <h2><strong>Фрезеровка ГБЦ</strong></h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th>
                <strong>&nbsp;Название</strong>
            </th>
            <th width="20%">
                <strong>Стоимость</strong>
            </th>
            <th width="20%">
                <strong>Срок выполнения</strong>
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.<br> Volvo Penta 3.0 GL/GS<br> MerCruiser 4.3 L/LX 4.3 MPI.<br> Volvo Penta 4.3
                    GL/GXi<br> MerCruiser 5.0 L / 5.0 MPI.<br> Volvo Penta 5.0 L<br> Indmar 5.0 L<br> Crusader 5.0 L<br>
                    MerCruiser 5.7L / MAG 350<br> Volvo Penta 5.7 L<br> Indmar 5.7 L<br> Crusader 5.7 L<br> MerCruiser
                    6.2 L MPI / MAG 377<br> Indmar 6.2 L<br> Crusader 6.2 L<br> MerCruiser 7.4 L / 454 MPI<br>
                    MerCruiser 8.1 L / 496 MPI<br> MerCruiser 8.2 L / 502 MPI</p>
            </td>
            <td>
                <p>1950</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Работы включают мойку ГБЦ. <br>Притирка клапанов - 250 руб. / шт.</p>
    <h2><strong>Опрессовка впускных и выпускных коллекторов Mercuiser,</strong></h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th>
                <strong>Название</strong>
            </th>
            <th width="20%">
                <strong>Впускной</strong>
            </th>
            <th width="20%">
                <strong>Выпускной</strong>
            </th>
            <th width="20%">
                <strong>Срок выполнения</strong>
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.</p>
            </td>
            <td>
                <p>1950</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 4.3 L/LX</p>
            </td>
            <td>
                <p>2490</p>
            </td>
            <td>
                <p>1980</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.0 L</p>
            </td>
            <td>
                <p>2780</p>
            </td>
            <td>
                <p>2490</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p><br> MerCruiser 5.7L</p>
            </td>
            <td>
                <p>2780</p>
            </td>
            <td>
                <p>2490</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <h2>Опрессовка головок блоков цилиндров</h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th><strong>Название</strong></th>
            <th width="20%">
                <strong>Стоимость</strong>
            </th>
            <th width="20%">
                <strong>Срок выполнения</strong>
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.<br> Volvo Penta 3.0 GL/GS<br> MerCruiser 4.3 L/LX 4.3 MPI.<br> Volvo Penta 4.3
                    GL/GXi<br> MerCruiser 5.0 L / 5.0 MPI.<br> Volvo Penta 5.0 L<br> Indmar 5.0 L<br> Crusader 5.0 L<br>
                    MerCruiser 5.7L / MAG 350<br> Volvo Penta 5.7 L<br> Indmar 5.7 L<br> Crusader 5.7 L<br> MerCruiser
                    6.2 L MPI / MAG 377<br> Indmar 6.2 L<br> Crusader 6.2 L<br> MerCruiser 7.4 L / 454 MPI<br>
                    MerCruiser 8.1 L / 496 MPI<br> MerCruiser 8.2 L / 502 MPI</p>
            </td>
            <td>
                <p>2900</p>
            </td>
            <td>
                <p>1-2 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Работы включают мойку ГБЦ</p>
    <h2><strong>Опрессовка блоков цилиндров</strong></h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <thead>
            <th>Название</th>
            <th width="20%">
                Кол-во цилиндров
            </th>
            <th width="20%">
                Стоимость
            </th>
            <th width="20%">
                Срок выполнения
            </th>
        </thead>
        <tr>
            <td>
                <p>MerCruiser 3.0 L/LX.<br> Volvo Penta 3.0 GL/GS</p>
            </td>
            <td>
                <p>4</p>
            </td>
            <td>
                <p>6900</p>
            </td>
            <td>
                <p>2 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 4.3 L/LX 4.3 MPI.<br> Volvo Penta 4.3 GL/GXi</p>
            </td>
            <td>
                <p>6</p>
            </td>
            <td>
                <p>8950</p>
            </td>
            <td>
                <p>2 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.0 L / 5.0 MPI.<br> Volvo Penta 5.0 L<br> Indmar 5.0 L<br> Crusader 5.0 L</p>
            </td>
            <td>
                <p>8&nbsp;</p>
            </td>
            <td>
                <p>12850</p>
            </td>
            <td>
                <p>2-3 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 5.7L / MAG 350<br> Volvo Penta 5.7 L<br> Indmar 5.7 L<br> Crusader 5.7 L</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>12850</p>
            </td>
            <td>
                <p>2-3 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 6.2 L MPI / MAG 377<br> Indmar 6.2 L<br> Crusader 6.2 L</p>
            </td>
            <td>
                <p>8 &nbsp;</p>
            </td>
            <td>
                <p>12850</p>
            </td>
            <td>
                <p>2-3 р.д.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>MerCruiser 7.4 L / 454 MPI<br> MerCruiser 8.1 L / 496 MPI<br> MerCruiser 8.2 L / 502 MPI</p>
            </td>
            <td>
                <p>8</p>
            </td>
            <td>
                <p>15890</p>
            </td>
            <td>
                <p>2-4 р.д.</p>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Работы включают мойку двигателя</p>

</div>