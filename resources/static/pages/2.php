<div id="contacts-page">
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#moscow">Москва</a></li>
        <li class="active"><a data-toggle="tab" href="#saint-petersburg">Санкт-Петербург</a></li>
    </ul>
    <div class="common-contacts">
        <div class="group">
            <div class="key">Федеральный номер:</div>
            <div class="value"><span class="highlight">8 (800) 555-85-81</span> <span class="contact-hint">(бесплатно для регионов России)</span></div>
        </div>
    </div>
    <div class="tab-content">
        <div id="moscow" class="tab-item tab-pane fade in">
            <div class="local-contacts">
                <div class="group">
                    <div class="key">Телефон:</div>
                    <div class="value"><span class="highlight">+7 (495) 514-67-89</span></div>
                </div>
                <div class="group">
                    <div class="key">Емайл:</div>
                    <div class="value"><span class="highlight">info@nwmotors.ru</span></div>
                </div>
                <div class="invisible-delimeter"></div>
                <div class="group">
                    <div class="key">Часы работы магазина:</div>
                    <div class="value">с <span class="highlight">9:30</span> до <span class="highlight">18:00</span> в будние дни, суббота и воскресенье –
                        выходной</div>
                </div>
                <div class="invisible-delimeter"></div>
                <div class="group">
                    <div class="key">Адрес:</div>
                    <div class="value">Россия, Москва, Московская обл. г.Щербинка, Остафьевское ш., д.2 (возле платф. Щербинка,5 км от МКАД)</div>
                </div>
                </div>
                <div class="contact-map-wrapper">
                    <div id="moscow-map" class="contact-map">
                        <div class="preloader"><img src="/images/loader-2.gif"></div>
                    </div>
                </div>
        </div>
        <div id="saint-petersburg" class="tab-item tab-pane fade in active">
            <div class="local-contacts">
                <div class="group">
                    <div class="key">Узнать состояние заказа:</div>
                    <div class="value"><span class="highlight">+7 (812) 324-01-96</span></div>
                </div>
                <div class="group">
                    <div class="key">Телефон:</div>
                    <div class="value"><span class="highlight">+7 (812) 606-71-76</span></div>
                </div>
                <div class="group">
                    <div class="key">Емайл:</div>
                    <div class="value"><span class="highlight">info@nwmotors.ru</span></div>
                </div>
                <div class="invisible-delimeter"></div>
                <div class="group">
                    <div class="key">Часы работы магазина:</div>
                    <div class="value">с <span class="highlight">9:30</span> до <span class="highlight">18:00</span> понедельник-пятница</div>
                </div>
                <div class="group">
                    <div class="key">Часы работы склада:</div>
                    <div class="value">с <span class="highlight">9:00</span> до <span class="highlight">21:00</span> ежедневно</div>
                </div>
                <div class="invisible-delimeter"></div>
                <div class="group">
                    <div class="key">Адрес:</div>
                    <div class="value">Россия, Санкт-Петербург, г.Пушкин, ул. Автомобильная, д.4A лит3, (бывш. Новая Прорезка, д.1)</div>
                </div>
            </div>
            <div class="contact-map-wrapper">
                <div id="saint-petersburg-map" class="contact-map">
                    <div class="preloader"><img src="/images/loader-2.gif"></div>
                </div>
            </div>
            <div class="contact-entrance-preview">
                <img src="/images/contact/spb-enterance.jpg" />
            </div>
        </div>
    </div>


</div>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    hashClick();
    function hashClick() {
        $('[href="'+window.location.hash+'"]').click();
    }
    window.onhashchange = hashClick;

    ymaps.ready(ymi);
    var moscowMap;
    var spbMap;

    function ymi(){ // yandex maps init
        spbMap = new ymaps.Map("saint-petersburg-map", {
            center: [59.729422, 30.445010],
            zoom: 15,
            controls: []
        });
        spbMark = new ymaps.Placemark(spbMap.getCenter(), { hintContent: 'Марин трейдинг', balloonContent: '<p><strong>Марин трейдинг</strong></p><p>Главный офис и магазин-склад</p>' });
        spbMap.geoObjects.add(spbMark);

        moscowMap = new ymaps.Map("moscow-map", {
            center: [55.508350, 37.560655],
            zoom: 15,
            controls: []
        });
        moscowMark = new ymaps.Placemark(moscowMap.getCenter(), { hintContent: 'Марин трейдинг', balloonContent: '<p><strong>Марин трейдинг</strong></p><p>Магазин-склад</p>' });
        moscowMap.geoObjects.add(moscowMark);

    }
</script>