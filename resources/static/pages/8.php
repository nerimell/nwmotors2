<div id="instructions-page">

    <h1 class="big">Инструкции по эксплуатации и обслуживанию. Оглавление</h1><ul><li><a href="#1">Карбюраторные двигатели Mercruiser</a></li>
        <li><a href="#2">Инжекторные двигатели Mercruiser</a></li>
        <li><a href="#3">Подвесные лодочные моторы Mercury</a></li>
        <ul style="margin-top:0">
            <li><a href="#4">Двухтактные лодочные моторы Mercury</a></li>
            <li><a href="#5">Четырёхтактные лодочные моторы Mercury</a></li>
            <li><a href="#6">Мощные подвесные двигатели Mercury Verado </a></li>
            <li><a href="#7">Мощные двухтактные моторы Mercury Optimax</a></li>
            <li><a href="#8">Моторы Mercury SeaPro для морской и коммерческой эксплуатации</a></li>
            <li><a href="#9">Подвесные водомёты Mercury</a></li>
        </ul>
        <li><a href="#10">Чертежи и установочные размеры двигателей</a></li>
        <ul style="margin-top:0">
            <li><a href="#11">Карбюраторные двигатели Mercruiser</a></li>
            <li><a href="#12">Инжекторные двигатели Mercruiser</a></li>
        </ul>
        <li><a href="#13">Рекомендуемые моторные и редукторные масла и заливные объемы </a></li>
        <li><a href="#14">Объемы масла в поворотно-откидных колонках Mercury/Mercruiser</a></li>
        <li><a href="#15">Свечи зажигания для двигателей Mercruiser</a></li>
        <li><a href="#16">Период обкатки двигателей Mercruiser</a></li>
        <li><a href="#17">График технического обслуживания двигателей Mercruiser</a></li>
        <ul style="margin-top:0">

            <li><a href="#18">Обычное техническое обслуживание</a></li>
            <li><a href="#19">Регламентное техобслуживание</a></li>
        </ul>
        <li><a href="#20">Таблица смазок морского назначения</a></li>
        <li><a href="#21">Выбор подходящего гребного винта</a></li>
    </ul>
    <h1 class="big">Инструкции по эксплуатации и обслуживанию</h1>

    <a name="1"></a><h2>Карбюраторные двигатели Mercruiser</h2>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/3.0TKS_3.0MPI_s2.pdf" target="_blank">Mercruiser 3.0 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3,5.0,5.7TKS_s2.pdf" target="_blank">Mercruiser 4.3 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3,5.0,5.7TKS_s2.pdf" target="_blank">Mercruiser 5.0 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3,5.0,5.7TKS_s2.pdf" target="_blank">Mercruiser 5.7 TKS карбюраторный</a></li>
    </ul>
    <a name="2"></a><h2>Инжекторные двигатели Mercruiser</h2>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/3.0TKS_3.0MPI_s2.pdf" target="_blank">Mercruiser 3.0 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3_5.0MPI_s2.pdf" target="_blank">Mercruiser 4.3 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.5MPI_s2.pdf" target="_blank"> Mercruiser 4.5 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3_5.0MPI_s2.pdf" target="_blank"> Mercruiser 5.0 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_350mag_377mag_s2.pdf" target="_blank"> Mercruiser 350 MAG/5.7 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_350mag_377mag_s2.pdf" target="_blank">Mercruiser 377 MAG/6.2 MPI инжекторный</a></li>
    </ul>
    <a name="3"></a><h2>Подвесные лодочные моторы Mercury</h2>
    <a name="4"></a><h3>Двухтактные лодочные моторы Mercury</h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke2.5.pdf">Mercury 2.5 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke3.3.pdf">Mercury 3.3 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke4.pdf">Mercury 4 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke5.pdf">Mercury 5 M, 5 ML</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke9.9.pdf">Mercury 9.9 M, 8 M, 8 ML</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke15.pdf">Mercury 15 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke30.pdf">Mercury 30 E, 30 ML, 30 M, 30 EL</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke40.pdf">Mercury 40 ELPTO, 40 ELO 697cc, 40 MH 697cc, 40 EO, 40 EO 697cc, 40 ML 697cc</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke50.pdf">Mercury 50 EO, 50 ELPTO</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke60.pdf">Mercury 60 ELPTO, 60 EO</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke60bf.pdf">Mercury 60 ELPTO BigFoot</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke75.pdf">Mercury 75 ELPTO</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke90.pdf">Mercury 90 ELPTO</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/TwoStroke200.pdf">Mercury 200 EFI</a></li>
    </ul>
    <a name="5"></a><h3>Четырёхтактные лодочные моторы Mercury</h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke2.5.pdf">Mercury F2.5 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke3.5.pdf">Mercury F3.5 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke4.pdf">Mercury F4 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke5.pdf">Mercury F5 M, F5 ML Sailpower</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke6.pdf">Mercury F6 M, F6 ML</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke8.pdf">Mercury F8 M, F8 ML</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke9.9.pdf">Mercury 9.9 ELPT BigFoot (RC), 9.9 EL BigFoot (RC), 9.9 MLH BigFoot, F9.9 M</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke15.pdf">Mercury F15 MH, F15 MLH, F15 E (RC), F15 EL (RC)</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke20.pdf">Mercury F20 EL, F20 M, F20 ML, F20 ELPT, F20 E</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke25.pdf">Mercury F25 EL EFI, F25 E EFI, F25 ELPT EFI, F25 M EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke30.pdf">Mercury F30 ML GA EFI, F30 M GA EFI, F30 ELPT EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke40.pdf">Mercury F40 E EFI, F40 EPT EFI, F40 ELPT EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke50.pdf">Mercury F50 ELPT EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke60.pdf">Mercury F60 ELPT EFI, F60 ELPT EFI BigFoot</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke80.pdf">Mercury F80 ELPT EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke100.pdf">Mercury F100 EXLPT EFI, F100 ELPT EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke115.pdf">Mercury F115 EXLPT EFI, F115 ELPT EFI, F115 ECXLPT CT, F115 EXLPT CT, F115 ELPT CT</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/FourStroke150.pdf">Mercury F 150 L EFI, F 150 XL EFI, F 150 СXL EFI</a></li>
    </ul>
    <a name="6"></a><h3>Мощные подвесные двигатели Mercury Verado </h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Verado150.pdf">Mercury 150 L Verado, 150 XL Verado, 150 CXL Verado</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Verado200.pdf">Mercury 200 L Verado, 200 XL Verado, 200 CXL Verado</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Verado225.pdf">Mercury 225 L Verado, 225 XXL Verado, 225 XL Verado, 225 CXL Verado, 225 CXXL Verado</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Verado250.pdf">Mercury 250 CXXL Verado, 250 XXL Verado, 250 L Verado, 250 CXL Verado, 250 XL Verado</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Verado300.pdf">Mercury 300 L Verado, 300 CXXL Verado, 300 XXL Verado, 300 CXL Verado, 300 XL Verado</a></li>
    </ul>
    <a name="7"></a><h3>Мощные двухтактные моторы Mercury Optimax</h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax90.pdf">Mercury 90 ELPT OptiMax</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax115.pdf">Mercury 115 EXLPT OptiMax, 115 ELPT OptiMax</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax150.pdf">Mercury 150 PRO XS L OptiMax, 150 PRO XS XL OptiMax, 150 PRO XS CXL OptiMax</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax200.pdf">Mercury 200 XL OptiMax, 200 CXL OptiMax, 200 L OptiMax</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax225.pdf">Mercury 225 XL OptiMax, 225 CXL OptiMax, 225 L OptiMax</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Optimax250.pdf">Mercury 250 XXL OptiMax, 250 XL OptiMax, 250 CXL OptiMax, 250 CXXL OptiMax</a></li>
    </ul>
    <a name="8"></a><h3>Моторы Mercury SeaPro для морской и коммерческой эксплуатации</h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Seapro15.pdf">Mercury 15 ML SeaPro, 15 M SeaPro</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Seapro25.pdf">Mercury 25 M SeaPro, 25 ML SeaPro</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Seapro40.pdf">Mercury 40 M SeaPro, 40 ML SeaPro</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Seapro55.pdf">Mercury 55 ML SeaPro</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Seapro60.pdf">Mercury 60 ML SeaPro BigFoot</a></li>
    </ul>
    <a name="9"></a><h3>Подвесные водомёты Mercury</h3>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Jet25.pdf">Mercury Jet 25 ML, Jet 25 MLH GA EFI</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/manuals/Jet40.pdf">Mercury Jet 40 ELPT EFI</a></li>
    </ul>
    <a name="10"></a><h1 class="big">Чертежи и установочные размеры двигателей</h1>
    <a name="11"></a><h2>Карбюраторные двигатели Mercruiser</h2>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/3.0TKS_s2.pdf" target="_blank">Mercruiser 3.0 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3TKS_s2.pdf" target="_blank">Mercruiser 4.3 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_5.7TKS_s2.pdf" target="_blank">Mercruiser 5.0 TKS карбюраторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_5.7TKS_s2.pdf" target="_blank">Mercruiser 5.7 TKS карбюраторный</a></li>
    </ul>
    <a name="12"></a><h2>Инжекторные двигатели Mercruiser</h2>
    <ul>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/3.0mpi_s2.pdf" target="_blank">Mercruiser 3.0 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/4.3mpi-alpha_s2.pdf" target="_blank">Mercruiser 4.3 MPI инжекторный</a></li>
        <li>Mercruiser 4.5 MPI инжекторный</li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_350mag_377mag_s2.pdf" target="_blank"> Mercruiser 5.0 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_350mag_377mag_s2.pdf" target="_blank">Mercruiser 350 MAG/5.7 MPI инжекторный</a></li>
        <li><a href="<?php echo storageUrl; ?>resources/pdf/5.0_350mag_377mag_s2.pdf" target="_blank">Mercruiser 377 MAG/6.2 MPI инжекторный</a></li>
    </ul>
    <a name="13"></a><h1 class="big">Рекомендуемые моторные и редукторные масла и заливные объемы </h1>
    <div class="siteonly">
        <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
            <tbody>
            <tr bgcolor="#EEEEEE">
                <th rowspan="2" width="20%"><strong> Тип масла</strong></th>
                <th colspan="2">Cтационарные бензиновые</th>
                <th colspan="3">4-х тактные подвесные</th>
            </tr>
            <tr bgcolor="#EEEEEE">
                <th width="16%">без каталитического нейтрализатора</th>
                <th width="16%">с каталитическим нейтрализатором</th>
                <th width="16%">2,5 - 60 л.с.</th>
                <th width="16%">75-115 л.с.</th>
                <th width="16%">более 115 л.с.<br>
                    (не Verado)</th>
            </tr>
            <tr>
                <td><strong><a href="/tovar/maslo-quicksilver-25w40-sinteticheskoe-4l-detail">25W-40 синтетика</a></strong></td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
            </tr>
            <tr>
                <td><strong><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/maslo-quicksilver-25w40-dlya-dvigatelej-s-povorotno-otkidnymi-kolonkami-4l-349-detail">25W-40 минеральное</a></strong></td>
                <td>Хорошо</td>
                <td>Хорошо</td>
                <td>Хорошо</td>
                <td>Хорошо</td>
                <td>Хорошо</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="mobileonly">
        <table class="brdr" width="40%" border="1" cellspacing="0" cellpadding="5" style="float:left;margin-right:5%">
            <tbody>
            <tr bgcolor="#EEEEEE">
                <th rowspan="2" width="20%"><strong> Тип масла</strong></th>
                <th colspan="2">Cтационарные бензиновые</th>
            </tr>
            <tr bgcolor="#EEEEEE">
                <th width="16%">без каталитического нейтрализатора</th>
                <th width="16%">с каталитическим нейтрализатором</th>
            </tr>
            <tr>
                <td><strong><a href="/tovar/maslo-quicksilver-25w40-sinteticheskoe-4l-detail">25W-40 синтетика</a></strong></td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
            </tr>
            <tr>
                <td><strong><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/maslo-quicksilver-25w40-dlya-dvigatelej-s-povorotno-otkidnymi-kolonkami-4l-349-detail">25W-40 минеральное</a></strong></td>
                <td>Хорошо</td>
                <td>Хорошо</td>
            </tr>
            </tbody>
        </table>
        <table class="brdr" width="50%" border="1" cellspacing="0" cellpadding="5" style="float:left">
            <tbody>
            <tr bgcolor="#EEEEEE">
                <th rowspan="2" width="20%"><strong> Тип масла</strong></th>
                <th colspan="3">4-х тактные подвесные</th>
            </tr>
            <tr bgcolor="#EEEEEE">
                <th width="20%">2,5 - 60 л.с.</th>
                <th width="20%">75-115 л.с.</th>
                <th width="30%">более 115 л.с.<br>
                    (не Verado)</th>
            </tr>
            <tr>
                <td><strong><a href="/tovar/maslo-quicksilver-25w40-sinteticheskoe-4l-detail">25W-40 синтетика</a></strong></td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
                <td>Лучше всего</td>
            </tr>
            <tr>
                <td><strong><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/maslo-quicksilver-25w40-dlya-dvigatelej-s-povorotno-otkidnymi-kolonkami-4l-349-detail">25W-40 минеральное</a></strong></td>
                <td>Хорошо</td>
                <td>Хорошо</td>
                <td>Хорошо</td>
            </tr>
            </tbody>
        </table>
    </div>
    <h2> </h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5" style="clear:both">
        <tbody>
        <tr bgcolor="#eeeeee">
            <td valign="top" width="20%"><p><strong>Модель двигателя</strong></p></td>
            <td valign="top" width="16%"><p align="center"><strong>Обеъм масла л.</strong></p></td>
            <td colspan="2" valign="top"><p><strong>Рекомендуемое моторное масло</strong></p></td>
        </tr>
        <tr>
            <td colspan="2" valign="top"><p><strong>Карбюраторные двигатели</strong></p></td>
            <td rowspan="12" valign="middle" width="32%"><p align="center"><a href="/tovar/maslo-quicksilver-25w40-sinteticheskoe-4l-detail" target="_blank"><span style="text-decoration: underline;"><strong><img src="/images/static/quicksilver-25w40_synthblend.jpg" border="0" alt="" width="150" height="238"><br>
          Масло 4-х тактное для лодочных моторов синтетическое 25W40 QUICKSILVER 25W40 Synthetic blend</strong></span></a></p>
                <p align="center"><strong>Лучше всего</strong></p></td>
            <td rowspan="12" valign="middle" width="32%"><p align="center"><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/maslo-quicksilver-25w40-dlya-dvigatelej-s-povorotno-otkidnymi-kolonkami-4l-349-detail" target="_blank"><span style="text-decoration: underline;"><strong><img src="/images/static/quicksilver-25w40_mineral.jpg" border="0" alt="" width="150" height="238"><br>
          Масло 4-х тактное для лодочных моторов минеральное 25W40 QUICKSILVER 25W40 Mineral</strong></span></a></p>
                <p align="center"><strong>Хорошо</strong></p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 3.0L</p></td>
            <td valign="top"><p align="center">3.80 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 4.3L</p></td>
            <td valign="top"><p align="center">3.80 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 5.0L</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 5.7L</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        <tr>
            <td colspan="2" valign="top"><p><strong>Впрысковые двигатели</strong></p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 3.0 MPI</p></td>
            <td valign="top"><p align="center">3.80 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 4.3 MPI</p></td>
            <td valign="top"><p align="center">3.80 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 4.5 MPI</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 5.0 MPI</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 350 MAG</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        <tr>
            <td valign="top"><p>Mercruiser 377 MAG</p></td>
            <td valign="top"><p align="center">4.25 л.</p></td>
        </tr>
        </tbody>
    </table>
    <p><em>Приведенные объемы являются приблизительными. Используйте измерительный щуп для определения точного требуемого количества масла. Доливка 0,95 л моторного масла поднимет уровень от отметки ADD (ДОЛИТЬ) до отметки ОК</em>.</p>
    <a name="14"></a><h2><br>
        Объемы масла в поворотно-откидных колонках Mercury/Mercruiser</h2>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <tr bgcolor="#eeeeee">
            <td valign="top"><p align="center"><strong>Модель поворотно-откидной колонки</strong></p></td>
            <td valign="top" width="32%"><p align="center"><strong>Заправочный объем</strong></p></td>
            <td valign="top" width="32%"><p align="center"><strong>Тип масла</strong></p></td>
        </tr>
        <tr>
            <td valign="middle" height="75"><p><strong>Mercruiser Alpha One Gen II</strong></p></td>
            <td valign="middle"><p align="center"><strong>1982 мл. (1.982 л.)</strong></p></td>
            <td rowspan="4" valign="middle" height="300"><p align="center"><a href="/quicksilver-catalog/maslo-smazka-uhod-za-dvigatelem/transmissionnoe-maslo-quicksilver/tovar-858064qb1-transmissionnoe-maslo-sae-90-detail" target="_blank"><span style="text-decoration: underline;"><strong><img src="/images/static/quicksilver-sae90.jpg" border="0" alt="" width="150" height="238"><br>
          Quicksilver SAE 90 High Performance</strong></span></a></p></td>
        </tr>
        <tr valign="middle">
            <td height="75"><p><strong>Mercruiser Bravo One</strong></p></td>
            <td><p align="center"><strong>2736 мл. (2.736 л.)</strong></p></td>
        </tr>
        <tr valign="middle">
            <td height="75"><p><strong>Mercruiser Bravo Two</strong></p></td>
            <td><p align="center"><strong>3209 мл. (3.209 л)</strong></p></td>
        </tr>
        <tr valign="middle">
            <td height="75"><p><strong>Mercruiser Bravo Three</strong></p></td>
            <td><p align="center"><strong>2972 мл. (2.972 л.)</strong></p></td>
        </tr>
        </tbody>
    </table>
    <p><em>Все объемы включают полную заправку колонки и заправку бачка подпитки колонки до максимального рабочего уровня</em></p>
    <h1 class="big">Свечи зажигания для двигателей Mercruiser</h1>
    <a name="15"></a><h2>Рекомендуемые свечи зажигания для карбюраторных и инжекторных двигателей Mercruiser</h2>
    <ul>
        <li><a href="/mercruiser-zapchasti/sistema-zazhiganiya/iridievaya-svecha-zazhiganiya-acdelco-detail">AC Platinum (AC 41-993) </a></li>
        <li><a href="/mercruiser-zapchasti/sistema-zazhiganiya/iridievaya-svecha-zazhiganiya-acdelco-detail">AC Platinum (AC 41-101) </a></li>
    </ul>
    <p>Искровой промежуток : 1,5 мм</p>
    <h2>Рекомендуемые свечи зажигания для карбюраторных двигателей Mercruiser 1997г.в. и новее</h2>
    <ul>
        <li><a href="/mercruiser-zapchasti/sistema-zazhiganiya/iridievaya-svecha-zazhiganiya-acdelco-detail">AC Platinum (AC 41-993) </a></li>
        <li><a href="/mercruiser-zapchasti/sistema-zazhiganiya/svecha-zazhiganiya-detail">Quicksilver 816336Q </a></li>
    </ul>
    <p>Искровой промежуток : 1,5 мм</p>
    <h2>Рекомендуемые свечи зажигания для карбюраторных двигателeй Mercruiser 1997г.в. и старше</h2>
    <ul>
        <li><a href="/mercruiser-zapchasti/sistema-zazhiganiya/svecha-zazhiganiya-ngk-detail">ACDelco MR43T</a></li>
    </ul>
    <p>Искровой промежуток : 1,5 мм</p>
    <a name="16"></a>
    <h1 class="big">Период обкатки двигателей Mercruiser</h1>
    <h2>20-часовый период обкатки</h2>
    <p>Первые 20 часов эксплуатации являются периодом обкатки двигателя. Правильная обкатки имеет решающее значение для обеспечения минимального расхода масла и максимальной эффективности двигателя. В течение этого периода обкатки необходимо соблюдать следующие правила.</p>
    <ul>
        <li>Первые 10 часов эксплуатационного периода не допускайте оборотов ниже 1500 об/мин в течение продолжительных периодов времени. Как можно скорее переходите на передачу после запуска и доводите дроссельную заслонку до уровня свыше 1500 об/мин, если существуют условия для безопасной эксплуатации.</li>
        <li>Не допускайте продолжительной работы на постоянной скорости.</li>
        <li>Не превышайте 3/4 раскрытия дроссельной заслонки в течение первых 10 часов. В течение последующих 10 часов разрешена периодическая эксплуатация с полностью открытой дроссельной заслонкой (не дольше пяти минут).</li>
        <li>Избегайте акселерации с полностью открытой дроссельной заслонкой от оборотов холостого хода.</li>
        <li>Не эксплуатируйте лодку с полностью открытой дроссельной заслонкой до тех пор, пока двигатель не достигнет нормальной рабочей температуры.</li>
        <li>Часто проверяйте уровень моторного масла. При необходимости доливайте масло. В течение периода обкатки высокий расход масла является нормальным явлением.</li>
    </ul>
    <h2>Период времени после обкатки</h2>
    <p>Для продления срока службы силового агрегата Mercury MerCruiser следуйте рекомендациям:</p>
    <ul>
        <li>Проверьте, позволяет ли гребной винт эксплуатировать двигатель у верхней границы рекомендованного эксплуатационного диапазона числа оборотов с полностью открытой дроссельной заслонкой.</li>
        <li>Эксплуатируйте двигатель с открытием дроссельной заслонки на 3/4 или менее. Избегайте длительной эксплуатации с полностью открытой дроссельной заслонкой.</li>
        <li>Смените моторное масло и масляный фильтр.</li>
        <li>Смените трансмиссионное масло.</li>
    </ul>
    <p>Новые поворотно-откидные колонки Bravo могут потребовать добавления 470 мл смазки редуктора в бачке подпитки во время периода обкатки (20 часов работы). В течение периода обкатки необходимо следить и поддерживать правильный уровень масла трансмиссии. При изначальной установке колонки в верхней части корпуса карданного вала может скапливаться воздух. Эта пустота заполняется из бачка подпитки колонки во время обкатки поворотно-откидной колонки. Так как воздух из поворотно-откидной колонки выводится через колбу дозиметра смазки, уровень смазки в колбе падает.</p>
    <a name="17"></a>
    <h1 class="big">График технического обслуживания двигателей Mercruiser</h1>
    <a name="18"></a><h2>Обычное техническое обслуживание</h2>
    <p class="bbox"><strong>ПРИМЕЧАНИЕ:</strong> Выполняйте только то техническое обслуживание, которое относится к конкретному силовому агрегату.</p>
    <p> </p>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <tr bgcolor="#eeeeee">
            <td width="25%"><p><strong>Интервал выполнения задач</strong></p></td>
            <td width="75%"><p><strong>Техническое обслуживание, которое должно быть выполнено</strong></p></td>
        </tr>
        <tr>
            <td><p>Ежедневное – перед началом работы</p></td>
            <td><ul id="l94">
                    <li>
                        <p>Проверьте уровень масла в двигателе. (Вы можете увеличить этот интервал на основании своего опыта использования изделия.)</p>
                    </li>
                    <li>
                        <p>Проверьте уровень смазки редуктора поворотно-откидной колонки.</p>
                    </li>
                    <li>
                        <p>Проверьте уровень масла насоса дифферентной системы.</p>
                    </li>
                    <li>
                        <p>Проверьте насос гидроусилителя рулевого управления или уровень жидкости компактной гидросистемы управления, в зависимости от системы рулевого управления на вашей модели.</p>
                    </li>
                </ul></td>
        </tr>
        <tr>
            <td><p>Каждый день в конце работы</p></td>
            <td><ul id="l95">
                    <li>
                        <p>При эксплуатации в соленой, солоноватой или загрязненной воде следует промывать систему охлаждения после каждого использования.</p>
                    </li>
                </ul></td>
        </tr>
        <tr>
            <td><p>Еженедельно</p></td>
            <td><ul id="l96">
                    <li>
                        <p>Проверяйте водоприемники на сор и обрастание.</p>
                    </li>
                    <li>
                        <p>Проверьте фильтр забортной воды и очистите его (если он установлен).</p>
                    </li>
                    <li>
                        <p>Проверьте уровень охлаждающей жидкости.</p>
                    </li>
                    <li>
                        <p>Проверьте аноды поворотно-откидной колонки и замените их, если эрозия составляет 50% или более.</p>
                    </li>
                </ul></td>
        </tr>
        <tr>
            <td><p>Каждые два месяца или каждые 50 часов эксплуатации</p></td>
            <td><ul id="l">
                    <li>
                        <p>Снимите гребной винт и смажьте его вал, затем затяните гайку до определенного момента. (При эксплуатации только в пресной воде вы можете продлить этот интервал до четырех месяцев.)</p>
                    </li>
                    <li>
                        <p>При эксплуатации в соленой, солоноватой или загрязненной воде нанесите на силовой агрегат антикоррозийное средство.</p>
                    </li>
                    <li>
                        <p>Проверьте соединения аккумуляторной батареи и уровень жидкости.</p>
                    </li>
                    <li>
                        <p>Убедитесь, что все измерительные приборы и проводные соединения закреплены. Очищайте измерительные приборы. (При эксплуатации в соленой воде сократите интервал до 25 часов или 30 дней, в зависимости от того, что наступит раньше.)</p>
                    </li>
                </ul></td>
        </tr>
        </tbody>
    </table>
    <a name="19"></a><h2>Регламентное техобслуживание</h2>
    <p class="bbox"><strong>ПРИМЕЧАНИЕ:</strong> Выполнить только то техническое обслуживание, которое относится к конкретному силовому агрегату.</p>
    <table class="brdr" width="100%" border="1" cellspacing="0" cellpadding="5">
        <tbody>
        <tr bgcolor="#eeeeee">
            <td width="25%" bgcolor="#eeeeee"><p><strong>Интервал выполнения задач</strong></p></td>
            <td width="75%"><p><strong>Техническое обслуживание, которое должно быть выполнено</strong></p></td>
        </tr>
        <tr>
            <td><p>После периода обкатки в течение первых 20 часов</p></td>
            <td><p>Смените моторное масло и фильтр.</p></td>
        </tr>
        <tr>
            <td><p>Через каждые 50 часов работы или раз в 2 месяца (в зависимости от того, что наступит раньше)</p></td>
            <td><p>Все модели Bravo, кроме 496: Смажьте муфту двигателя (при эксплуатации на холостых оборотах в течение длительных периодов времени смазывайте муфту двигателя через каждые 50 часов).</p></td>
        </tr>
        <tr>
            <td><p>Через каждые 100 часов работы или ежегодно (в зависимости от того, что наступит раньше)</p></td>
            <td><ul id="l2">
                    <li>
                        <p>Выполните мелкий ремонт красочного покрытия на силовом агрегате.</p>
                    </li>
                    <li>
                        <p>Замените моторное масло и фильтр.</p>
                    </li>
                    <li>
                        <p>Замените смазку редуктора поворотно-откидной колонки.</p>
                    </li>
                    <li style="text-align: justify;">
                        <p>На моделях с замкнутой системой охлаждения проверьте уровень охлаждающей жидкости и концентрацию антифриза для адекватной защиты от замерзания. При необходимости исправьте. См. раздел <span>Технические характеристики</span>.</p>
                    </li>
                    <li>
                        <p>Затяните соединение кольца карданного подвеса с рулевым валом до заданного значения.</p>
                    </li>
                    <li>
                        <p>Замените водоотделительный топливный фильтр.</p>
                    </li>
                    <li>
                        <p>Проверьте систему рулевого управления и дистанционное управление на наличие незатянутых или поврежденных деталей и убедитесь, что все детали на месте. Смажьте кабели и рычажные механизмы.</p>
                    </li>
                    <li>
                        <p>Проверьте цепь заземления на наличие незакрепленных или поврежденных соединений. При наличии блока MerCathode проверьте его мощность.</p>
                    </li>
                    <li>
                        <p>Очистите пламегаситель, глушитель управления холостым ходом с помощью пневматического привода (IAC) и патрубки сапуна картера. Проверьте клапан принудительной вентиляции картера двигателя, если установлен.</p>
                    </li>
                    <li>
                        <p>Проверьте состояние и натяжение ремней.</p>
                    </li>
                    <li>
                        <p>Модели с удлиненным карданным валом: смажьте карданные шарниры карданного вала и входные и выходные подшипники задней бабки.</p>
                    </li>
                </ul></td>
        </tr>
        <tr>
            <td><p>Через каждые 150 часов работы или ежегодно (в зависимости от того, что наступит раньше)</p></td>
            <td><p>Все модели Bravo, кроме 496: Смажьте муфту двигателя.</p></td>
        </tr>
        <tr>
            <td><p>Через каждые 300 часов работы или раз в 3 года</p></td>
            <td><ul id="l3">
                    <li>
                        <p>Проверьте плотность затяжки опор двигателя и при необходимости подтяните.</p>
                    </li>
                    <li>
                        <p>Проверьте электросистему на наличие ослабленных, поврежденных или корродированных крепежных деталей.</p>
                    </li>
                    <li>
                        <p>Проверьте состояние свечей зажигания, проводов свечей зажигания, крышки распределителя и ротора, если они установлены. При необходимости замените.</p>
                    </li>
                    <li>
                        <p>Проверьте затяжку хомутов шлангов системы охлаждения и выхлопной системы. Проверьте обе системы на повреждения или протечки.</p>
                    </li>
                    <li>
                        <p>Разберите и осмотрите насос для забортной воды и замените изношенные компоненты.</p>
                    </li>
                    <li>
                        <p>На моделях с замкнутой системой охлаждения очистите секцию забортной воды замкнутой системы охлаждения. Очистите, осмотрите и проверьте крышку герметизированной системы.</p>
                    </li>
                    <li>
                        <p>Проверьте комплектующие выхлопной системы. Если агрегат оборудован заслонками водометного движителя (пластинчатыми откидными клапанами), убедитесь, что все клапаны на месте и нет изношенных клапанов.</p>
                    </li>
                    <li>
                        <p>Проверьте юстировку двигателя.</p>
                    </li>
                    <li>
                        <p>Осмотрите карданные шарниры, шлицы и сильфоны, проверьте зажимы.</p>
                    </li>
                    <li>
                        <p>Смажьте шлицы универсального шарнира и засечки, если они оборудованы масленкой.</p>
                    </li>
                    <li>
                        <p>Осмотрите подшипник карданного подвеса на предмет неровностей поверхности. При необходимости замените. Обратитесь к своему дилеру, сертифицированному компанией Mercury MerCruiser.</p>
                    </li>
                    <li>
                        <p>Модели Vazer, модели Alpha и модели Bravo (только 496 MAG): Смажьте муфту двигателя.</p>
                    </li>
                </ul></td>
        </tr>
        <tr>
            <td><p>Раз в 5 лет</p></td>
            <td><ul id="l4">
                    <li>
                        <p>Смените охлаждающую жидкость. Если не используется охлаждающая жидкость длительного срока эксплуатации, меняйте каждые два года.</p>
                    </li>
                </ul></td>
        </tr>
        </tbody>
    </table>
    <a name="20" id="20"></a>
    <h1 class="big">Таблица смазок морского назначения</h1>
    <p>В этой таблице представлено руководство по общему техобслуживанию с   применением морской смазки 2-4-C или смазки для экстремальных условий.   Графики техобслуживания для каждой конкретной модели прилагаются к   инструкции владельца двигателя и должны соблюдаться.</p>
    <div class="siteonly">
        <table width="100%" border="1" cellpadding="5" cellspacing="0" class="brdr">
            <tbody>
            <tr bgcolor="#eeeeee">
                <th width="40%" bgcolor="#eeeeee"> </th>
                <th width="20%"><p>Подвесные двигатели</p></th>
                <th width="20%"><p>Поворотно-откидные колонки</p></th>
                <th width="20%"><p>Периодичность для пресной воды</p></th>
                <th width="20%"><p>Периодичность для морской воды </p></th>
            </tr>
            <tr>
                <td><p>Рулевой механизм/ тросы, тяги</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / не менее раза в год</p></td>
                <td><p>50 ч / не менее раза в год</p></td>
            </tr>
            <tr>
                <td><p>Тросы и тяги газа/реверса</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч</p></td>
                <td><p>50 ч</p></td>
            </tr>
            <tr>
                <td><p>Пульты ДУ</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td><p>Вал гребного винта</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / 120 дн.</p></td>
                <td><p>50 ч / 60 дн.</p></td>
            </tr>
            <tr>
                <td><p>Шарнирные пальцы</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / 120 дн.</p></td>
                <td><p>50 ч / 60 дн.</p></td>
            </tr>
            <tr>
                <td><p>Механизмы фиксации наклона и оси наклона</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td>&nbsp;</td>
                <td><p>100 дн.</p></td>
                <td><p>50 дн.</p></td>
            </tr>
            <tr>
                <td><p>Шарнирные болты</p></td>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td><p>Шлицы приводного вала (муфты двигателя)</p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td><p>Подшипники карданного подвеса</p></td>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td><p>Карданный шарнир (1)</p></td>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="5"><p>(1) Модели Alpha One 1993 г. и новее оснащены карданным шарниром Perma-lube, который не требует смазки.</p></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="mobileonly">
        <table width="100%" border="1" cellpadding="5" cellspacing="0" class="brdr">
            <tbody>
            <tr bgcolor="#eeeeee">
                <th width="20%"><p>Подвесные двигатели</p></th>
                <th width="20%"><p>Поворотно-откидные колонки</p></th>
                <th width="20%"><p>Периодич-ность для пресной воды</p></th>
                <th width="20%"><p>Периодич-ность для морской воды </p></th>
            </tr>
            <tr>
                <td colspan="4">Рулевой механизм/ тросы, тяги</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / не менее раза в год</p></td>
                <td><p>50 ч / не менее раза в год</p></td>
            </tr>
            <tr>
                <td colspan="4">Тросы и тяги газа/реверса</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч</p></td>
                <td><p>50 ч</p></td>
            </tr>
            <tr>
                <td colspan="4">Пульты ДУ</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="4">Вал гребного винта</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / 120 дн.</p></td>
                <td><p>50 ч / 60 дн.</p></td>
            </tr>
            <tr>
                <td colspan="4">Шарнирные пальцы</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / 120 дн.</p></td>
                <td><p>50 ч / 60 дн.</p></td>
            </tr>
            <tr>
                <td colspan="4">Механизмы фиксации наклона и оси наклона</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td>&nbsp;</td>
                <td><p>100 дн.</p></td>
                <td><p>50 дн.</p></td>
            </tr>
            <tr>
                <td colspan="4">Шарнирные болты</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/morskaya-smazka-2-4-s-quicksilver-227-gr-detail">2-4-C</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="4">Шлицы приводного вала (муфты двигателя)</td>
            </tr>
            <tr>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="4">Подшипники карданного подвеса</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="4">Карданный шарнир (1)</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><p><a href="/mercruiser-zapchasti/filtryi-masla-i-smazki-quicksilver/smazka-quicksilver-dlya-ekstremalnykh-uslovij-397-gr-detail">Экстрем.</a></p></td>
                <td><p>100 ч / или раз в год</p></td>
                <td><p>50 ч / или раз в год</p></td>
            </tr>
            <tr>
                <td colspan="4">(1) Модели Alpha One 1993 г. и новее оснащены карданным шарниром Perma-lube, который не требует смазки.</td>
            </tr>
            </tbody>
        </table></div>
    <p>&nbsp;</p>
    <a name="21" id="21"></a>
    <h1 class="big">Выбор подходящего гребного винта</h1>
    <p>При выборе подходящего гребного винта очень важно иметь точный тахометр для измерения скорости
        двигателя.</p>
    <p>Выберите для своей лодки такой гребной винт, который позволит эксплуатировать двигатель в указанном  рабочем диапазоне с полностью открытой дроссельной заслонкой. При эксплуатации лодки с полностью  открытой дроссельной заслонкой при обычной нагрузке скорость вращения двигателя должна  находиться в верхней половине рекомендованного диапазона скоростей вращения для работы с  полностью открытой дроссельной заслонкой. Если скорость  вращения двигателя превышает этот диапазон, выберите гребной винт с увеличенным шагом, чтобы  понизить скорость вращения двигателя. Если скорость вращения двигателя ниже рекомендованного  диапазона значений, выберите гребной винт с уменьшенным шагом, чтобы повысить скорость вращения  двигателя.</p>
    <p> <strong>ВАЖНАЯ ИНФОРМАЦИЯ: Чтобы обеспечить правильное крепление и оптимальные рабочие  характеристики, «Mercury Marine» рекомендует использовать фирменные гребные винты Mercury или  Quicksilver и фирменные крепежные приспособления.</strong></p>
    <p>  Гребные винты различаются по диаметру, шагу, числу лопастей и материалу. Диаметр и шаг<br>
        проштампованы (отлиты) сбоку или на стороне ступицы гребного винта. Первое число означает диаметр<br>
        гребного винта, а второе - шаг. Например, цифры 14x19 означают, что гребной винт имеет диаметр 14<br>
        дюймов и шаг 19 дюймов.</p>
    <p><img src="/images/static/vint.jpg" width="514" height="300" alt="">  </p>
    <p>Далее представлены некоторые основные сведения о гребных винтах, которые помогут Вам правильно  выбрать гребной винт для своей лодки.</p>
    <p>  <strong>Диаметр</strong> - Диаметр проходит через воображаемый круг, который можно провести, когда гребной винт  вращается. Для каждого гребного винта заранее рассчитан правильный диаметр в зависимости от  конструкции вашего подвесного двигателя. Однако в тех случаях, когда для одного и того же шага  предлагается несколько диаметров, используйте больший диаметр для тяжелых лодок и меньший  диаметр для более легких.</p>
    <p><strong> Шаг </strong>- Шагом называется теоретическая величина в дюймах, на которую гребной винт перемещается  вперед во время выполнения одного оборота. Шаг можно воспринимать как нечто аналогичное  передачам в автомобилях. Чем ниже передача, тем быстрее будет разгоняться автомобиль, но при этом  максимальная скорость будет более низкой. Аналогично, гребной винт с меньшим шагом будет быстрее  ускоряться, но достигнет меньшей максимальной скорости. Чем больше шаг гребного винта, тем быстрее  обычно плавает такая лодка; хотя при этом она медленно разгоняется.</p>
    <p><strong>  Определение правильного размера шага </strong>- Сначала проверьте скорость вращения с полностью открытой  дроссельной заслонкой в условиях обычной нагрузки. Если скорость вращения с полностью открытой  дроссельной заслонкой находится в рекомендованном диапазоне, выберите запасной или новый  гребной винт с таким же шагом, что и имеющийся гребной винт.</p>
    <ul>
        <li>    Увеличение шага на 1 дюйм понизит скорость вращения с полностью открытой дроссельной  заслонкой на 150-200 оборотов в минуту</li>
        <li> Уменьшение шага на 1 дюйм повысит скорость вращения с полностью открытой дроссельной  заслонкой на 150-200 оборотов в минуту</li>
        <li> Замена гребного винта с 3 лопастями гребным винтом с 4 лопастями обычно понижает скорость  вращения с полностью открытой дроссельной заслонкой на 50-100 оборотов в минуту</li>
    </ul>
    <p>  <strong>ВАЖНАЯ ИНФОРМАЦИЯ: Избегайте повреждения двигателя. Никогда не используйте гребной винт,  который позволяет двигателю превысить рекомендованный диапазон скорости вращения с полностью  открытой дроссельной заслонкой при полностью открытой дроссельной заслонке.</strong></p>
    <h2>  МАТЕРИАЛ ГРЕБНОГО ВИНТА</h2>
    <p> Большинство гребных винтов, изготовленных «Mercury Marine», сделаны либо из алюминия, либо из  нержавеющей стали. Алюминий подходит для использования в обычных целях и стандартно  используется на многих современных лодках. Нержавеющая сталь по прочности более чем, в пять раз  превосходит алюминий и обычно обеспечивает более высокие характеристики для ускорения и  максимальной скорости благодаря высокому кпд. Гребные винты из нержавеющей стали также  предлагаются в более широком диапазоне размеров и типов, что позволяет покупателям получать исключительные рабочие характеристики для своей лодки.</p>
    <h2> 3 ЛОПАСТИ ИЛИ 4 ЛОПАСТИ</h2>
    <p> Гребные винты с 3 и 4 лопастями, различных размеров, из алюминия и из нержавеющей стали,  обеспечивают уникальные рабочие характеристики. В целом, гребные винты с 3 лопастями<br>
        обеспечивают хорошие характеристики и более высокие максимальные скорости, чем винты с 4  лопастями. Однако винты с 4 лопастями обычно обеспечивают более быстрое глиссирование и более  высокие крейсерские скорости, но не позволяют достигнуть таких максимальных скоростей, как для винтов с 3 лопастями.</p>


</div>