<div id="how-to-find-page">


    <div class="nbd">
        <ol>
            <li><strong><a href="/kak-najti#in1">Как найти детали по известному Вам номеру детали</a> </strong></li>
            <li><strong><a href="/kak-najti#in2">Как найти если номер детали не известен</a></strong></li>
            <li><strong><a href="/kak-najti#in3">Как найти детали по сериному номеру двигателя, колонки или транцевой
                        сборки</a></strong></li>
            <li><strong><a href="/kak-najti#i1">Как найти серийный номер двигателя Mercruiser</a> </strong></li>
            <li><strong><a href="/kak-najti#i2">Как найти серийный номер и передаточное число колонки Mercruiser Alpha
                        One</a></strong></li>
            <li><strong><a href="/kak-najti#i3">Как найти серийный номер и передаточное число колонки Mercruiser Bravo
                        1, 2, 3 X/XR<span style="letter-spacing:-1px"> (One, Two, Three)</span></a></strong></li>
            <li><strong><a href="/kak-najti#i5">Как найти серийный номер транца Mercruiser Alpha</a></strong></li>
            <li><strong><a href="/kak-najti#i4">Как найти серийный номер транца Mercruiser Bravo</a></strong></li>
            <li><strong><a href="/kak-najti#i9">Как найти серийный номер подвесного мотора Mercury</a></strong>

            </li>

            <li><strong><a href="/kak-najti#i9-1">Как найти серийный номер двухтактного подвесного мотора
                        Mercury</a></strong></li>
            <li><strong><a href="/kak-najti#i9-2">Как найти серийный номер четырехтактного подвесного мотора Mercury</a></strong>
            </li>
            <li><strong><a href="/kak-najti#i9-3">Как найти серийный номер подвесного мотора Mercury Verado</a></strong>
            </li>
            <li><strong><a href="/kak-najti#i9-4">Как найти серийный номер подвесного мотора Mercury
                        Optimax</a></strong></li>

        </ol>
        <p><br>
            Серийные номера являются ключами изготовителя к многочисленным деталям, относящимся к вашему силовому
            агрегату производства Mercruiser. Для простого и точного поиска деталей в нашем электронном каталоге мы
            специально выделили поиск запчастей по номеру агрегата на все страницы нашего сайта. Часто номер двигателя в
            судовом билете не указан, а номера колонок и транцевых сборок в регистрационных документах не указываются
            вообще, для таких случаев мы подготовили этот раздел.</p>

        <a name="in1"></a>
        <h1 class="big">Поиск по известному Вам номеру детали.</h1>
        <p>Номер детали может быть известен Вашему механику или сохранилась коробка от старой детали или номер есть на
            детали, которую сняли с двигателя. Оригинальный номер выглядит так:</p>
        <p><img src="/images/static/howtobuy_clip_image001.jpg" alt="" height="160" border="0"> <img
                src="/images/static/howtobuy_clip_image003.jpg" alt="" height="160" border="0"> <img
                src="/images/static/howtobuy_clip_image005.jpg" alt="" height="160" border="0"><br>
        </p>
        <p>В этом случае для поиска необходимо ввести данный номер в поле ПОИСК ЗАПЧАСТЕЙ по №, расположенного в левой
            части страницы. После нажатия кнопки «Найти» вы попадёте на страницу с результатами поиска. В случае если
            код распознался системой поиска Вы увидите список двигателей их серийные номера и разделы каталога в которых
            используется данная деталь. Нажав на интересующий раздел каталога Вы попадете на страницу со списком
            деталей, схемой по установке, где нужная деталь будет выделена желтым цветом.</p>
        <p>Далее можно приступать к заказу.</p>
        <a name="in2"></a>
        <h1 class="big"> Поиск в каталоге запчастей Mercury/Mercruiser </h1>
        <p>Когда номер детали неизвестен, допустим, мы ищем генератор на <a
                href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/">Mercruiser 5.0L</a>. На сайте размещён <a
                href="/catalog/">он-лайн каталог запчастей Mercury/Mercruiser</a>, в который можно попасть «кликнув» по
            банеру «Электронный каталог запчастей» и выбрав нужную Вам модель двигателя или поворотно-отктдной уолонки
            из списка. </p>
        <p>Алгоритм следующий: </p>
        <ul>
            <li> выбрать тип двигателя или колонки(например, <a href="/catalog/mercruiser-benzin/">бензиновый
                    стационарный</a>)
            </li>
            <li> выбрать параметры двигателя (например, <a
                    href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/">Mercruiser 5.0 L/LX/EFI/MPI</a>)
            </li>
            <li> выбрать модель двигателя (например <a
                    href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/5-0l-mpi-alpha-bravo/">5.0L MPI
                    ALPHA/BRAVO</a>)
            </li>
            <li> выбрать номер двигателя (например <a
                    href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/5-0l-mpi-alpha-bravo/615/">1A611928
                    THRU 1A623184</a>)
            </li>
            <li> в появившихся <a
                    href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/5-0l-mpi-alpha-bravo/615/">схемах</a>
                выбрать категорю деталей (например, <a
                    href="/catalog/mercruiser-benzin/mercruiser-5-0-l-lx-efi-mpi/5-0l-mpi-alpha-bravo/615/24119/">генератор
                    и кронштейны</a>)
            </li>
            <li> в выпадающем списке и схеме выбрать требуемую деталь (<a
                    href="/catalog/mercruiser-benzin/5-0l-mpi-alpha-bravo/615/24119/?part=863077T#5-0l-mpi-alpha-bravo">Генератор
                    в сборе со шкивом 65A</a>)
            </li>
        </ul>
        <p>Если на сайте имеется аналогичная деталь для замены, она появится вверху над списком, с кратким описанием и
            ценой и так же будет доступна для оформления покупки.</p>
        <p>Если поиск нужной детали вызывает затруднения, можно воспользоваться услугами нашей службы технической
            поддержки кликнув по ссылке <strong>ЗАПРОС ДЕТАЛИ ПО № ДВИГАТЕЛЯ</strong> в левой части страницы под поиском
            запчастей по № детали, и заполнив форму запроса на открывшейся странице. В течении суток (как правило, в
            течении 2-3 часов) наши консультанты подберут нужную деталь и отправят вам по эл.почте результаты.</p>
        <a name="in3"></a>
        <h1 class="big">Поиск в каталоге запчастей Mercury/Mercruiser по серийному номеру двигателя, колонки или
            транцевой сборки</h1>
        <p> В случае, когда номер агрегата на который Вы ищите детали известен, его необходимо ввести в поле <strong>ПОИСК
                ЗАПЧАСТЕЙ по № двигателя</strong>, расположенного центральной части страницы. После нажатия кнопки
            «Найти» вы попадёте на страницу с результатами поиска. где будут изображены разделы каталога, название
            двигателя, код двигателя и его серийный номер. Нажав на интересующий раздел каталога Вы попадете на страницу
            со списком деталей, схемой по установке, где можно будет выбрать нужную деталь и перейти к оформлению
            кокупки .</p>
        <a name="i1"></a>
        <h1 class="big">Номер двигателя Mercruiser на верхней крышке двигателя или пламегасителе</h1>
        <h2><strong>Инжекторные двигатели Mercruiser MPI</strong></h2>
        <p>Слева по ходу движения на торце крышки инжекторного двигателя Mercruiser MPI находится информационная
            табличка двигателя которая содержит серийные номера двигателя, транца и колонки</p>
        <ol>
            <li>Engine serial number - серийный номер двигателя</li>
            <li>Transom serial number - серийный номер транца</li>
            <li>Drive serial number - серийный номер колонки</li>
        </ol>
        <p align="center" class="sm"><img width="661" height="240" src="/images/static/instruct0.jpg"> <br>
            <img width="661" height="98" src="/images/static/instruct1.jpg"><br>
            Табличка двигателя, которая содержит серийные номера двигателя, транца и колонки.<strong><br clear="all">
            </strong></p>
        <h2><strong>Карбюраторные двигатели Mercruiser </strong></h2>
        <p>На карбюраторных двигателях табличка с маркировкой двигателя находится на пламегасителе карбюратора.</p>
        <ol>
            <li>Engine serial number - серийный номер двигателя</li>
            <li>Transom serial number - серийный номер транца</li>
            <li>Drive serial number - серийный номер колонки</li>
        </ol>
        <p align="center" class="sm"><img src="/images/static/instruct2.jpg" width="324" height="243" class="sm"><br>
            Этикетка с номером двигателя на пламегасителе <br>
        </p>
        <p> или на верхней крышке двигателя</p>
        <p align="center" class="sm"><img width="324" height="243" src="/images/static/instruct3.jpg"> <br>
            Этикетка с номером двигателя на верхней крышке двигателя </p>

        <h1 class="big">Номер двигателя на информационной этикетке выбросов в атмосферу </h1>
        <p>На информационной этикетке контроля выбросов в атмосферу которая, располагается на правом выпускном
            коллекторе по ходу движения указан: серийный номер двигателя, семейство двигателей, применимая норма
            выброса, дата изготовления (месяц, год) и рабочий объем двигателя. </p>
        <div align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr valign="top">
                    <td class="sm" style="padding-right:20px"><p align="center">
                            <img width="329" height="247" src="/images/static/instruct4.jpg"><br>
                            Информационная этикетка на правом коллекторе <br>
                            по ходу движения инжекторного двигателя Mercruiser MPI </p></td>
                    <td class="sm"><p align="center"><img width="329" height="247" src="/images/static/instruct5.jpg"><br>
                            Информационная этикетка на правом коллекторе <br>
                            по ходу движения карбюраторного двигателя Mercruiser</p></td>
                </tr>
                </tbody>
            </table>
        </div>
        <p align="center" class="sm"><img width="329" height="247" src="/images/static/instruct6.jpg"><br>
            Информационная этикетка на правом коллекторе по ходу движения</p>
        <p align="center" class="sm"><img width="583" height="235" src="/images/static/instruct7.jpg"><br>
            Информационная этикетка на правом коллекторе по ходу движения </p>
        <ol>
            <li>Применимые нормы</li>
            <li>Серийный̆ номер двигателя</li>
            <li>Семейство двигателей̆</li>
            <li>Норма на эмиссию семейства углеводородов и оксидов азота</li>
            <li>Дата изготовления</li>
            <li>Рабочий̆ объем двигателя, мощность двигателя</li>
            <li>Норма на эмиссию семейства моноксидов углерода</li>
        </ol>
        <h1 class="big">Шильда с серийным номером двигателя</h1>
        <p>Если номер двигателя на верхней крышке двигателя стерся, повредился, и информационная табличка на выпускном
            коллекторе отсутствует, то на всех двигателях Mercruiser и карбюраторных и инжекторных, есть Шильда с
            серийным номером двигателя. Табличка с серийным номером расположена в кормовой части по правому борту блока,
            примерно на 5 см выше двигателя стартера. <br>
            Выглядит она вот так:</p>
        <p align="center" class="sm">
            <img width="329" height="247" src="/images/static/instruct8.jpg"><br>
            Табличка с серийным номером двигателя </p>
        <p> Найти ее можно ориентируясь на эти фотографии:</p>
        <div align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top" class="sm" style="padding-right:20px"><p align="center">
                            <img width="329" height="247" src="/images/static/instruct9.jpg"><br>
                            Табличка с серийным номером двигателя </p></td>
                    <td valign="top" class="sm"><p align="center"><img width="329" height="247"
                                                                       src="/images/static/instruct10.jpg"><br>
                            Табличка с серийным номером двигателя</p></td>
                </tr>
                </tbody>
            </table>
        </div>
        <a name="i2"></a>
        <h1 class="big">Серийный номер и передаточное число поворотно-откидной колонки Mercruiser Alpha One</h1>
        <h2>Идентификация наклейки поворотно-откидной колонок серии Mercruiser Alpha One</h2>
        <p>Серийный номер и передаточное число поворотно-откидных колонок серии Alpha One проштампованы на наклейке
            которая расположена в верхней части колонки. Серийный номер привода находится на стороне левого борта
            поворотно-откидных колонок Alpha One. <br>
            Drive serial No - серийный номер колонки.</p>
        <p align="center" class="sm"><img width="329" height="247" src="/images/static/instruct11.jpg"><br>
            Серийный номер поворотно-откидной колонки Alpha One </p>
        <p>Передаточное число находится на стороне правого борта поворотно-откидных колонок Alpha One.<br>
            Drive ratio - передаточное число.</p>
        <p align="center" class="sm"><img width="329" height="247" src="/images/static/instruct12.jpg"><br>
            Передаточное число поворотно-откидной колонки Alpha One </p>
        <p align="center" class="sm"><img width="344" height="247" src="/images/static/instruct13.jpg"><br>
            Серийный номер (b) и передаточное число (a) поворотно-откидных колонок серии Alpha One </p>
        <a name="i3"></a>
        <h1 class="big">Серийный номер и передаточное число поворотно-откидной колонки Mercruiser Bravo 1, 2, 3 (One,
            Two, Three)</h1>
        <h2>Идентификация поворотно-откидной колонки серии Bravo по наклейке</h2>
        <p>Серийный номер и передаточное число поворотно-откидных колонок серии Bravo проштампованы на наклейке, которая
            расположена в верхней части колонки.<br>
            Серийный номер колонки и передаточное число находятся на стороне левого борта поворотно-откидных колонок
            Bravo. <br>
            Drive ratio - передаточное число.<br>
            Drive serial No - серийный номер колонки.</p>
        <div align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top" class="sm" style="padding-right:20px"><p align="center"><img width="329"
                                                                                                  height="247"
                                                                                                  src="/images/static/instruct14.jpg"><br>
                            Серийный номер и передаточное число <br>
                            колонки Bravo на наклейке</p></td>
                    <td valign="top" class="sm"><p align="center"><img width="329" height="247"
                                                                       src="/images/static/instruct15.jpg"><br>
                            Серийный номер и передаточное число <br>
                            колонки Bravo на наклейке</p></td>
                </tr>
                </tbody>
            </table>
        </div>
        <h2>Серийный номер на корпусе приводного вала</h2>
        <p>Серийный номер поворотно-откидной колонки Bravo, передаточное число, номер модели и штрих-код выдавлены на
            пластине заземления, расположенной по левому борту поворотно-откидной колонки.</p>
        <p align="center" class="sm"><img width="387" height="247" src="/images/static/instruct16.jpg"><br>
            Серийный номер поворотно-откидной колонки Bravo на пластине заземления </p>
        <p> Серийный номер проштампован также на корпусе приводного вала позади задней крышки.</p>
        <p align="center" class="sm">
            <img width="321" height="247" src="/images/static/instruct17.jpg"><br>
            Проштампованный серийный номер поворотно-откидной колонки Bravo </p>
        <a name="i4"></a>
        <h1 class="big">Серийный номер транца Bravo</h1>
        <p>Серийный номер транца Bravo проштампован на наклейке ниже логотипа Mercury.<br>
            Transom serial No - серийный номер транца</p>
        <p class="sm" align="center"><img width="329" height="247" src="/images/static/instruct18.jpg"><br>
            Серийный номер транца Bravo на наклейке. </p>
        <p class="sm" align="center"><img width="240" height="247" src="/images/static/instruct21.jpg"><br>
            Серийный номер транца Bravo на наклейке
        </p>
        <h2>Серийный номер транца Bravo на пластине стремянки</h2>
        <p>Серийный номер транца Bravo так же проштампован на пластине стремянки крепления на узле транца Bravo.</p>
        <p class="sm" align="center"><img width="240" height="247" src="/images/static/instruct19.jpg"><br>
            Расположение серийного номера на пластине стремянки крепления </p>
        <h2>Серийный номер транца Bravo на корпусе карданного подвеса</h2>
        <p>&nbsp;</p>
        <p>Если на наклейке номер стерся, а пластина отсутствует, то серийный номер транца Bravo можно найти на корпусе
            карданного подвеса.</p>
        <p class="sm" align="center"><img width="211" height="247" src="/images/static/instruct20.jpg"><br>
            Расположение серийного номера на корпусе карданного подвеса </p>
        <a name="i5"></a>
        <h1 class="big">Серийный номер транца Mercruiser Alpha One</h1>
        <p>Серийный номер транца Mercruiser Alpha One проштампован на наклейке ниже логотипа Mercury.<br>
            Transom serial No - серийный номер транца. </p>
        <p class="sm" align="center"><img width="329" height="247" src="/images/static/instruct18.jpg"><br>
            Серийный номер транца Mercruiser Alpha One на наклейке</p>
        <p class="sm" align="center"><img width="240" height="247" src="/images/static/instruct21.jpg"><br>
            Серийный номер транца Mercruiser Alpha One на наклейке
        </p>

        <a name="i9"></a>
        <h1 class="big">Как найти серийный номер подвесного мотора Mercury</h1>
        <h2><a name="i9-1" id="i9-1"></a>Серийный номер 2-х тактных подвесных лодочных моторов Mercury</h2>
        <p>Для моторов Mercury 2.5, Mercury 3.3<br>
            <img src="/images/static/outboard1.jpg" width="650" height="200" alt=""></p>
        <p>Для моторов Mercury 4, Mercury 5, Mercury 6, Mercury 8, Mercury 9.9, Mercury 15, Mercury 20, Mercury 25<br>
            <img src="/images/static/outboard2.jpg" width="650" height="200" alt=""></p>
        <p>Для моторов Mercury 30, Mercury 40, Mercury 50, Mercury 60<br>
            <img src="/images/static/outboard3.jpg" width="650" height="200" alt=""></p>
        <p>Для моторов Mercury 55, Mercury 60, Mercury 75, Mercury 90, Mercury 115, Mercury 125<br>
            <img src="/images/static/outboard4.jpg" width="650" height="200" alt=""></p>

        <h2><a name="i9-2" id="i9-2"></a>Серийный номер 4-х тактных подвесных лодочных моторов Mercury</h2>
        <p>Для моторов Mercury 2.5, Mercury 3.5<br>
            <img src="/images/static/outboard5.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury 4, Mercury 5, Mercury 6<br>
            <img src="/images/static/outboard6.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury 8, Mercury 9.9 <br>
            <img src="/images/static/outboard2.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury 15, Mercury 20<br>
            <img src="/images/static/outboard7.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury 25, Mercury 30<br>
            <img src="/images/static/outboard8.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury 40, Mercury 50, Mercury 60<br>
            <img src="/images/static/outboard9.jpg" width="650" height="200" alt=""> <br>
            <br>
            Для мотора Mercury 80, Mercury 100, Mercury 115 <br>
            <img src="/images/static/outboard11.jpg" width="650" height="200" alt=""></p>
        <p>Для мотора Mercury 150<br>
            <img src="/images/static/outboard10.jpg" width="650" height="200" alt=""></p>
        <h2><a name="i9-3" id="i9-3"></a>Серийный номер подвесных лодочных моторов Mercury Verado</h2>
        <p>Для моторов Mercury Verado 135, Mercury Verado 150, Mercury Verado 175, Mercury Verado 200<br>
            <img src="/images/static/outboard12.jpg" width="650" height="200" alt=""></p>
        <p> Для моторов Mercury Verado 225, Mercury Verado 250, Mercury Verado 250pro, Mercury Verado 300, Mercury
            Verado 300pro, Mercury Verado 350<br>
            <img src="/images/static/outboard14.jpg" width="650" height="200" alt=""></p>
        <h2><a name="i9-4" id="i9-4"></a>Серийный номер подвесных лодочных моторов Mercury Optimax</h2>
        <p>Для моторов Mercury Optimax 75, Mercury Optimax 90, Mercury Optimax 115, Mercury Optimax 125, Mercury Optimax
            135, Mercury Optimax 150, Mercury Optimax 175, Mercury Optimax 200, Mercury Optimax 225, Mercury Optimax 250<br>
            <img src="/images/static/outboard15.jpg" width="650" height="200" alt=""></p>
    </div>

</div>