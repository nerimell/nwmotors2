
    <h2>Накопительная скидка 3%-7%</h2>
    <p>Совершите больше покупок и получите накопительную скидку:</p>
    <p><strong class="roundski redski">3</strong><span class="percski">%</span> после вручения карты. <a href="#card">Как получить карту?</a><br> <strong class="roundski redski">5</strong><span class="percski">%</span> после достижения суммы покупок<strong> 200 000 руб</strong><br> <strong class="roundski redski">7</strong><span class="percski">%</span> после достижения суммы покупок <strong>500 000 руб</strong></p>
    <p><span style="font-size: 12.16px; line-height: 1.3em;">Если вы уже совершали покупки в нашем интернет-магазине, пожалуйста, войдите в личный кабинет.</span></p>
    <h2>Скидки для активных пользователей социальных сетей 3-5%</h2>
    <p><strong>Скидку можно получить, вступив в нашу группу <a href="http://vk.com/nwmotorsru">ВКонтакте</a>. Для этого необходимо:</strong></p>
    <table class="nobrd" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr valign="top">
            <td width="33%"><strong class="roundski blueski">1</strong>Перейти по <a href="http://vk.com/nwmotorsru" target="_blank">ссылке</a> и нажать на кнопку "Вступить группу".</td>
            <td width="33%"><strong class="roundski blueski">2</strong>Нажать "Рассказать друзьям"</td>
            <td width="33%"><strong class="roundski blueski">3</strong>Скопировать из адресной строки браузера ссылку на Вашу страницу, и отправить по электронной почте на адрес
                <script type="text/javascript">
                    <!--
                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                    var path = 'hr' + 'ef' + '=';
                    var addy74082 = 'sk&#105;dk&#105;' + '&#64;';
                    addy74082 = addy74082 + 'nwm&#111;t&#111;rs' + '&#46;' + 'r&#117;';
                    var addy_text74082 = 'sk&#105;dk&#105;' + '&#64;' + 'nwm&#111;t&#111;rs' + '&#46;' + 'r&#117;';
                    document.write('<a ' + path + '\'' + prefix + ':' + addy74082 + '\'>');
                    document.write(addy_text74082);
                    document.write('<\/a>');
                    //-->\n </script><a href="mailto:skidki@nwmotors.ru">skidki@nwmotors.ru</a><script type="text/javascript">
                    <!--
                    document.write('<span style=\'display: none;\'>');
                    //-->
                </script><span style="display: none;">Этот адрес электронной почты защищен от спам-ботов. У вас должен быть включен JavaScript для просмотра.
 <script type="text/javascript">
 <!--
 document.write('</');
 document.write('span>');
 //-->
 </script></span></td>
        </tr>
        <tr>
            <td><a href="http://vk.com/nwmotorsru" target="_blank"><img src="/images/static/skidki_vk1.jpg" border="0" alt=""></a></td>
            <td><a href="http://vk.com/nwmotorsru" target="_blank"><img src="/images/static/skidki_vk2.jpg" border="0" alt=""></a><a href="http://vk.com/nwmotorsru"></a></td>
            <td><a href="http://vk.com/nwmotorsru" target="_blank"><img src="/images/static/skidki_vk3.jpg" border="0" alt=""></a><a href="http://vk.com/nwmotorsru"></a></td>
        </tr>
        </tbody>
    </table>
    <p>После этого в ответном письме, менеджер нашего магазина, вышлет Вам код на скидку 3%, который необходимо будет ввести при оформлении заказа в Корзине. Если новость о нашей группе наберет на Вашей странице 5 репостов, мы вышлем Вам код со скидкой в 5%.</p>
    <p><a name="card"></a></p>
    <h3 class="big">Дисконтная карта</h3>
    <p><img src="/images/static/card1.jpg" border="0" alt=""> <img src="/images/static/card2.jpg" border="0" alt=""></p>
    <p>Дисконтную карту Вы можете получить по почте, если Вы делали заказы в нашем интернет-магазине.</p>
