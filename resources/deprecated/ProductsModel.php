<?php
/*
namespace App\Models;
use App\Models\CategoriesModel;
class ProductsModel extends Model {

    // common
    public $params;
    public $page;
    public $total;
    protected $q;
    public $path;
    public $simplePath;
    public $breadcrumbs;

    // products
    public $product;
    public $products;
    public $productAlias;
    public $namespaces;
    public $manufacturer;
    public $partOfAnother;

    // categories
    public $categories;
    public $category;
    public $pathElements;
    public $activeCategories = [];
    public $cm;


    function __construct() {
        $this->cm = CategoriesModel::getInstance();
    }


    function getProductById($id) {
        $this->product = \DB::table('products')
        ->select('products.*', 'product_prices.price_data')
        ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.product_id')
       // ->where('products.status', 1)
        ->where('products.product_id', $id);
        if(user_group_id) {
            $this->product->where('product_prices.user_group_id', user_group_id);
        } else {
            $this->product->where('product_prices.user_group_id', 0);
        }

        $this->product = $this->product->first();
        if(!$this->product) { return 0; }
        $this->product->priceinfo = \json_decode($this->product->priceinfo);
        $this->product->codes = self::getProductCodesById($this->product->product_id);
        $categoryIds = \DB::table('product_categories')->where('product_id', $this->product->product_id)->get();
        $this->categories = 0;
        if(!$categoryIds->count()) {
            $this->categories = []; $this->product->link = '/tovar/'.$this->product->alias;
        } else {
            if($categoryIds->count() > 1) {
                $catIds = [];
                foreach($categoryIds as $x) { $catIds[] = $x->category_id; }
                $this->categories = $this->cm->getCategoriesByIds($catIds);
                $this->product->link = '/tovar/'.$this->product->alias;
            } else {
                if(!$this->cm->getCategoryById($categoryIds[0]->category_id)) { return 0; }
                $this->categories = [$this->cm->category];
                $this->product->link = $this->cm->category->link.'/'.$this->product->alias;
                if(!empty($this->path)) { if('/'.$this->simplePath != $this->cm->category->link) { return 0; } }
            }
        }
        return 1;
    }

    static function getProductCodesById($id) {
        $codes = \DB::table('product_codes')->select('product_code')->where('product_id', $id)->get();
        if(!$codes) { return null; }
        $result = [];
        foreach($codes as $x) {
            $result[] = $x->product_code;
        }
        return $result;
    }


    function getProductByPath($path) {
        $this->path = rtrim($path, '/');
        $this->pathElements = explode('/', $path);
        $this->productAlias = array_pop($this->pathElements);
        $this->simplePath = implode('/', $this->pathElements);
        $product = $this->getProductByAlias($this->productAlias);

        if(!$product) { return 0; }
        return $this;
    }


    function getProductByAlias($alias) {
        $this->product = \DB::table('products')->select('products.*')->select('product_id')->where('status', 1)->where('products.alias', $alias)->first();
        if(!$this->product) { return 0; }
        return $this->getProductById($this->product->product_id);
    }

    function select($arr = false) {
        $this->q->select('products.title', 'products.product_id', 'product_prices.price_data', 'products.avatar', 'products.quantity', 'products.alias');
    }

    function searchByTitle() {
        $this->q = \DB::table('products')
            ->where('products.status', 1)
            ->where('products.title', 'LIKE', '%'.$this->params['q'].'%');
            $this->select();
    }

    function searchByEngine() {
        $this->q = \DB::table('products')
            ->where('products.status', 1);
            $this->select();
    }

    function searchByCode() {
        $this->q = \DB::table('products')
        ->whereRaw(\DB::raw("products.product_id IN (SELECT DISTINCT product_codes.product_id FROM product_codes WHERE product_codes.product_code LIKE '%".$this->params["q"]."%')"))
        ->where('products.status', 1);
        $this->select();
    }

    function joinPrices() {
        if($this->params['minp'] !== false || $this->params['maxp'] !== false) {
            if($this->params['minp'] !== false) {
                $this->q->where('product_prices.price', '>=', (float)$this->params['minp']);
            }
            if($this->params['maxp'] !== false) {
                $this->q->where('product_prices.price', '<=', (float)$this->params['maxp']);
            }
            $this->q->join('product_prices', 'product_prices.product_id', '=', 'products.product_id');
        } else {
            $this->q->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.product_id');
        }
    }

    function search() {
        $this->collectParams();
        $this->reformatParams();
        if($this->params['q']) {
            switch($this->params['ns']) {
                case 1: $this->searchByTitle(); break;
                case 2: $this->searchByCode(); break;
                case 3: $this->searchByEngine(); break;
                default: $this->searchByTitle(); break;
            }
        }

        if(user_group_id) {
            $this->q->where('product_prices.user_group_id', user_group_id);
        } else {
            $this->q->where('product_prices.user_group_id', 0);
        }
        $this->joinPrices();

       // dd($this->q->toSql());
        $this->total = $this->q->count();
        $this->products = $this->q
        ->take($this->perPage)
        ->skip(($this->params['p']-1)*$this->perPage)
        ->orderBy('product_prices.price', 'desc')->get();
        $this->completeProductsData($this->products);
        $this->breadcrumbs = $this->params['q'] ? [(object)['title' => 'Поиск', 'link' => false], (object)['title' => $this->params['q'], 'link' => false]] : [];
        return $this;
    }

    function getProductCategoryByPath($path) {
        $this->collectCategoryParams();
        $this->path = rtrim($path, '/');
        $this->pathElements = explode('/', $path);
        if(!$this->cm->getCategoryByPath($path)) { return 0; }
        $this->category = $this->cm->category;
        $this->activeCategories[] = $this->category->category_id;
        if(!empty($this->category->parents)) {
            foreach($this->category->parents as $x) {
                $this->activeCategories[] = $x->category_id;
            }
        }
        $this->getCategoryProducts($this->category->category_id);
        return $this;
    }

    function getProductCategoryById($id) {
        $this->collectCategoryParams();
        $this->category = $this->cm->getCategoryById($id);
        if(!$this->category) { return 0; }
        $this->category->parents = $this->cm->parents;
        $this->category->childs = $this->cm->childs;
        $this->activeCategories[] = $this->category->category_id;
        if(!empty($this->category->parents)) {
            foreach($this->category->parents as $x) {
                $this->activeCategories[] = $x->category_id;
            }
        }
        $this->getCategoryProducts($this->category->category_id);
        return $this;
    }

    function getCategoryProducts($id) {
        $this->q = \DB::table('products')
            ->where('products.status', 1)
            ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.product_id')
            ->select('products.title', 'products.product_id', 'product_prices.price_data', 'products.avatar', 'products.quantity', 'products.alias')
            ->where('product_categories.category_id', $id)
            ->join('product_categories', 'product_categories.product_id', '=', 'products.product_id');
        if(user_group_id) {
            $this->q->where('product_prices.user_group_id', user_group_id);
        } else {
            $this->q->where('product_prices.user_group_id', 0);
        }
        $this->total = $this->q->count();
        $this->products = $this->q
        ->orderBy('product_prices.price', 'desc')
        ->offset(($this->params['p']-1)*$this->perPage)
        ->take($this->perPage)
        ->get();
        //print_r($this->products); die;
        if(!$this->products) { return 0; }
        if(!$this->category) {
            $this->category = $this->getProductCategoryById($id);
        }
        $this->completeProductsData($this->products);
        return 1;
    }

    function completeProductsData(&$products) {
        if(empty($products)) { return; }
        $productIds = [];
        $productsById = [];
        foreach($products as $k => $v) {
            $productIds[] = $v->product_id;
            $products[$k]->categories = [];
            $products[$k]->codes = [];
            $products[$k]->priceinfo = \json_decode($products[$k]->priceinfo);
            $productsById[$v->product_id] = &$products[$k];
        }
        $productIds = array_unique($productIds);
        $categoriesIds = \DB::table('product_categories')
        ->whereIn('product_id', $productIds)
        ->get();
        $productCodes = \DB::table('product_codes')->whereIn('product_id', $productIds)->get();
        $categories = [];
        foreach($categoriesIds as $x) {
            $productsById[$x->product_id]->categories[] = $x->category_id;
            if(!isset($categories[$x->category_id])) {
                if($this->cm->getCategoryById($x->category_id)) {
                    $categories[$x->category_id] = $this->cm->category;
                }
            }
        }
        
        foreach($productCodes as $x) {
            $productsById[$x->product_id]->codes[] = $x->product_code;
        }

        foreach($products as $k => $v) {
            if(count($v->categories) == 1 && !empty($categories[$v->categories[0]])) {
                $products[$k]->link = $categories[$v->categories[0]]->link.'/'.$v->alias.'-detail';
            } else {
                $products[$k]->link = '/tovar/'.$v->alias.'-detail';
            }
        }

    }





    function getCategoriesByPath($path) {
        $this->cm->getCategoryByPath($path);
        return $this->cm->category;

    }


    function getCategoryByAlias($alias) {
        $key = 'category_alias_and_id_'.$alias;
        if(!\Cache::has($key)) {
            $q = \DB::table('categories')->where('alias', $alias);
            $obj =$q->first();
            if(!$obj) { return false; }
            \Cache::put($key, \json_encode($obj), 180);
        }
        return json_decode(\Cache::get($key));
    }

    function getCategoryIdByAlias($alias) {
        $key = 'category_id_of_'.$alias;
        if(!\Cache::has($key)) {
            $q = \DB::table('categories')->select('category_id')->where('alias', $alias);
            $obj =$q->first();
            if(!$obj) { return 0; }
            \Cache::put($key, $obj->category_id, 180);
        }
        return \Cache::get($key);
    }

    function getProductsByAlias($alias) {
        return \DB::table('products')->where('alias', $alias)->get();
    }

    function collectParams() {
        $this->params = [];
        $this->params['p'] = (int)\Request::get('p');
        $this->params['p'] = $this->params['p'] < 1 ? 1 : $this->params['p'];
        $this->params['q'] = mb_substr(trim(\Request::get('q')), 0, 40);
        $this->params['ns'] = \Request::get('ns');
        $this->params['minp'] = \Request::get('minp');
        $this->params['maxp'] = \Request::get('maxp');
    }

    function reformatParams() {
        if(!in_array($this->params['ns'], $this->getAvailableNamespaces())) {
            $this->params['ns'] = 1;
        }
        if(!\anInt($this->params['minp']) || !$this->params['minp']) { $this->params['minp'] = false; }
        if(!\anInt($this->params['maxp']) || !$this->params['maxp']) { $this->params['maxp'] = false; }
    }

    function getAvailableNamespaces() {
        if($this->namespaces) { return $this->namespaces; }
        return $this->namespaces = [1, 2, 3];
    }

    function collectCategoryParams() {
        $this->params = [];
        $this->params['p'] = (int)\Request::get('p');
        $this->params['p'] = $this->params['p'] < 1 ? 1 : $this->params['p'];
    }
}
*/