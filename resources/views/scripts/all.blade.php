<script>
    var csrf_token = '<?php echo csrf_token(); ?>';
    var tzd = '<?php echo tzd; ?>';
    var tz = '<?php echo tz; ?>';
    var defaultPreloader = '<?php echo defaultPreloader; ?>';
    var freeDeliveryLimit = parseInt('<?php echo $website->freeDeliveryLimit; ?>');
    var cartTotal = parseInt('<?php echo @$cart->totals->final_price; ?>');
</script>
<!-- Код тега ремаркетинга Google -->
<script type="text/javascript">
    var google_tag_params = {
        ecomm_prodid: 'REPLACE_WITH_VALUE',
        ecomm_pagetype: 'REPLACE_WITH_VALUE',
        ecomm_totalvalue: 'REPLACE_WITH_VALUE',
    };
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 929187083;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/929187083/?guid=ON&amp;script=0"/>
    </div>
</noscript>