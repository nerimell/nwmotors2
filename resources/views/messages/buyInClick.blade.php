<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />

    <style type="text/css">
        body,
        p,
        div {
            font-family: Roboto, arial, sans-serif;
            font-size: 16px;
            color: #4a4a4a;
        }

        .icon {
            width: 30px;
            height: 48px;
            text-align: center;
            float: left;
            margin-right: 10px;
        }
    </style>
    <style type="text/css">
        /* -------------------------------------
         GLOBAL RESETS
     ------------------------------------- */

        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%;
            height: auto;
        }

        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 16px;
            line-height: 1.2;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        h5 {
            font-size: 17px;
        }
        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%;
        }

        table td {
            font-family: Roboto, arial, sans-serif;
            font-size: 16px;
            vertical-align: top;
        }
        /* -------------------------------------
         BODY & CONTAINER
     ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%;
        }
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */

        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: auto !important;
            width: 580px;
        }
        /* This should also be a block element, so that it will fill 100% of the .container */

        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
        }
        /* -------------------------------------
         HEADER, FOOTER, MAIN
     ------------------------------------- */

        .main {
            background: #fff;
            width: 100%;
            box-sizing: border-box;
            padding: 8px;
        }

        .wrapper {
            box-sizing: border-box;
            padding: 20px;
            border: 1px solid #dddddd;
        }

        .footer {
            clear: both;
            padding-top: 10px;
            text-align: center;
            width: 100%;
        }

        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center;
        }
        /* -------------------------------------
         TYPOGRAPHY
     ------------------------------------- */
        .col-xs-12, .col-md-6 {
            float: left;
            position: relative;
        }
        .col-xs-12 {
            width: 100%;
        }
        h1,
        h2,
        h3,
        h4 {
            color: #4a4a4a;
            font-family: Roboto, arial, sans-serif;
            font-weight: 400;
            line-height: 1.1;
            margin: 0;
        }

        h1 {
            font-size: 22px;
            font-weight: bold;
            text-align: center;
        }

        h2,
        h3 {
            font-weight: bold;
        }

        h3 {
            margin-bottom: 10px;
        }

        h5 {
            color: #4a4a4a;
        }

        p,
        ul,
        ol {
            font-family: Roboto, arial, sans-serif;
            font-size: 18px;
            font-weight: normal;
            margin: 0;
            Margin-bottom: 15px;
        }

        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px;
        }

        a {
            color: #a9643f;
            text-decoration: underline;
        }
        /* -------------------------------------
         OTHER STYLES THAT MIGHT BE USEFUL
     ------------------------------------- */

        .last {
            margin-bottom: 0;
        }
        img {
            max-width: 100%;
        }
        .first {
            margin-top: 0;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        .mt0 {
            margin-top: 0;
        }

        .mb0 {
            margin-bottom: 0;
        }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0;
        }

        .powered-by a {
            text-decoration: none;
        }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0;
        }
    </style>
    <style type="text/css">
        .col {
            width: 100% !important;
            min-width: 230px !important;
            padding-top: 15px;
        }

        @media only screen and (max-width: 450px) {
            .bulletproof-button {
                background: #001435;
            }
            h1 {
                font-size: 24px;
            }
            .col {
                min-width: 0 !important;
            }
        }

        @media (min-width: 451px) {
            .wrapper {
                padding: 40px !important;
            }
            table[class="col"] {
                display: block !important;
                width: 230px !important;
                max-width: 230px !important;
            }
            .col {
                display: block !important;
                width: 230px !important;
                max-width: 230px !important;
            }
        }
        @media (min-width: 992px) {
            .col-md-6 {
                width: 50%;
            }
        }

        .param-group {
            text-align: left;
            float: left;
            width: 100%;
            margin-bottom: 5px;
        }
        .param-key, .param-value {
            float: left;
            min-height: 30px;
            box-sizing: border-box;
        }
        .param-value {
            padding-left: 10px;
        }
        .param-value a {
            font-size: 14px;
        }
        div {
            box-sizing: border-box;
        }
        a {
            color: #0077cc;
        }
    </style>
    <!--[if mso]>
    <style type=”text/css”>
        body {
            font-family: Arial, sans-serif;
        }
    </style>
    <![endif]-->
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" class="body">
    <tbody>
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">
                <!-- START CENTERED WHITE CONTAINER -->
                <table class="main">
                    <!-- START MAIN CONTENT AREA -->
                    <tbody>
                    <tr>
                        <td class="wrapper">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td><span class="sg-image" style="float: none; display: block; text-align: center;"><a href="{!! \URL::asset('/') !!}" style="text-align: center; display: block; margin-bottom: 5px"><img height="130" src="{!! \URL::asset(defaultImage) !!}" style="width: 130px; height: 130px;" width="130" /></a></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <h1 style="margin-bottom: 20px">На сайте <?php echo \Request::getHttpHost(); ?> появилась заявка на покупку в один клик</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                    <div class="col-xs-12" >
                                        <h5>Информация о клиенте</h5>

                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">имя:</div>
                                            <div class="param-value" style="width: 61%;">{!! @$data->first_name !!}</div>
                                        </div>
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">телефон:</div>
                                            <div class="param-value" style="width: 61%;">+7 {!! @$data->phone !!}</div>
                                        </div>
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">емайл:</div>
                                            <div class="param-value" style="width: 61%;">{!! @$data->email !!}</div>
                                        </div>
                                        @if(@$data->user)
                                            <div class="param-group">
                                                <div class="param-key" style="width: 31%; text-align: right;">логин:</div>
                                                <div class="param-value" style="width: 61%;">{!! @$data->user->username !!}</div>
                                            </div>
                                        @endif
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">ip адрес:</div>
                                            <div class="param-value" style="width: 61%;">{!! @$data->ip !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <h5>Информация о товаре</h5>
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">товар:</div>
                                            <div class="param-value" style="width: 61%;">
                                            <a href="{!! \URL::asset(@$data->product->link) !!}">{!! @$data->product->title !!}</a>
                                            </div>
                                        </div>
                                        @if(!empty($data->product->codes))
                                        @php $totalCodes = count(@$data->product->codes); @endphp
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">@if($totalCodes > 1)артикулы:@else артикул: @endif</div>
                                            <div class="param-value" style="width: 61%;">
                                                <?php
                                                if(@$data->product) {
                                                    calculateProductPrice($data->product);
                                                }
                                                for($i = 0; $i < $totalCodes; $i++) {
                                                ?>
                                                <a style="    color: #dd3e3e;" href="{!! \URL::asset('/search?q='.$data->product->codes[$i].'&ns=2') !!}">{!! $data->product->codes[$i] !!}</a>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">наша цена:</div>
                                            <div class="param-value" style="width: 61%; font-size: 15px;">{{ @number_format(@$data->product->priceinfo['final_price']) }} р.</div>
                                        </div>
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">ссылка на наш товар:</div>
                                            <div class="param-value" style="width: 61%;"><a href="{!! \URL::asset(@$data->product->link) !!}">{!! \URL::asset(@$data->product->link) !!}</a></div>
                                        </div>
                                        <div class="param-group">
                                            <div class="param-key" style="width: 31%; text-align: right;">ссылка на товар конкурента:</div>
                                            <div class="param-value" style="width: 61%;"><a href="{!! @$data->offer_link !!}">{!! @$data->offer_link !!}</a></div>
                                        </div>
                                    </div>
                                    @if(@$data->comment)
                                        <div class="col-xs-12">
                                            <h5>Комментарий клиента</h5>
                                            <div class="param-group">
                                                {!! $data->comment !!}
                                            </div>
                                        </div>
                                    @endif
                                    </td>


                                </tr>

                                <tr>
                                    <td style="height: 40px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 30px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!-- END MAIN CONTENT AREA -->
                    </tbody>
                </table>

                <!-- END FOOTER -->
                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>

</body>

</html>