<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />


</head>

<body style="background: #f4f0f0; padding: 40px;">


<table border="0" cellpadding="0" cellspacing="0" class="body" style="margin: 0 auto; background: #fff; font-family: Roboto,arial,sans-serif;">
    <tbody>
    <tr>
    <tr>
        <td style="height: 20px; width: 40px;">&nbsp;</td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>

    <tr>
        <td style="height: 20px; width: 40px;">&nbsp;</td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    <td style="height: 20px; width: 40px;">&nbsp;</td>

        <td class="container" style="border: 1px solid #dddddd; padding: 40px;">

                <!-- START CENTERED WHITE CONTAINER -->
                <table class="main">
                    <!-- START MAIN CONTENT AREA -->
                    <tbody>

                    <tr>
                        <td class="wrapper">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td><span class="sg-image" style="float: none; display: block; text-align: center;"><a href="{!! \URL::asset('/') !!}" style="text-align: center; display: block; margin-bottom: 5px"><img height="130" src="data:image/gif;base64,{!! base64_encode(file_get_contents(public_path().defaultImage)) !!}" style="width: 130px; height: 130px;" width="130" /></a></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">
                                        <h1 style="margin-bottom: 20px; font-size: 22px; text-transform: uppercase; color: #4a4a4a; font-weight: bold;">Клиент нашел более дешевый товар чем на сайте <?php echo \Request::getHttpHost(); ?></h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <h2 style="color: #4a4a4a;text-transform: uppercase;font-size: 17px;font-family: Roboto;">Информация о клиенте</h2>
                                        <table border="0" cellpadding="0" cellspacing="15" style="width: 100%">
                                            <tr>
                                                <td style="color: #919191;">имя:</td>
                                                <td>{!! @$data->first_name !!}</td>
                                            </tr>
                                            <tr>
                                                <td style="color: #919191;">телефон:</td>
                                                <td>+7 {!! @$data->phone !!}</td>
                                            </tr>
                                            <tr>
                                                <td style="color: #919191;">емайл:</td>
                                                <td>{!! @$data->email !!}</td>
                                            </tr>
                                            @if(@$data->user)
                                                <tr>
                                                    <td style="color: #919191;">логин:</td>
                                                    <td>{!! @$data->user->username !!}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td style="color: #919191;">ip адрес:</td>
                                                <td>{!! @$data->ip !!}</td>
                                            </tr>
                                        </table>
                                        <h2 style="color: #4a4a4a;text-transform: uppercase;font-size: 17px;font-family: Roboto;">Информация о товаре</h2>
                                        <table border="0" cellpadding="0" cellspacing="15" style="width: 100%">
                                            <tr>
                                                <td  style="color: #919191;">товар:</td>
                                                <td>
                                                    <a href="{!! \URL::asset(@$data->product->link) !!}">{!! @$data->product->title !!}</a>
                                                </td>
                                            </tr>
                                            @if(!empty($data->product->codes))
                                                @php $totalCodes = count(@$data->product->codes); @endphp
                                                <tr>
                                                    <td style="color: #919191;">@if($totalCodes > 1)артикулы:@else артикул: @endif</td>
                                                    <td>
                                                        <?php
                                                        if(@$data->product) {
                                                            calculateProductPrice($data->product);
                                                        }
                                                        for($i = 0; $i < $totalCodes; $i++) {
                                                        ?>
                                                        <span style="color: #dd3e3e;">{!! $data->product->codes[$i] !!}</span>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td  style="color: #919191;">наша цена:</td>
                                                <td>{{ @number_format(@$data->product->priceinfo['final_price']) }} р.</td>
                                            </tr>
                                            <tr>
                                                <td  style="color: #919191;">ссылка на наш товар:</td>
                                                <td><a href="{!! \URL::asset(@$data->product->link) !!}">{!! \URL::asset(@$data->product->link) !!}</a></td>
                                            </tr>
                                            <tr>
                                                <td  style="color: #919191;">ссылка на товар конкурента:</td>
                                                <td><a href="{!! @$data->offer_link !!}">{!! @$data->offer_link !!}</a></td>
                                            </tr>
                                            @if(@$data->comment)
                                            <tr>
                                                <td  style="color: #919191;">Комментарий клиента</td>
                                                <td>{!! $data->comment !!}</td>
                                            </tr>
                                            @endif
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="height: 40px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 30px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="height: 20px">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </td>
    <td style="height: 20px; width: 40px;"></td>
    </tr>
    <tr>
        <td style="height: 20px; width: 40px;"></td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    <tr>
        <td style="height: 20px; width: 40px;"></td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    </tbody>
</table>

</body>

</html>