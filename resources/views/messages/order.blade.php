<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />
</head>
@php
    $bot = curl_init();
    curl_setopt($bot, CURLOPT_RETURNTRANSFER, 1);
    $thStyle = 'style="border: solid 1px #e4e4e4;
                                                padding: 10px 7px;
                                                text-transform: lowercase;
                                                font-family: Roboto;
                                                text-align: center;
                                                color: #b5b5b5;
                                                font-size: 17px;"';
@endphp
<body style="background: #f4f0f0; padding: 40px;">
<table border="0" cellpadding="0" cellspacing="0"  style="margin: 0 auto; max-width: 100%;  background: #fff; font-family: Roboto,arial,sans-serif;">
    <tbody>
    <tr>
    <tr>
        <td style="height: 20px; width: 40px;">&nbsp;</td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>

    <tr>
        <td style="height: 20px; width: 40px;">&nbsp;</td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    <td style="height: 20px; width: 40px;">&nbsp;</td>

    <td class="container" style="border: 1px solid #dddddd; padding: 40px;">

        <!-- START CENTERED WHITE CONTAINER -->
        <table class="main" style="width: 100%;">
            <!-- START MAIN CONTENT AREA -->
            <tbody>

            <tr>
                <td class="wrapper">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                        <tr>
                            <td><span class="sg-image" style="float: none; display: block; text-align: center;"><a href="{!! \URL::asset('/') !!}" style="text-align: center; display: block; margin-bottom: 5px"><img height="130" src="data:image/jpeg;base64,{!! base64_encode(file_get_contents(public_path().defaultImage)) !!}" style="width: 130px; height: 130px;" width="130" /></a></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <h1 style="margin-bottom: 20px; font-size: 22px; text-transform: uppercase; color: #4a4a4a; font-weight: bold;">Новый заказ на сайте <?php echo env('APP_NAME'); ?></h1>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <h2 style="color: #4a4a4a;text-transform: uppercase;font-size: 17px;font-family: Roboto;">Информация о клиенте</h2>
                                <table border="0" cellpadding="0" cellspacing="15" style="width: 100%">
                                    <tr>
                                        <td style="color: #919191;">имя:</td>
                                        <td>{!! @$order->userinfo->fullname !!}</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #919191;">телефон:</td>
                                        <td>+7 {!! @$order->userinfo->phone !!}</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #919191;">емайл:</td>
                                        <td>{!! @$order->userinfo->email !!}</td>
                                    </tr>
                                    @if(@$order->userinfo->username)
                                        <tr>
                                            <td style="color: #919191;">логин:</td>
                                            <td>{!! @$order->userinfo->username !!}</td>
                                        </tr>
                                    @endif
                                    @if(@$order->userinfo->post_index)
                                    <tr>
                                        <td style="color: #919191;">почтовый индекс:</td>
                                        <td>{!! @$order->userinfo->post_index !!}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td style="color: #919191;">Тип аккаунта:</td>
                                        <td>{!! @$order->userinfo->user_type !!}</td>
                                    </tr>
                                   @if($order->userinfo->user_type && !empty($fields))
                                     @foreach($fields as $k => $v)
                                        @if($order->userinfo->{$v->name})
                                                <tr>
                                                    <td style="color: #919191;">{!! $v->title !!}:</td>
                                                    <td>{!! @$order->userinfo->{$v->name} !!}</td>
                                                </tr>
                                        @endif
                                     @endforeach
                                   @endif
                                    <tr>
                                        <td style="color: #919191;">ip адрес:</td>
                                        <td>{!! @$order->ip !!}</td>
                                    </tr>


                                </table>
                                <h2 style="    margin-top: 70px; color: #4a4a4a;text-transform: uppercase;font-size: 17px;font-family: Roboto;">Информация о заказе</h2>
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <thead>
                                        <th {!! $thStyle !!}>Фото</th>
                                        <th {!! $thStyle !!}>Название</th>
                                        <th {!! $thStyle !!}>Кол-во</th>
                                        <th {!! $thStyle !!}>Цена за единицу</th>
                                        <th {!! $thStyle !!}>Всего</th>
                                    </thead>
                                    @foreach($order->completeinfo->instances as $x)
                                    <tr>
                                    @if(@$x->complex_id)

                                    @else
                                    @php
                                        $path = \Html::productThumbnailPath($x->avatar);
                                        curl_setopt($bot, CURLOPT_URL, $path);
                                        $type = pathinfo($path, PATHINFO_EXTENSION);

                                        if(proxy) {
                                            curl_setopt($bot, CURLOPT_PROXY, proxy);
                                        }
                                        curl_setopt($bot ,CURLOPT_ENCODING , "gzip");

                                        $avatar = curl_exec($bot);
                                        $discount = number_format(@$instance->priceinfo->discount_value*@$instance->q);
                                    @endphp
                                        <td width="120" style="text-align: center; padding: 10px 10px 30px;">
                                            <img src="data:image/jpeg;base64,{!! base64_encode($avatar) !!}" style="max-width: 100px;"  />
                                        </td>
                                        <td style="text-align: center; padding: 7px 15px;">{!! $x->title !!}</td>
                                        <td style="text-align: center; padding: 7px 15px;">{!! $x->q !!} шт.</td>
                                        <td style="text-align: center; padding: 7px 15px;">
                                            <div @if(!$discount) style="display: none; "@endif>
                                                <p>
                                                        <span>
                                                            {!! number_format(@$x->priceinfo->price) !!}
                                                        </span> р.
                                                </p>
                                            </div>
                                            {!! @number_format(@$x->priceinfo->final_price) !!} р.
                                        <td style="text-align: center; padding: 7px 15px;">
                                                <div @if(!$discount) style="display: none;" @endif>
                                                    <p>
                                                        <span>
                                                            {!! number_format(@$x->priceinfo->price*@$x->q) !!}
                                                        </span> р.
                                                    </p>
                                                </div>
                                                <p><span class="final-price total-final-price">{!! @number_format(@$x->priceinfo->final_price*@$x->q) !!}</span> р.</p>
                                        </td>
                                    @endif
                                        <td></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td style="height: 40px">&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="height: 20px">&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="height: 30px">&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="height: 20px">&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="height: 20px">&nbsp;</td>
                        </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
    <td style="height: 20px; width: 40px;"></td>
    </tr>
    <tr>
        <td style="height: 20px; width: 40px;"></td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    <tr>
        <td style="height: 20px; width: 40px;"></td>
        <td></td>
        <td style="height: 20px; width: 40px;"></td>
    </tr>
    </tbody>
</table>

</body>

</html>