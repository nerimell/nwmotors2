@extends('layouts.front')
@section('page')
@php \DateFormatter::getDate(); @endphp
    <div id="articles-page">
        <h1>Новости</h1>
        @if(!empty($articles))
        @foreach($articles as $article)
        @php
            if(!empty($article->preview)) {
                $display = $article->preview;
            } else {
                $display = $article->content;
            }
            $display = strip_tags($display, '<p>');
        @endphp
            <div class="article-item">
                <div class="title">
                    <a href="{!! $article->link !!}">{!! $article->title !!}</a>
                    <div class="time"><i class="mdi mdi-clock"></i> {!! \DateFormatter::reformatDate($article->updated_at, 1) !!}</div>
                </div>
                <div class="article-content">
                {!! $display !!}
                </div>
            </div>
        @endforeach
        @include('elements.pagination')
        @endif
    </div>
@endSection