@extends('layouts.front')
@section('page')
    <h1>{!! $article->title !!}</h1>
    <div id="article-page">
        @php
            if(empty($article->content)) {
                $display = $article->preview;
            } else {
                $display = $article->content;
            }
        @endphp
        {!! $display !!}
    </div>
 @endSection