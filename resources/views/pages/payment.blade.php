@extends('layouts.front')
@section('page')
    <div id="static-page">
        <h1>{!! $page->title !!}</h1>
        @php
            $file = '../resources/static/pages/'.$page->page_id.'.php';
            if(is_file($file)) {
                include($file);
            }
        @endphp
    </div>
@endsection
