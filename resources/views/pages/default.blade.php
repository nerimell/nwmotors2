@extends('layouts.front')
@section('page')
    <div id="static-page">
    <h1>{!! $page->title !!}</h1>
    <div class="static-page"@if($page->css)id="{!! $page->css !!}"@endif>
    @php
    $file = '../resources/static/pages/'.$page->page_id.'.php';
    if(is_file($file)) {
        include($file);
    }
    @endphp
    </div>
    </div>
@endsection
