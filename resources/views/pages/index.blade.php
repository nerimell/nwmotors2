@extends('layouts.front')
@section('page')
    <div class="row">
        <div class="col-xs-12 mb20">
            @include('elements.slider')
        </div>
    </div>

    <div class="categories-list">
        <div class="row row-eq-height">
        @if(!empty($categories))
            @foreach($categories as $x)
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    @include('elements.shop.category.default')
                </div>
            @endforeach
            @endif
        </div>
    </div>
@endsection
