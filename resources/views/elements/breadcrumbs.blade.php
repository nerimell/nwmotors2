@if(@$breadcrumbs)
<ul id="breadcrumbs">
    <li>
        <a href="/">Главная</a>
    </li>
    @php $totalBreadcrumbs = count($breadcrumbs); @endphp
    @for($i = 0; $i < $totalBreadcrumbs; $i++)
    @php if($i == ($totalBreadcrumbs-1)) { $breadcrumbs[$i]->link = 0; } @endphp
    <li>
        @if($breadcrumbs[$i]->link)<a href="{!! $breadcrumbs[$i]->link !!}">@else<span>@endif{!! $breadcrumbs[$i]->title !!}@if($breadcrumbs[$i]->link)</a>@else</span>@endif
    </li>
    @endfor
</ul>
@endif