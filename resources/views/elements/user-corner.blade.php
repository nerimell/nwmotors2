<div id="user-corner" class="header-col">
    @if($user)
        <button type="button">
        <i class="mdi mdi-lock i-right"></i>Личный кабинет<i class="mdi mdi-menu-down drop-cart-icon"></i>
        </button>
        <div id="user-layout" class="head-dropdown">
            <div class="inner">
                <a href="/my/orders" rel="nofollow">Мои заказы</a>
                <a href="/profile" rel="nofollow">Мои настройки</a>
                <a href="/logout" rel="nofollow">Выйти</a>
            </div>
        </div>
    @else
        <button type="button" onclick="Actions.user.auth.callDialog()"><i class="mdi mdi-lock i-right"></i>Личный кабинет<i class="mdi mdi-menu-right drop-cart-icon"></i></button>
    @endif
</div>