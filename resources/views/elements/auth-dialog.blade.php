<div id="auth-dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-inner">
        <div class="modal-header">{!! $website->loginTitle !!}</div>
        <div class="modal-body">
            <form method="post" action="/async-login" onsubmit="return Actions.user.auth.login(event, this)">
                <div class="error-container"></div>
                <div class="success-container"></div>
                <div class="input-group m-b-15">
                    <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                    <input type="text" name="username" class="form-control" placeholder="логин" />
                </div>
                <div class="input-group m-b-15">
                    <span class="input-group-addon"><i class="mdi mdi-lock"></i></span>
                    <input type="password" name="password" class="form-control" placeholder="Пароль" />
                </div>
                <div class="row">
                    <div class="col-xs-6 text-left">
                        <a href="/restore-password" onclick="Actions.user.window.modal('hide')">Забыли пароль?</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="/registration" onclick="Actions.user.window.modal('hide')">Регистрация</a>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-lg btn-blue">Войти</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>