@php
    $menuitem->level++;
    $allowChilds = (!empty($menuitem->childs) && $menuitem->level < 3) ? 1 : 0;
    $class = '';

    if(@in_array($menuitem->category_id, @$activeCategories)) { $class .= 'active'; }
    if($allowChilds) { $class .= ' has-childs'; }
@endphp
@if($menuitem->level < 4)
    <li class="{!! $class !!} {!! $menuitem->css !!}">
        <a href="{!! $menuitem->link !!}"
           class="level-{!! $menuitem->level !!}">
                <span class="title">
                {!! $menuitem->title !!}
                </span>
            @if($allowChilds && !$menuitem->level)
                <i class="menu-icon"></i>
            @endif
        </a>
        @if($allowChilds)
            @php
              $class = @$menuitem->columns ? 'columns-'.$menuitem->columns : '';
            @endphp
            <ul class="childs {!! $class !!}">
              <div class="childs-wrapper">
                @if(!@$menuitem->columns)
                  <div class="column">
                  <div class="title"><a href="{!! $menuitem->link !!}">{!! $menuitem->title !!}</a></div>
                @endif
                @foreach($menuitem->childs as $x)
                  @if(!empty($x->childs))
                    <div class="column">
                    <div class="title"><a href="{!! $x->link !!}">{!! $x->title !!}</a></div>
                    @foreach($x->childs as $child)
                      @php $child->level = $menuitem->level; @endphp
                      @include('elements.menuItemPrototype', ['menuitem' => $child])
                    @endforeach
                    </div>
                    @else
                      @php
                          $x->level = $menuitem->level;
                      @endphp
                      @include('elements.menuItemPrototype', ['menuitem' => $x])
                    @endif
                @endforeach
                @if(!@$menuitem->columns)
                </div>
                @endif
                </div>
            </ul>
        @else
        @php
            $childsTotal = false;
            $class = false;
            $anchor = false;
            $colsCount = false;
            @endphp
        @endif
    </li>
@endif
