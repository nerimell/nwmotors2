<div id="fixed-wrapper">
    <div class="container">
        <div id="section-one" class="dark-blue">
            <div class="col-sm-12 col-md-7">
                <div class="row">
                    <div class="hidden-xs col-sm-6 text-center header-col">
                        {!! $website->slogan !!}<i class="mdi mdi-truck-fast"></i>
                    </div>
                    <div class="col-xs-12 col-sm-6 header-col i-right text-center text-lg-left">
                        <span>
                        Ваш город <button type="button" class="button-clear city-container" onclick="Actions.location.dialog()">{!! \Html::$city->title !!}</button>?
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 text-right">
                <div class="row">
                    <div class="inline-col">
                        @include('elements.shop.cart-corner')
                    </div>
                    <div class="inline-col">
                        @include('elements.user-corner')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-two">
    <div class="col-xs-12 col-md-6">
        <a href="/">
            {!! \Html::systemPicture('nwlogo.png', 'id="logo"') !!}
        </a>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="contacts">
          <div class="contact-row">
            <i class="mdi mdi-phone-settings"></i>
            <div class="phone">+7 (812) 949-89-89</div>
            <div class="desc">Магазин и склад в Санкт-Петербурге <a href="/kontakti#saint-petersburg">(Карта)</a></div>
          </div>
          <div class="contact-row">
            <i class="mdi mdi-phone-settings"></i>
            <div class="phone">+7 (495) 514-67-89</div>
            <div class="desc">Склад в Москве <a href="/kontakti#moscow">(Карта)</a></div>
          </div>
          <div class="free-call-text">БЕСПЛАТНЫЙ ЗВОНОК ИЗ ЛЮБОГО РЕГИОНА РОССИИ</div>
        </div>
    </div>
</div>
