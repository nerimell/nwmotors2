<div id="sidebar" class="sidebar-box sidebar-left">
    <div id="sidemenu">
        <ul>
            @foreach($menu->sidebar as $menuitem)
                @php $menuitem->level = -1; @endphp
                @include('elements.menuItemPrototype')
            @endforeach
        </ul>
    </div>
</div>
<div class="sidebar-box sidebar-left">
    <div class="widget-title widget-title-blue">Поиск</div>
    <div class="invisible-delimeter"></div>
    <form method="get" action="/search" id="search-form" onsubmit="return Actions.search.validate()">
        <div class="box search-box">
            <input type="text" class="form-control" autocomplete="off" id="q" name="q" placeholder="Начните ввод ..."
                   value="{!! @$params['q'] !!}"/>
            <button type="submit" title="найти" id="search-icon"><i class="mdi mdi-magnify"></i></button>
            <div class="control-group">
                <input type="radio" name="ns" class="namespace-cb radio-view"
                       @if(!@$params['ns'] || @$params['ns'] == 1) checked @endif value="1" id="in1"/>
                <label for="in1">Искать по названию и номеру детали</label>
            </div>
            <div class="control-group">
                <input type="radio" name="ns" class="namespace-cb radio-view" @if(@$params['ns'] == 2) checked
                       @endif value="2" id="in2"/>
                <label for="in2">Искать по серийному номеру двигателя</label>
            </div>

            <div class="delimeter"></div>
            <div class="title">Цена</div>
            <div class="row relative price-search-row search-filter-row">
                <div class="col-xs-12 col-sm-6">
                    <input type="text" class="form-control slight-fc" name="minp" onkeyup="submitOnEnter(event)"
                           onkeypress="return validateNumericInput(event)" placeholder="от" maxlength="8"
                           value="@if(@$params['minp'] !== false){!! @$params['minp'] !!}@endif"/>
                </div>
                <div class="price-delimeter">-</div>
                <div class="col-xs-12 col-sm-6">
                    <input type="text" class="form-control slight-fc" name="maxp" onkeyup="submitOnEnter(event)"
                           onkeypress="return validateNumericInput(event)" placeholder="до" maxlength="8"
                           value="@if(@$params['maxp'] !== false){!! @$params['maxp']  !!}@endif"/>
                </div>
            </div>
            <div class="category-search-row search-filter-row">
                @php /*
            @if(!empty($category))
            <input type="checkbox" id="only-this-cat" name="ca" checked value="{!! @$category->category_id !!}" />
            <label for="only-this-cat"></label><span>Искать в этой категории</span>
            @endif
            */
                @endphp
            </div>

            <button type="submit" id="search-btn">Найти</button>
            <div class="delimeter"></div>
        </div>
    </form>
</div>
<div id="coupon" class="sidebar-box sidebar-left hidden-xs hidden-sm hidden-md">
    <div class="widget-title widget-title-blue">Купоны! Скидки!</div>
    <div class="box">
        <form method="post" id="coupon-form"
              onsubmit="return Actions.coupon.activate(this, function(db) { if(db.result) { location.reload(); } })">
            <div class="invisible-delimeter"></div>
            <div class="checkout-calculator-title f-s-13 m-b-15 current-coupon-info">@if(App\Models\ShopModel::$coupon)
                    Активирован купон на {!! App\Models\ShopModel::$coupon->discount_value !!} % @endif</div>
            <input type="text" class="form-control slight-fc m-b-10" maxlength="20" name="coupon"
                   placeholder="Введите код купона"/>
            <button type="submit" class="btn green-btn w100 medium-btn">Использовать купон<i
                        class="mdi mdi-sale i-left"></i></button>
            <a href="/skidki">Как получить скидку?</a>
            <div class="invisible-delimeter"></div>
        </form>
    </div>
</div>
<div class="sidebar-box sidebar-left">
    <div class="widget-title widget-title-blue">Мы в соцсетях</div>
    <div class="box">
        <div class="invisible-delimeter"></div>
        <noindex><a href="{!! $website->social->twitter !!}" target="_blank" rel="nofollow"><img
                        src="/images/social/twitter-ico.jpg"/></a>
            <a href="{!! $website->social->vk !!}" target="_blank" rel="nofollow"><img src="/images/social/vk-ico.jpg"/></a>
        </noindex>
    </div>
</div>
