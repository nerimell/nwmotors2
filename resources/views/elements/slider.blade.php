<div class="wide-slider">
    <i class="mdi mdi-chevron-left ws-arrow ws-arrow-left"></i>
    <div class="slider-item">
        <div class="ws-from-left ws-first-row ws-row ws-xl ws-red ws-animation slide-in-left delay-1100">
            Большая распродажа
        </div>
        <div class="ws-from-left ws-sm ws-second-row ws-row ws-blue ws-animation slide-in-left delay-1500">
            лодочных моторов Mercury от 2.5 до 15 л.с
        </div>
        <img src="/images/banners/mercury-engines.png" class="slide-in-up delay-500 ws-animation duration-300" />
    </div>
    <i class="mdi mdi-chevron-right ws-arrow ws-arrow-right"></i>
</div>
