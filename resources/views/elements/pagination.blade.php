@if($total > $perPage)
    @php
        $totalPages = $total/$perPage;
        $totalPages = is_float($totalPages) ? (int)$totalPages+1 : $totalPages;
        $paramsArray = @$params ? (array)($params) : [];
        $page = $params['p'] ? $params['p'] : 1;
        if(!isset($class)) { $class = ''; }
        if(!isset($linkAttributes)) { $linkAttributes = ''; }
    @endphp
    <div class="pagination-wrapper{{ ' '.$class }}">
        <ul class="pagination">
            @if($page > 2)
                @php
                    unset($paramsArray['p']);
                    $query = http_build_query($paramsArray);
                    $query = $query ? '?'.$query : '';
                @endphp
                <li class="jump">
                    <a href="{{ $baseUrl }}{{ $query }}" {!! $linkAttributes !!}><i class="mdi mdi-chevron-double-left"></i></a>
                </li>
            @endif
            @if($page > 1)
                @php
                    $paramsArray['p'] = ($page-1);
                    $query = http_build_query($paramsArray);
                    $query = $query ? '?'.$query : '';
                @endphp
                <li class="jump">
                    <a href="{{ $baseUrl }}?{{ $query }}" {!! $linkAttributes !!}><i class="mdi mdi-chevron-left"></i></a>
                </li>
            @endif



            @php
                $pagesPrinted = 0;
                $pageInit = ($page >= $totalPages) ? $totalPages : $page;
                  for($i = ($pageInit-3); $pagesPrinted < 7; $i++) {
                    if($i < 1) { continue; }
                    if($i > $totalPages) {
                      if(!is_float($totalPages)) { break; }
                      if(($i - $totalPages) > 1) { break; }
                    }
                    if($i > 1) {
                        $paramsArray['p'] = $i;
                    } else {
                        unset($paramsArray['p']);
                    }
                    $query = http_build_query($paramsArray);
                    $query = $query ? '?'.$query : '';
            @endphp
            <li @if($i == $page) class="active" @endif>
                <a href="{{ $baseUrl }}{{ $query }}" {!! $linkAttributes !!}>{{ $i }}</a>
            </li>
            @php
                $pagesPrinted++;
                  }
            @endphp



            @if($page < $totalPages)
                @php
                    $paramsArray['p'] = $page+1;
                    $query = http_build_query($paramsArray);
                    $query = $query ? '?'.$query : '';
                @endphp
                <li class="jump">
                    <a href="{{ $baseUrl }}{{ $query }}" {!! $linkAttributes !!}><i class="mdi mdi-chevron-right"></i></a>
                </li>
            @endif
            @if(($totalPages-$page) > 1)
                @php
                    $paramsArray['p'] = $totalPages;
                    $query = http_build_query($paramsArray);
                    $query = $query ? '?'.$query : '';
                @endphp
                <li class="jump">
                    <a href="{{ $baseUrl }}{{ $query }}" {!! $linkAttributes !!}><i class="mdi mdi-chevron-double-right"></i></a>
                </li>
            @endif




        </ul>
    </div>
@endif
