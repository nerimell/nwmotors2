<div id="footer">

    <div class="footer-a">
        <div class="col-xs-12">
        Заметили ошибку или не понравилось обслуживание, пишите {!! $website->contact->directorEmail !!} - исправим!
        </div>
    </div>
    <div class="footer-b">
        <div class="col-xs-12">
        <p>Все товары на нашем сайте вы можете оплатить без комиссий — вы платите только за товар, комиссии платежных систем мы оплатим сами</p>
        <img src="/images/paymenticons.png" alt="Принимаем к оплате без комиссии" />
        </div>
    </div>
    <div class="footer-c">
        <div class="col-xs-12">
            <ul>
                @foreach($footer->menu as $x)
                    <li><a href="{!! $x->link !!}">{!! $x->title !!}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="footer-g">
        <div class="col-xs-12">
        {!! $footer->text !!}
        </div>
    </div>
</div>