<ul class="header-menu offset-group">
@foreach($menu->header as $menuitem)
  <li @if(@in_array($menuitem->link, @$activeLinks)) class="active" @endif><a href="{!! $menuitem->link !!}">{!! $menuitem->title !!}</a></li>
@endforeach
</ul>
