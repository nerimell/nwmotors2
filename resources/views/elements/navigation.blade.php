<div class="relative nav-wrapper dark-blue" id="nav-wrapper">
<div class="overflow-hidden">
<i class="mdi mdi-chevron-left navigate navigate-left" onclick="Nav.fn.left()"></i>
<ul id="nav">
    @foreach($menu->header as $menuitem)
        @php $menuitem->level = -1; @endphp
        @include('elements.menuItemPrototype')
    @endforeach
</ul>
<i class="mdi mdi-chevron-right navigate navigate-right" onclick="Nav.fn.right()"></i>
</div>
</div>