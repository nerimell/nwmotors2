<div id="<?php if(!@$dialogId) { ?>city-dialog<?php } else { ?>{!! $dialogId !!}<?php } ?>" class="city-dialog modal dialog-style-one dialog-style-one-l fade" @if(cd) data-empty="1" @else data-empty="0" @endif >
    <div class="modal-dialog">
        <div class="modal-inner">
            <div class="modal-header">Выберите ваш город</div>
            <div class="modal-body">
                @if(!cd)
                    <?php include('cache/html/locations/city-dialog-inner.html'); ?>
                @endif
            </div>
        </div>
    </div>
</div>