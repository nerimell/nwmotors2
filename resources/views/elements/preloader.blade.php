@php
    if(empty($spinnerClass)) {
        $spinnerClass = 'spinner-white-only';
    }

@endphp
<div class="asyncPreloader invisible-all">
    <div class="preloader-wrapper small active">
        <div class="spinner-layer {{ $spinnerClass }}">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
