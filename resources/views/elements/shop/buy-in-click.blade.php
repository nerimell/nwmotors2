<div id="buy-in-click-dialog" class="modal dialog-style-one fade">
    <div class="modal-dialog">
        <div class="modal-inner">
            <div class="modal-header">Покупка в один клик</div>
            <div class="modal-body">
                <div id="buy-in-click-thanks">
                    <p>Спасибо за вашу заявку, наш менеджер свяжется с вами в ближайшее время</p>
                    <div class="invisible-delimeter"></div>
                    <div class="f-s-14 specific-font-one">
                        <p class="orange">Внимание!</p>
                        <p>Наш офис работает с <span class="orange">9:30</span> до <span class="orange">18:00</span> по московскому времени.</p>
                        <p>Благодарим за понимание.</p>
                    </div>
                    <div class="invisible-delimeter"></div><div class="invisible-delimeter"></div>
                    <div class="text-right">
                        <button type="button" class="green-btn btn" onclick="Actions.product.hideBuyInClickDialog()">Закрыть</button>
                    </div>
                </div>
                <form method="post" action="/buy-in-click" id="buy-in-click-form" onsubmit="return Actions.product.buyInClick(this)">
                    <input type="hidden" name="product_id" value="{!! $product->product_id !!}" />
                    <div class="buy-in-click-product">
                        <div class="buy-in-click-product-avatar">
                            {!! \Html::productPicture(@$product->avatar) !!}
                        </div>
                        <div class="buy-in-click-product-title">
                            <div class="title-wrapper">
                            {!! @$product->title !!}
                            </div>
                        </div>
                    </div>
                    <div class="buy-in-click-phone-wrapper">
                        <div>
                        <span>Ваш телефон:</span>
                        <div class="phone-wrapper">
                          <input type="text" maxlength="15" name="phone" class="form-control phone" placeholder="(___) ___-__-__" value="{!! @$user->phone !!}" />
                        </div>
                        </div>
                        <p>Наш менеджер свяжется с вами по указанному вами телефону</p>
                    </div>
                    @php $hour = date('H'); @endphp
                    <div class="buy-in-click-comfortable-time @if(($hour >= 18) || ($hour < 9) || ($hour == 9 && date('i') <= 30)) active @endif">
                        <p>Наш офис работает с <span class="orange">9:30</span> до <span class="orange">18:00</span> по московскому времени</p>
                        <label>Выберите удобное время для звонка</label>
                        <div class="buy-in-click-comfortable-time-inputs">
                            <input type="text" name="min_call_time" class="form-control time-control"   maxlength="5" placeholder="с" />
                            <div class="buy-in-click-delimeter">-</div>
                            <input type="text" name="max_call_time" maxlength="5" class="form-control time-control"  placeholder="до" />
                        </div>
                    </div>
                        <div class="error-container">
                        </div>
                    <div class="text-right">
                        <button type="submit" class="btn green-btn" id="buy-in-one-click-submit">Купить сейчас</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/js/masked_input.min.js"></script>
<script>
  $('.time-control').mask('99:99');
  $('.phone').mask('(999) 999-99-99');
</script>
