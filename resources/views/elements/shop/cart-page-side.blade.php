<div class="checkout-calculator-title f-s-13">Введите купон, чтобы получить скидку:</div>
<input type="hidden" id="coupon-code-input" value="{!! @$coupon->coupon_code !!}" />
<form onsubmit="return CartObject.coupon.activate(this);">
    <input type="hidden" id="active-coupon-id" value="{!! @App\Models\ShopModel::$coupon->coupon_code !!}" />
    <input type="text" class="form-control slight-fc m-b-10" maxlength="20" name="coupon" placeholder="md5933ffcxffb">
    <button type="submit" class="btn silver-btn medium-btn w100 checkout-btn">Активировать купон</button>
    <div class="checkout-calculator-title f-s-13 text-right coupon-info-wrapper"  @if(!App\Models\ShopModel::$coupon) style="display: none;" @endif>Активирован купон на
        <span id="current-coupon-percent">{!! @App\Models\ShopModel::$coupon->discount_value !!}</span> %
    </div>
</form>
<div class="checkout-totals">
    <div class="checkout-row">
        <div class="checkout-key">Всего товаров:</div>
        <div class="checkout-value"><span
                    id="checkout-total-products">{!! $cart->totals->instances !!}</span>
            шт.
        </div>
    </div>
    <div class="checkout-row gifts-info-wrapper" @if(!$cart->totals->gifts) style="display: none;" @endif>
        <div class="checkout-key">Всего подарков:</div>
        <div class="checkout-value"><span
                    id="checkout-total-gifts">{!! $cart->totals->gifts !!}</span>
            шт.
        </div>
    </div>

    <div class="checkout-row coupon-info-wrapper" @if(!App\Models\ShopModel::$coupon) style="display: none;" @endif>
        <div class="checkout-key">Скидка от купона:</div>
        <div class="checkout-value"><span id="checkout-total-coupon-discount">{!! @number_format($cart->totals->coupon_discount) !!}</span> р.
        </div>
    </div>
    <div class="checkout-row"  @if(!$cart->totals->discount) style="display: none;" @endif>
        <div class="checkout-key">Скидка по акциям:</div>
        <div class="checkout-value"><span
                    id="checkout-total-base-discount">{!! @number_format($cart->totals->base_discount) !!}</span>
            р.
        </div>
    </div>
    <div class="checkout-row" @if(!$cart->totals->final_discount) style="display: none;" @endif>
        <div class="checkout-key">Общая скидка:</div>
        <div class="checkout-value"><span
                    id="checkout-total-final-discount">{!! @number_format($cart->totals->final_discount) !!}</span>
            р.
        </div>
    </div>

    <div class="checkout-row">

        <div class="checkout-key">Общая стоимость:</div>
        <div class="checkout-value"><span id="checkout-total-final-price">{!! @number_format($cart->totals->final_price) !!}</span> р.
        </div>
    </div>
</div>