@if(!empty($cart->instances))
    @foreach($cart->instances as $x)
        @if(isset($x->product_id)) @include('shop.cart.elements.mini-cart-product') @endif
        @if(isset($x->complex_id)) @include('shop.cart.elements.mini-cart-complex') @endif
    @endforeach
@endif