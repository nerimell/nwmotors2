@php
    $catOfTree->level++;
    $allowChilds = (!empty($catOfTree->childs) && $catOfTree->level < 2) ? 1 : 0;
@endphp
@if($catOfTree->status && !$catOfTree->hide_in_menu && $catOfTree->level < 3)
    <li @if(@in_array($catOfTree->category_id, @$activeCategories)) class="active" @endif>

        <a href="{!! $catOfTree->link !!}" title="{!! $catOfTree->category_id !!}"
           class="level-{!! $catOfTree->level !!} @if($catOfTree->menu_avatar) bg-menu @endif {!! $catOfTree->css !!}">
                <span class="title">
                {!! $catOfTree->menu_title !!}
                </span>
            @if($allowChilds && !$catOfTree->level)
                <i class="menu-icon"></i>
            @endif
        </a>
        @if($allowChilds)
            @php
                $childsTotal = count($catOfTree->childs);
                $class = false;
                $colsCount = 1;
                if($childsTotal > 7) {
                    $class = 'double';
                    $colsCount = 2;
                }
                if($childsTotal > 16) {
                    $class = 'triple';
                    $colsCount = 3;
                }
                $i = 0;

            @endphp
            <ul class="childs {!! $class !!}">
                @foreach($catOfTree->childs as $x)
                    @if($colsCount > 1 && $i && ($i % $colsCount == 0))</div>@endif
                    @if($colsCount > 1 && (($i % $colsCount == 0) || $i === 0)) <div class="menu-row">@endif
                    @php
                        $x->level = $catOfTree->level;
                    @endphp
                    @include('elements.shop.categoryPrototype', ['catOfTree' => $x])
                    @php $i++; @endphp
                @endforeach
                @if($colsCount > 1)</div>@endif
            </ul>
        @else
        @php
            $childsTotal = false;
            $class = false;
            $anchor = false;
            $colsCount = false;
            @endphp
        @endif
    </li>
@endif