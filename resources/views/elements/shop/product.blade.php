@php
    $titlelength = mb_strlen($product->title);
    if(@$product) {
        calculateProductPrice($product);
    }
@endphp
<div class="product-wrapper">
    @if($product->priceinfo['discount'])
        <div class="discount-corner">-{!! $product->priceinfo['discount_percent'] !!}%</div>
    @endif
    <a class="category-avatar-link" href="{!! $product->link !!}">
        @include('shop.category.products.gift')
        <div class="avatar">
            @if($product->avatar)
            {!! \Html::productThumbnail($product->avatar, 'class="default-avatar"') !!}
            @else
            <img src="{!! defaultImage !!}" />
            @endif
        </div>
    </a>
    <div class="product-name-wrapper">
    <a href="{!! $product->link !!}">
        @if($titlelength > 65)
            <h2 title="{!! $product->title !!}">{!! mb_substr($product->title, 0, 65) !!} ...</h2>
        @else
            <h2>{!! $product->title !!}</h2>
        @endif
    </a>
    </div>
    @if(isset($codesRowLg) && isset($codesRowMd))<div class="@if(empty($codesRowsLg[@$product->rowLg])) hidden-lg @endif @if(empty($codeseRowsMd[@$product->rowMd])) hidden-sm hidden-md @endif">@endif
        <div class="codesRow">
            <?php
            if(!empty($product->codes)) {
                $total = count($product->codes);
                ?>
                Артикул<?php if($total > 1) { ?>ы<?php } ?>:
                <?php
                    $comma = ',';
                    for($i = 0; $i < $total; $i++) {
                    if($i == ($total-1)) { $comma = ''; }
                ?>
                    <span>{!! trim($product->codes[$i]) !!}</span>{!! $comma !!}
                <?php

                }
               }
            ?>

        </div>
    @if(isset($codesRowLg) && isset($codesRowMd))</div>@endif
    @if(isset($priceRowsLg) && isset($priceRowsMd))
        <div class="@if(empty($priceRowsLg[@$product->rowLg])) hidden-lg @endif @if(empty($priceRowsMd[@$product->rowMd])) hidden-sm hidden-md @endif">
    @endif
    <div class="price<?php if(!@$basePriceRowsLg[$product->rowLg]) { echo ' base-price-hidden-lg'; } if(!@$basePriceRowsMd[$product->rowMd]) { echo ' base-price-hidden-md'; }?>">
    @if($product->priceinfo['compare'] >= 1)
            <div class="simple-price">
                @if($product->priceinfo['discount'])
                <div class="discounted">
                    {!! number_format($product->priceinfo['price'], 0, '', ' ') !!} р.
                   </div>
                 @endif
            </div>
            <div>
                <div class="discount-price">{!! number_format($product->priceinfo['final_price'], 0, '', ' ') !!} р.</div>
                @if($product->priceinfo['discount'])
                <div class="discount">Скидка: {!! number_format($product->priceinfo['discount_value'], 0, '', ' ') !!} р.</div>
                @endif
            </div>

    @endif
    </div>
    @if(isset($priceRowsLg) && isset($priceRowsMd))
        </div>
    @endif
    @if($product->priceinfo['compare'] >= 1)
        <button name="addtocart" class="green-btn btn-xl" onclick="Actions.cart.addProductFromList(<?php echo $product->product_id; ?>, this)"><i class="mdi mdi-cart i-right"></i>Купить</button>
    @else
        <a href="{!! $product->link !!}" class="green-btn btn-xl">Подробнее</a>
    @endif
</div>