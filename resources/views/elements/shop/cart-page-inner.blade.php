<?php
$totalInstances = count($cart->instances);
$totalGifts = 0;
if($totalInstances) {
for($i = 0; $i < $totalInstances; $i++) {
if (!empty($cart->instances[$i]->gifts)) {
    foreach ($cart->instances[$i]->gifts as &$x) {
        $x->avatar = \Html::productThumbnailPath($x->avatar);
        handleProductLink($x);
        calculateProductPrice($x);
    }
}
if(@$cart->instances[$i]->t == 'c') {
?>
@include('shop.checkout.complex-row', ['instance' => $cart->instances[$i]])
<?php continue; } ?>
@include('shop.checkout.product-row', ['instance' => $cart->instances[$i]])
<?php }
} else {
?>
    <tr><td colspan="6">
            <div class="err simple-err @if($cart->instances) invisible @endif" id="cart-error">
                <p>Ваша корзина пуста. <a href="/">Вернуться к покупкам</a></p>
            </div>
        </td></tr>
<?php } ?>
<input type="hidden" id="order-hash-summ" value="{!! $cart->hash !!}" />
