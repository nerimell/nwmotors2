<div id="cart-products-list" class="not-empty">
    @include('elements.shop.cart-products')
</div>
<div id="cart-totals" class="not-empty">
    <div class="pull-left cart-totals-bold">Всего: <span id="total">{!! number_format($cart->totals->final_price) !!}</span> рублей</div><div class="pull-right"><a href="/cart" id="go-to-cart">В корзину</a></div>
</div>
<div id="cart-is-empty" class="empty"><span class="pull-left">Ваша корзина пуста</span><i class="mdi mdi-refresh pull-right" title="обновить" onclick="Actions.cart.refresh()"></i></div>
