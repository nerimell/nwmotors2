<div id="external-offer-dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-inner">
        <div class="modal-header">Нашли дешевле?</div>
        <div class="modal-body">
        <p>Для того чтобы купить этот товар дешевле, пройдите в раздел <a href="/skidki/" target="_blank">СКИДКИ</a>, и узнайте как получить дополнительную скидку или отправьте нам ссылку с более низкой ценой на сайте конкурента.</p>
        <p><strong class="blue-color">Мы делаем скидку</strong>, при условии того, что товар в другом магазине:</p>
        <ul>
            <li> есть в наличии;</li>
            <li>совокупная стоимость самого товара и доставки до покупателя ниже нашего предложения.</li>
            <li>другой магазин является официальным дилером Mercury</li>
        </ul>
        <p>Мы проверим ссылку, и если она соответсвует нашим условиям, <strong class="blue-color">вышлем Вам промокод на скидку!</strong></p>
        <p>Данной функцией мы не гарантируем предоставить цену ниже заявленной, лишь обещаем принять сигнал о том, что наша цена выше чем у конкурентов, и постараемся сделать индивидуальную скидку.</p>
        <div class="invisible-delimeter"></div>
            <form method="post" action="/promotion-discount" onsubmit="return sendPromotionDiscount(this)">
        <p>Поля отмеченные * являются обязательными.</p>
        <div class="input-group">
        <label>Ваш E-mail *: </label>
        <input type="text" name="email" class="form-control" data-required="1" placeholder="Ваш E-mail" value="{!! @$user->email !!}">
        </div>
        <div class="input-group"><label>Ваше имя *:</label>
        <input type="text" name="first_name" class="form-control" data-required="1" placeholder="Ваше имя" value="{!! @$user->first_name !!}">
        </div>
        <div class="input-group"><label>Контактный телефон *: </label>
        <div class="phone-wrapper">
        <input type="text"  name="phone" class="form-control phone" data-required="1" value="{!! @$user->phone !!}">
        </div>
        </div>
        <div class="input-group">
          <label>Ссылка на товар дешевле *: </label>
        <input type="text" name="offer_link" class="form-control" data-required="1">
        </div>
        <div class="input-group"><label>Примечание: </label>
        <textarea name="comment" class="form-control" cols="79" rows="2"></textarea>
        </div>
        <div class="input-group">
        <div class="error-container-wrapper">
             <div class="error-container"></div>
        </div>
        </div>
        <input type="hidden" name="product_id" value="{!! $product->product_id !!}" />
        <div class="text-right">
            <button type="submit" class="btn btn-lg green-btn">Отправить</button>
        </div>
        </form>
        </div>
        </div>
    </div>
</div>

