@php
    $titlelength = mb_strlen($x->title);

@endphp
<div class="ez-parts-catalog-category-wrapper sweep-to-right sweep-blue">
    <a href="{!! $x->link !!}">
        @if($titlelength > 58)
            <h2 title="{!! $x->title !!}">{!! mb_substr($x->title, 0, 58) !!} ...</h2>
        @else
            <h2>{!! $x->title !!}</h2>
        @endif
    </a>
</div>
