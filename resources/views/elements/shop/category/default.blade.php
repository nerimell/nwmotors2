<div class="subcategory-wrapper">
    <a href="{!! $x->link !!}">
        <div class="avatar">
            {!! \Html::picture('categories/resized/'.$x->avatar) !!}
        </div>
    </a>
    <div class="category-name-wrapper">
        <a href="{!! $x->link !!}" title="{!! $x->category_id !!}">
                <h2>{!! prepareString($x->title, 103) !!}</h2>
        </a>
        <div class="subtitle">

        </div>
    </div>
</div>