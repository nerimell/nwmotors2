<div id="cart-corner" class="header-col @if(empty($cart->instances)) wrapper-of-empty @endif">
@php $totalProducts = count($cart->instances); @endphp
    <a href="/cart"><i class="mdi mdi-cart i-right" id="cart-icon"></i><span id="totalProducts-desc">{!!  $cart->display->products !!}</span><i class="mdi mdi-menu-down drop-cart-icon"></i></a>
    <div id="cart-layout" class="head-dropdown">
        <div class="inner" id="cart-inner">
            @include('elements.shop.cart-inner')
        </div>
    </div>
</div>

