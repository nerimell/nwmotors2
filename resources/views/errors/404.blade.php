@extends('layouts.error')
@section('page')
    <div class="vertical-align-wrapper">
        <div class="vertical-align-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <div class="error-wrapper">
                            <div class="error-page-heading">#404</div>
                            <div class="error-page-description">Извените, страница не найдена</div>
                            <div class="text-center" style="font-size: 18px;padding: 10px 0;font-family: Ossb;text-transform: lowercase;">
                                <a href="/" class="orange">На главную</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection