@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="/css/app.css"/>
    <script src="/js/libs/jquery.toast.js"></script>
@endsection
@section('body')
<style>
    .product-data {
        padding-left: 250px;
    }

    .xq-product-avatar {
        position: absolute;
        top: 15px;
        left: 15px;
        width: 230px;
    }

    .xq-product-avatar > a > img {
        width: 100%;
    }

    .xq-product-title {
        font-family: Ossb;
        font-size: 19px;
    }

    .image {
        width: 115px;
        float: left;
        padding: 5px;
    }

    .image img {
        cursor: pointer;
    }
</style>
    <div class="container">
    <div class="row">
    <div id="products-inner">
        @include('dev-products')
    </div>
    </div>
    </div>
    <script>
        var async = 0;
        function deletePair(a, b, code) {
            if(async) { return; }
            async = 1;
            $.ajax({
                url: '/doubles/deletePair',
                method: 'post',
                data: { a: a, b: b, code: code },
                dataType: 'json',
                success: function(db) {
                    if(!db.display) {
                        getNewPair();
                    } else {
                        Actions.error(db.display, 5000);
                        getNewPair();
                    }
                    async = 0;
                },
                error: function() {
                    async = 0;
                }
            });
        }

        function intoProduct(el) {
            if(async) { return; }
            async = 1;
            el = $(el).closest('.product-wrapper');
            var data = {
                a: $('#aid').val(),
                b: $('#bid').val(),
                code: el.find('input[name="code"]').val(),
                winner: el.find('input[name="product_id"]').val()
            };
            $.ajax({
                url: '/doubles/intoProduct',
                data: data,
                type: 'post',
                dataType: 'json',
                success: function(db) {
                    if(!db.display) {
                        getNewPair();
                    } else {
                        Actions.error(db.display, 5000);
                    }
                    async = 0;
                }, error: function() {
                    async = 0;
                }
            });
        }

        function getNewPair() {
            $.ajax({
                type: 'post',
                url: '/xqfce',
                success: function(db) {
                    $('#products-inner').html(db);
                    lazyLoad();
                }
            });

        }
        var csrf_token = '<?php echo csrf_token(); ?>';
        var tzd = 0;
    </script>
    <script src="/js/application.min.js"></script>
    <script src="/js/footer.js"></script>
@endsection