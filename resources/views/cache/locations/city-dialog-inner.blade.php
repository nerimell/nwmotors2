<div class="cities-list">
    <?php
        $totalCities = count($cities)+1;
        $totalRegions = count($regions);
        $delimeter = ($totalCities/3);
        for($i = 0; $i < $totalCities; $i++) {
        if($i == ($totalCities-1)) {
            ?><li onclick="Actions.location.selectAnother(this)">Выбрать другой</li><?php
            continue;
        }
        if($i%$delimeter == 0) {
        if($i) { echo '</ul></div>'; }
           echo '<div class="col-xs-12 col-md-4"><ul class="city-column">';
        }
    ?>
        <li class="city-el @if($cities[$i]->office) office @endif"  data-id="{!! $cities[$i]->city_id !!}">
            {!! $cities[$i]->title !!}
        </li>
    <?php } ?>
</div>
</div>
<div class="cities-description-wrapper">
    <span><i class="mdi mdi-map-marker"></i> маркером отметкой отмечены города с нашими складами</span>
</div>
<div class="cities-selector-wrapper select-another-city-wrapper"  style="display: none; ">
    <div class="cities-selector">
        <div class="cities-select-wrapper">
            <select onchange="Actions.location.changeRegion(this)">
                @for($i = 0; $i < $totalRegions; $i++)
                    <option value="{!! $regions[$i]->region_id !!}">{!! $regions[$i]->title !!}</option>
                @endfor
            </select>
        </div>
        <div class="cities-select-wrapper">
            <select class="cities-list-select">
                <?php include('cache/html/locations/regions/options/47.html'); ?>
            </select>
        </div>
        <button type="button" class="green-btn select-city-btn">Выбрать</button>
    </div>