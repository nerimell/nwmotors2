<div class="price-wrapper font-xl">
    @if(!$product->priceinfo['compare'])
        <button type="button" class="btn-deprecated btn-xl m-b-15">Товар временно недоступен</button>
        <div class="f-s-15  m-b-30 p-l-7">
            <a href="/ask-our-manager?product_id=<?php echo $product->product_id; ?>" rel="nofollow"
               class="ask-our-manager">Узнать о возможности приобретения у менеджера</a>
        </div>
    @else
        @php
            $priceText = 'Цена';
            if($product->priceinfo['discount']) {
                $priceText = 'Старая цена';
            }
        @endphp
        <div class="price">
            <div class="row">
                <div>
                    <div class="col-xs-5 text-right">{!! $priceText !!}:</div>
                    <div class="col-xs-7">
                        <div class="simple-price">
                            @if($product->priceinfo['compare'] <= 0)
                                <a href="/ask-our-manager" class="ask-our-manager">Уточнить у менеджера</a>
                            @else
                                @if($product->priceinfo['discount'])
                                    <div class="discounted">@endif
                                        {!! number_format($product->priceinfo['price'], 0, '', ' ') !!} р.
                                        @if($product->priceinfo['discount'])</div>@endif
                            @endif
                        </div>
                    </div>
                </div>
                @if($product->priceinfo['discount'])
                    <div class="new-price">
                        <div class="col-xs-5 text-right">Новая цена:</div>
                        <div class="col-xs-7">
                            <div class="discount-price">{!! number_format($product->priceinfo['final_price'], 0, '', ' ') !!}
                                р.
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-xs-5 text-right">Скидка:</div>
                        <div class="col-xs-7">
                            <div class="discount">{!! number_format($product->priceinfo['discount_value'], 0, '', ' ') !!}
                                р.
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="buy-in-one-click-wrapper">
            <button type="button" class="green-btn btn-xl buy-in-one-click" id="buy-in-click-btn"
                    onclick="Actions.product.buyInClickDialog(this)"><i class="mdi mdi-cursor-pointer i-right"></i>Купить в один клик
            </button>
        </div>
        <form class="form-horizontal" method="post" action="/add-to-cart"
              onsubmit="return Actions.cart.addProduct(this)">
              <div class="add-to-cart-form">
            <input type="hidden" name="product_id" value="{!! $product->product_id !!}"/>
            <div class="quantity">
                <input name="quantity" class="quantity-control"
                       onkeypress="return validateNumericInput(event)" value="1" maxlength="2"
                       type="text"/>
                <i class="mdi mdi-plus-box plus" onclick="Actions.cart.quantityPlus(this)"></i>
                <i class="mdi mdi-minus-box minus" onclick="Actions.cart.quantityMinus(this)"></i>
            </div>
            <button type="submit" class="green-btn btn-xl m-b-15"><i class="mdi mdi-cart i-right"></i>В корзину</button>
        </div>
        <button class="promotion-discount" type="button" data-toggle="modal" data-target="#external-offer-dialog">Нашли дешевле? <span
                    class="p-l-7">Получите скидку</span></button>
            <div class="product-instock silver-box">
                @if($product->quantity)
                    В наличии: <img src="/images/storage-four.png" class="product-in-stock-image" />
                @else
                    Заказ на {!! \DateFormatter::getPreorderingDate() !!}
                @endif
            </div>
        @include('shop.product.elements.gifts')
        </form>
        @include('shop.product.elements.complex')
        @include('shop.product.elements.delivery')
        <div class="invisible-delimeter"></div>
    @endif
</div>
