@if($product->complexes)
    <form class="sidebar-box silver-border m-b-10" onsubmit="return Actions.product.buyComplex(this)">
        <div class="widget-title widget-title-blue-2">Комплекты дешевле</div>
        <div class="invisible-delimeter"></div>
        @include('shop.product.elements.complexProduct', ['complexProduct' => $product, 'link' => 0])
        <div class="complex-plus"><i class="mdi mdi-plus-circle"></i></div>
        <div id="complex-variant">
            @php $link = 1; $i = 0; @endphp
            @foreach($product->complexes[0]->products as $complexProduct)
                @php if(@$complexProduct->ignore) { continue; } @endphp
                @if($i)<div class="complex-plus"><i class="mdi mdi-plus-circle"></i></div>@endif
                @include('shop.product.elements.complexProduct')
                @php $i++; @endphp
            @endforeach
        </div>
        <div id="complex-total">
            <div class="complex-base-price"><span id="complex-base-price">{!! number_format($product->complexes[0]->priceinfo['price']) !!}</span> р.</div>
            <div class="complex-sales-price"><span id="complex-final-price">{!! number_format($product->complexes[0]->priceinfo['final_price']) !!}</span> р.</div>
        </div>
        @if(count($product->complexes) > 1)
            <div class="complex-title">Вы можете выбрать другой вариант:</div>
            <div class="relative">
            <i class="mdi mdi-chevron-left complex-left complex-arrow" onclick="Actions.product.prevComplex()"></i>
            <select id="complex-select" name="complex" class="form-control" onchange="Actions.product.changeComplex(this)">
                @foreach($product->complexes as $k => $complex)
                    <option value="{!! $complex->complex_id !!}" data-price="{!! number_format($complex->priceinfo['price']) !!}" data-key="{!! $k !!}" data-final_price="{!! number_format($complex->priceinfo['final_price']) !!}">{!! $complex->title !!}</option>
                @endforeach
            </select>
            <div class="prototype">
                @foreach($product->complexes as $k => $complex)
                    <div id="complex-products-json-{!! $k !!}"><?php echo \json_encode($complex->products); ?></div>
                @endforeach
            </div>
            <i class="mdi mdi-chevron-right complex-right complex-arrow" onclick="Actions.product.nextComplex()"></i>
            </div>
            <div class="prototype">
            <div class="complex-plus" id="complex-plus-prototype"><i class="mdi mdi-plus-circle"></i></div>
            @include('shop.product.elements.complexProduct', ['complexProduct' => 0])
            </div>
        @endif
        <button type="submit" class="green-btn btn-xl m-t-14">Купить комплект</button>
    </form>
@endif