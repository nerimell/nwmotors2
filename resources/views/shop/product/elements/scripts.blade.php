@section('post-scripts')
    <script src="/js/product-slider.min.js"></script>
    <script>
        slider.init();
        document.addEventListener('DOMContentLoaded', function() {
            Actions.product.calculateDelivery(<?php echo $product->product_id; ?>)
        });
        var productPrice = parseInt('<?php echo $product->priceinfo['final_price']; ?>');
    </script>
@endsection