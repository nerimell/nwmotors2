<div class="side-product">
    @php
        calculateProductPrice($special);
        handleProductLink($special);
    @endphp
    @if($special->priceinfo['discount'])
        <div class="discount-corner">-{!! $special->priceinfo['discount_percent'] !!}%</div>
    @endif
    <div class="side-product-avatar">
        <a href="{!! $special->link !!}">{!! \Html::productThumbnail($special->avatar) !!}</a>
    </div>
    <div class="side-product-title"><a href="{!! $special->link !!}"><h5>{!! $special->title !!}</h5></a></div>
    <div class="side-product-price">
        @if($special->priceinfo['compare'] >= 1)
            <div class="simple-price">
                @if($special->priceinfo['discount'])
                    <div class="discounted">
                        {!! number_format($special->priceinfo['price'], 0, '', ' ') !!} р
                    </div>
                @endif
            </div>
            <div class="discount-wrapper">
                <div class="discount-price">{!! number_format($special->priceinfo['final_price'], 0, '', ' ') !!} р.</div>
            </div>

        @endif
    </div>
</div>
