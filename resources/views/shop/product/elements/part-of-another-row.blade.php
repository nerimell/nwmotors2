@if(!empty($product->parent_products))
    <div class="parts">
        <table class="parts-table parent-parts-table">
            <thead>
            <th class="part-category">Категория детали</th>
            <th class="part-title">Название детали</th>
            </thead>
            <tbody>
            <?php
            $totalParts = count($product->parent_products);
            for($i = 0; $i < $totalParts; $i++) {
            ?>
            <tr>
                <td class="part-category">
                @if(!empty($product->parent_products[$i]->categories))
                    @foreach($product->parent_products[$i]->categories as $rc)
                        <a href="{!! $rc->link !!}">{!! $rc->title !!}</a>
                    @endforeach
                @endif
                </td>
                <td class="part-title" data-id="{!! $product->parent_products[$i]->product_id !!}">@if(!$product->parent_products[$i]->hide_link)<a href="{!! $product->parent_products[$i]->link !!}">@endif{!! $product->parent_products[$i]->title !!}@if(!$product->parent_products[$i]->hide_link)</a>@endif</td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
@endif