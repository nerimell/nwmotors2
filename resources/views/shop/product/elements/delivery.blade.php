<div class="sidebar-box silver-border m-b-10" id="deliveries-sidebar-container">
    <div class="widget-title widget-title-blue-2">Информация о доставке</div>
    <div class="widget-subtitle widget-title-with-icon">
        <div class="m-b-5">Доставка бесплатна при сумме заказа от <span class="sidebar-delivery-price">{!! $website->freeDeliveryLimit !!}</span> р.</div>
        <div><i class="mdi mdi-truck-fast"></i>Цена доставки в город</div>
        <div><span class="sidebar-delivery-price">{!! \Html::$city->title !!}</span></div>
    </div>
    <div id="deliveries-inner">
        <div class="sidebar-delivery-row">
            <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pecom.jpg') !!}</div>
            <div class="sidebar-delivery-text">
                <div>Доставка до терминала</div>
                <span class="sidebar-delivery-price delivery-price-span" id="pecom-terminal-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
                <span class="delivery-is-free" style="display: none;">бесплатно</span>
            </div>
        </div>
        <div class="sidebar-delivery-row">
            <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pecom.jpg') !!}</div>
            <div class="sidebar-delivery-text">
                <div>Доставка до двери</div>
                <span class="sidebar-delivery-price" id="pecom-door-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
            </div>
        </div>
        <div class="sidebar-delivery-row">
            <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/jd.jpg') !!}</div>
            <div class="sidebar-delivery-text">
                <div>Доставка до терминала</div>
                <span class="sidebar-delivery-price delivery-price-span" id="jd-terminal-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
                <span class="delivery-is-free" style="display: none;">бесплатно</span>
            </div>
        </div>

    </div>
</div>