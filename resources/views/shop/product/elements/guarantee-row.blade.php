<div class="col-xs-12 post-description-info">
  @if(@$product->manufacturer->guarantee_text)
  <div class="title">Информация о гарантии</div>
    {!! $product->manufacturer->guarantee_text !!}
  @endif
  <div class="title">Информация о условиях поставки</div>
  Товар отгружается со склада только после получения 100% оплаты
  <div class="title">Информация о возврате и обмене товара</div>
  <a href="/vozvrat">Информация о правилах возврата и обмена товара</a>
</div>
