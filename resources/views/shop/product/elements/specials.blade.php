@if(!empty($specials))
    <div class="col-xs-12">
        <div class="sidebar-box silver-border"  id="special-products">
            <div class="widget-title widget-title-blue-2">Специальное предложение</div>
    @foreach($specials as $special)
        @include('shop.product.elements.specialProduct')
    @endforeach
        </div>
    </div>
@endif