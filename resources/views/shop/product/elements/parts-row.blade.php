
@if(!empty($product->child_products))
    <div class="parts smooth-white">
       <table class="parts-table">
        <thead>
            <th class="part-avatar"><i class="mdi mdi-file-image"></i></th>
            <th class="part-number">#</th>
            <th class="part-code">Артикул детали</th>
            <th class="part-qty"><p>Кол</p><p>во</p></th>
            <th class="part-title"><p>Назвние</p><p>детали</p></th>
            <th class="part-price"><p>Цена</p><p>детали</p></th>
            <th class="part-order"><p>На</p><p>складе</p></th>
            <th class="part-actions"><p>купить</p><p>деталь</p></th>
        </thead>
        <tbody>
            <?php
                $totalParts = count($product->child_products);
                for($i = 0; $i < $totalParts; $i++) {
                   $product->child_products[$i]->scheme = \json_decode($product->child_products[$i]->scheme);
                   calculateProductPrice($product->child_products[$i]);

            ?>
                <tr>
                    <td class="part-avatar">
                        @if($product->child_products[$i]->avatar)
                            {!! \Html::productThumbnail($product->child_products[$i]->avatar) !!}
                            @else
                            <img src="/images/codesm.png" />
                        @endif
                    </td>
                    <td class="part-number">{!! @$product->child_products[$i]->scheme->number !!}</td>
                    <td class="part-code">
                        @if(!empty($product->child_products[$i]->codes))
                            @foreach($product->child_products[$i]->codes as $code)
                                <p>{!! $code !!}</p>
                            @endforeach
                        @endif
                    </td>
                    <td class="part-qty">{!! $product->child_products[$i]->consistance_quantity !!}</td>
                    <td class="part-title">
                    @if(!$product->child_products[$i]->hide_link)<a href="{!! $product->child_products[$i]->link !!}">@endif{!! $product->child_products[$i]->title !!}@if(!$product->child_products[$i]->hide_link)</a>@endif
                    @if(!empty(@$product->child_products[$i]->scheme->hint))<span class="part-hint">({!! trim($product->child_products[$i]->scheme->hint) !!})</span>@endif
                    </td>
                    <td class="part-price">
                        @if($product->child_products[$i]->priceinfo['compare'])
                            @if($product->child_products[$i]->priceinfo['discount_value'])
                            <div class="part-price-row"><div class="part-discount-price part-price-wrapper">{!! number_format($product->child_products[$i]->priceinfo['price']) !!}</div> р.</div>
                            @endif
                            <div class="part-price-row"><div class="part-final-price part-price-wrapper">{!! number_format($product->child_products[$i]->priceinfo['final_price']) !!}</div> р.</div>
                        @else
                            -
                        @endif
                    </td>
                    <td class="part-order">
                        @if($product->child_products[$i]->priceinfo['compare'])
                            @if($product->child_products[$i]->quantity)
                                <img src="/images/storage-five.png" title="в наличии" />
                                @else
                                <img src="/images/storage-preorder.png" title="Под заказ на {!! \DateFormatter::getPreorderingDate() !!}" />
                            @endif
                        @else
                            <img src="/images/storage-disabled.png" title="Товар временно не доступен" />
                        @endif
                    </td>
                    <td class="part-actions">
                    @if($product->child_products[$i]->priceinfo['compare'])
                    <button type="button" class="btn green-btn" onclick="Actions.cart.addProductFromTable(<?php echo $product->child_products[$i]->product_id; ?>, this)">купить</button>
                    @endif
                    </td>
                </tr>
            <?php } ?>
        </tbody>
       </table>
    </div>
@endif
