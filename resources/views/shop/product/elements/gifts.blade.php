@if($product->gifts)
@php $totalGifts = count($product->gifts); $severalGifts = ($totalGifts > 1); @endphp
    <div class="sidebar-box silver-border m-b-10" id="gifts-container" onsubmit="return false">
        <div class="widget-title widget-title-blue-2">@if($severalGifts) Выберите подарок @else Подарок к товару @endif</div>
        @for($i = 0; $i < $totalGifts; $i++)
            <div class="product-gift">
                <div class="product-gift-avatar"><a href="{!! $product->gifts[$i]->link !!}">{!! \Html::productThumbnail($product->gifts[$i]->avatar) !!}</a></div>
                <div class="product-gift-action">
                    @if($severalGifts)
                    <input type="radio" class="radio-to-checkbox" onchange="Actions.product.changeGift(this)" id="gift-{!! $i !!}" @if(!$i) checked @endif name="gift" value="{!! $product->gifts[$i]->product_id !!}" />
                    <label class="gift-label" for="gift-{!! $i !!}"><span data-id="gift-{!! $i !!}">@if($i)Выбрать этот подарок @else Подарок выбран @endif</span></label>@endif
                </div>
                <div class="product-gift-title">{!! $product->gifts[$i]->title !!}</div>
                <div class="product-gift-price"><span>{!! number_format($product->gifts[$i]->priceinfo['final_price']) !!}</span> р.</div>
            </div>
        @endfor
    </div>
@endif