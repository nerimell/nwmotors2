@php $rows = 0;  @endphp
<div class="product-information row">
    <div class="col-xs-12">
        <div class="info-row row">
            @if(count($product->codes) == 1)
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 col-sm-6 key">
                            <span class="red">Артикул:</span>
                    </div>
                    <div class="col-xs-12 col-sm-6 value-col">
                        @if(!empty($product->codes))
                        @foreach($product->codes as $code)
                                <span class="value cat-link"><h2 class="value" >{!! $code !!}</h2></span>
                        @endforeach
                        @else
                            <div class="value">отсутствует</div>
                        @endif
                    </div>
                </div>
                @php $rows++; @endphp
                @endif
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 col-sm-6 key">В наличии:</div>
                <div class="col-xs-12 col-sm-6 value-col">
                    <div class="value">@if($product->quantity) Да @else Нет @endif</div>
                </div>
                @php $rows++; @endphp
            </div>
            @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @if(!$product->manufacturer)
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 col-sm-6 key">Производитель:</div>
                <div class="col-xs-12 col-sm-6 value-col">
                    <div class="value">Не указан</div>
                </div>
                @php $rows++; @endphp
            </div>
            @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @endif
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 col-sm-6 key">Вес:</div>
                <div class="col-xs-12 col-sm-6 value-col value">
                    @if(@$product->weight) {!! number_format($product->weight, 2) !!} кг @else Не
                    указан @endif
                </div>
                @php $rows++; @endphp
            </div>
            @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @if($product->length)
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 col-sm-6 key">Длина:</div>
                <div class="col-xs-12 col-sm-6 value-col value">
                        {!! $product->display->length !!}
                </div>
                @php $rows++; @endphp
            </div>
            @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @endif
            @if($product->height)
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 col-sm-6 key">Высота:</div>
                    <div class="col-xs-12 col-sm-6 value-col value">
                        {!! $product->display->height !!}
                    </div>
                    @php $rows++; @endphp
                </div>
                @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @endif
            @if($product->width)
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 col-sm-6 key">Ширина:</div>
                    <div class="col-xs-12 col-sm-6 value-col value">
                        {!! $product->display->width !!}
                    </div>
                    @php $rows++; @endphp
                </div>
                @if($rows % 2 == 0) </div><div class="info-row row"> @endif
            @endif
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 col-sm-6 key">Доставка на:</div>
                <div class="col-xs-12 col-sm-6 value-col value">
                    @if($product->quantity) От 1-го дня @else <a href="/ask-our-manager" class="value" rel="no-follow">уточнить у менеджера</a> @endif
                </div>
                @php $rows++; @endphp
            </div>
            @if($rows % 2 == 0) </div><div class="info-row row"> @endif
        </div>
        <div class="invisible-delimeter"></div>
        @if(@$product->data->country)
            <div class="info-row-light">
                <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5 key">Страна производитель:</div>
                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 value-col">
                    <div class="value"> {!! @$product->data->country !!} </div>
                </div>
            </div>
        @endif
        @if($product->manufacturer)
            <div class="info-row-light">
                <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5 key">Производитель:</div>
                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 value-col">
                    <a href="/proizvoditel/{!! $product->manufacturer->alias !!}" class="value">{!! $product->manufacturer->title !!}</a>
                </div>
            </div>
        @endif
        @if(!empty($categories))
        @php
            $categoryWord = (count($categories) > 1) ? 'Категории' : 'Категория';
        @endphp
                <div class="info-row-light">
                    <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5 key">{!! $categoryWord !!} товара:</div>
                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 value-col">

                        @php $comma = ' ,'; $totalCategories = count($categories); @endphp
                        @for($i = ($totalCategories-1); $i >= 0; $i--)
                            @php if(!$i) $comma = ''; @endphp
                            <a href="{!! $categories[$i]->link !!}" class="value cat-link">{!! $categories[$i]->title !!}</a>{!! $comma !!}
                        @endfor

                    </div>
                </div>
        @endif
        @if(count($product->codes) > 1)
            <div class="info-row-light">
                <div class="col-xs-12 col-sm-5 col-md-5 key "><span class="red">Список артикулов запчасти:</span></div>
                <div class="col-xs-12 col-sm-7 col-md-7 value-col">
                    @if(!empty($product->codes))
                        @php $comma = ' ,'; $totalCodes = count($product->codes); @endphp
                        @for($i = ($totalCodes-1); $i >= 0; $i--)
                            @php if(!$i) $comma = ''; @endphp
                            <span class="value cat-link"><h2 class="value inline-block" >{!! $product->codes[$i] !!}</h2></span>{!! $comma !!}
                        @endfor

                    @endif
                </div>
            </div>
        @endif

        
        <div class="invisible-delimeter"></div>
        <div class="invisible-delimeter"></div>

    </div>
</div>
