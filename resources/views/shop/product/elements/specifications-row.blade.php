@if(@$tabs['specifications-row']->display)
    <div class="parts represent product-information">
    <div class="row">
       @foreach($product->data as $rowK => $dataRow)
            <div class="col-xs-12 info-row">
                <div class="col-xs-12 col-sm-6 key">@if(isset($specificationTranslations[$rowK])){!! $specificationTranslations[$rowK] !!}@else{!! $rowK !!}@endif:</div>
                <div class="col-xs-12 col-sm-6 value-col value">{!! $dataRow !!}</div>
            </div>
       @endforeach
       </div>
    </div>
@endif