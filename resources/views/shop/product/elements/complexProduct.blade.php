<div class="complex-product" @if(!$complexProduct) id="complex-product-prototype" @endif>
    <div class="complex-product-avatar">
        @if($link)<a href="{!! @$complexProduct->link !!}">@endif{!! \Html::productThumbnail(@$complexProduct->avatar) !!}@if($link)</a>@endif
    </div>
    <div class="complex-product-title-wrapper">
        @if($link)<a href="{!! @$complexProduct->link !!}">@endif<span class="complex-product-title">{!! @$complexProduct->title !!}</span>@if($link)</a>@endif
    </div>
</div>