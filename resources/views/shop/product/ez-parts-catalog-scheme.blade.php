@extends('layouts.front')
@section('htmlClass') class="product-page" @endsection
@if(@$product->avatar)
    @section('header')
        @parent
        <script src="/js/libs/jquery.elevatezoom.js"></script>
    @endsection
@endif
@section('page')
    @php
        $product->data = 0;
        calculateProductPrice($product);
        $tabs = [
           'information-row' => (object)['id' => 'information-row', 'class' => '', 'display' => 1, 'title' => 'О Товаре'],
           'specifications-row' => (object)['id' => 'specifications-row', 'class' => '', 'title' => 'Спецификации'],
           'parts-row' => (object)['id' => 'parts-row', 'class' => '', 'title' => 'Схема'],
           'guarantee-row' => (object)['id' => 'guarantee-row', 'class' => '', 'display' => 1, 'title' => 'Гарантии и условия']
        ];
        $active = 0;

        if(!empty($product->child_products)) {
            $active = 'parts-row';
        } else {
            if(!empty($product->parent_products)) {
               $active = 'parts-of-another-row';
            }
        }

        if(!empty($product->child_products)) { $tabs['parts-row']->display = 1; }
        if(!empty($product->parent_products)) { $tabs['parts-of-another-row']->display = 1; }
        if(!empty($product->data)) { $tabs['specifications-row']->display = 1; }
        $active = $active ? $active : 'information-row';
        $tabs[$active]->class = 'active';
    @endphp
    <div id="product-page" class="ez-parts-catalog-page">
        <h1>{!! $product->title !!}</h1>
        <div class="box">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="relative">
                        <input type="hidden" value="{!! $product->product_id !!}" />
                        @if($product->priceinfo['discount'])
                            <div class="discount-corner">-{!! $product->priceinfo['discount_percent'] !!}%</div>
                        @endif
                        @if(@$product->avatar)
                        <div class="slider-heading">Наведите курсор на изображение чтобы увеличить <button type="submit" onclick="Actions.imgPrint()"><i class="mdi mdi-printer"></i> распечатать</button></div>

                        @endif
                            <div id="slider" class="slider">
                                {!! \Html::productPicture($product->avatar, 'id="avatar"  data-zoom-image="'.\Html::productPicturePath($product->avatar).'"') !!}
                            </div>
                        @if(@$product->avatar)
                            <script>
                            /*
                                    $('#avatar').elevateZoom({
                                        zoomType: "inner",
                                        cursor: "crosshair"
                                    });
                                    */
                            </script>
                        @endif
                        <div class="payment-info">
                            <div class="payment-info-title green-btn">Все платежи через наш сайт без дополнительных комиссий</div>
                            <div class="payment-info-body">
                                <img src="/images/payments/payment_methods_Yandex_1.png" />
                            </div>
                        </div>
                    </div>
                        @include('shop.product.elements.description-row')
                        <div id="parts-row">
                            @include('shop.product.elements.parts-row')
                        </div>


                </div>
                <div class="col-xs-12 col-md-4">

                    <div class="row">
                        @include('shop.product.elements.specials')
                    </div>
                </div>

            </div>
        </div>
    </div>


    @if($product->priceinfo['compare'])
        @include('elements.shop.promotion-discount')
    @include('elements.shop.buy-in-click')
    @endif

@endsection

