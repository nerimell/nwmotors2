@extends('layouts.front')
@section('htmlClass') class="product-page" @endsection
@section('page')
    @php
         calculateProductPrice($product);
         $tabs = [
            'information-row' => (object)['id' => 'information-row', 'class' => '', 'display' => 1, 'title' => 'О Товаре'],
            'specifications-row' => (object)['id' => 'specifications-row', 'class' => '', 'title' => 'Спецификации'],
            'parts-row' => (object)['id' => 'parts-row', 'class' => '', 'title' => 'Комплектация'],
            'parts-of-another-row' => (object)['id' => 'part-of-another-row', 'class' => '', 'title' => 'Применимость'],
            'guarantee-row' => (object)['id' => 'guarantee-row', 'class' => '', 'display' => 1, 'title' => 'Гарантии и условия']
         ];
         $active = 0;

         if(!empty($product->child_products)) {
             $active = 'parts-row';
         } else {
             if(!empty($product->parent_products)) {
                $active = 'parts-of-another-row';
             }
         }

         if(!empty($product->child_products)) { $tabs['parts-row']->display = 1; }
         if(!empty($product->parent_products)) { $tabs['parts-of-another-row']->display = 1; }
         if(!empty($product->data->specifications)) { $tabs['specifications-row']->display = 1; }
         $active = $active ? $active : 'information-row';
         $tabs[$active]->class = 'active';
    @endphp
    <div id="product-page">
        <h1>{!! $product->title !!}</h1>
        <div class="box">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="relative">
                    <input type="hidden" value="{!! $product->product_id !!}" />
                        @if($product->priceinfo['discount'])
                            <div class="discount-corner">-{!! $product->priceinfo['discount_percent'] !!}%</div>
                        @endif
                        <div id="slider" class="slider">
                                {!! \Html::productPicture($product->avatar, 'id="avatar"') !!}
                        </div>
                        @php $product->images = \json_decode($product->images); //
                        @endphp
                        @if(!empty($product->images) && count($product->images) > 1)
                        <div class="images-list-wrapper">
                            <i class="mdi mdi-chevron-left slide-action slide-left" onclick="slider.slideLeft()"></i>
                            <div class="images-list">
                            <div  id="images-wrapper" class="images-container">
                                <div class="image-wrapper">{!! \Html::productThumbnail($product->avatar, 'onclick="slider.setAvatar(this)", data-source="'.\Html::productPicturePath($product->avatar).'"') !!}</div>
                                @foreach($product->images as $img)
                                    <div class="image-wrapper">{!! \Html::productThumbnail($img, 'onclick="slider.setAvatar(this)", data-source="'.\Html::productPicturePath($img).'"') !!}</div>
                                @endforeach

                                </div>
                            </div>
                            <i class="mdi mdi-chevron-right slide-action slide-right" onclick="slider.slideRight()"></i>
                        </div>
                        @endif
                        <div class="payment-info">
                          <div class="payment-info-title green-btn">Все платежи через наш сайт без дополнительных комиссий</div>
                          <div class="payment-info-body">
                            <img src="/images/payments/payment_methods_Yandex_1.png" />
                          </div>
                        </div>
                    </div>
                    <div class="product-tabs-wrapper panel-border">
                    <ul class="nav nav-tabs">
                        @php // @$product->data->specifications = 1; @$product->parts = 1; @$partOfAnother = 1; @endphp
                        @foreach($tabs as $tab)
                            @if(@$tab->display)
                            <li class="{!! $tab->class !!}"><a data-toggle="tab" href="#{!! $tab->id !!}">{!! $tab->title !!}</a></li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        <div id="information-row" class="tab-pane fade in {!! $tabs['information-row']->class !!}">
                            @include('shop.product.elements.information-row')
                            @include('shop.product.elements.description-row')
                        </div>
                        <div id="specifications-row" class="tab-pane fade in {!! $tabs['specifications-row']->class !!}">
                            @include('shop.product.elements.specifications-row')
                        </div>
                        <div id="parts-row" class="tab-pane fade in {!! $tabs['parts-row']->class !!}">
                            @include('shop.product.elements.parts-row')
                        </div>
                        <div id="part-of-another-row" class="tab-pane fade in {!! $tabs['parts-of-another-row']->class !!}">
                            @include('shop.product.elements.part-of-another-row')
                        </div>
                        <div id="guarantee-row" class="tab-pane fade in {!! $tabs['guarantee-row']->class !!}">
                            @include('shop.product.elements.guarantee-row')
                        </div>
                    </div>
                    </div>


                </div>
                <div class="col-xs-12 col-md-4">
                    @if(!empty($product->codes))
                        <div class="barcode-row">
                            {!! \Html::productBarcode($product->codes[0].'.png') !!}
                        </div>
                    @endif
                    @include('shop.product.elements.add-to-cart')
                    <div class="row">
                        @include('shop.product.elements.specials')
                    </div>
                </div>

                </div>
            </div>
        </div>


        <div style="margin-top: 350px;">

        </div>
        @if($product->priceinfo['compare'])
        @include('elements.shop.promotion-discount')
        @include('elements.shop.buy-in-click')
        @endif

@endsection
@include('shop.product.elements.scripts')
