@extends('layouts.front')
@section('htmlClass') class="order-page" @endsection
@section('page')
    @php
        $info = [
            (object)['key' => 'Статус заказа', 'value' => @$orderStatuses[$order->status]->title],
        ];
        if($order->completeinfo->totals->coupon_discount) {
            $info[] = (object)['key' => 'Код купона', 'value' => number_format($order->coupon_code)];
        }
        $info[] = (object)['key' => 'Всего товаров', 'value' => $order->completeinfo->totals->instances];
        if($order->completeinfo->totals->gifts) {
            $info[] = (object)['key' => 'Всего подарков', 'value' => number_format($order->completeinfo->totals->gifts).' шт.'];
        }
        if($order->status) {
            $info[] = (object)['key' => 'Способ оплаты', 'value' => @$paymentMethods[$order->payment_method]];
        }
    @endphp
    <h1>Заказ #{!! $order->order_id !!}</h1>
    @if($message)
        <div class="success-notification">{!! $message !!}</div>
    @endif
    <div class="panel-border m-b-25">
        <div class="col-xs-12">
            <div class="order-details-title">Общая информация:</div>
            <div class="order-details-tip">
                <div>Заказ сформирован. Цены и подарки зафиксированы.</div>
                @if(!$order->status)
                    <div>Сформированный неоплаченный заказ действителен до {!! \DateFormatter::datePlus($order->created_at, 72) !!}</div>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="row">
                @foreach($info as $x)
                    @include('shop.orders.info-row')
                @endforeach
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="price">
                <div class="row">
                    @if($order->completeinfo->totals->final_discount)
                        <div>
                            <div class="col-xs-5 text-right order-details-key m-b-0">Цена без скидки:</div>
                            <div class="col-xs-7">
                                <div class="simple-price">
                                    <div class="discounted">{!! number_format($order->completeinfo->totals->base_price, 0, '', ' ') !!}
                                        р.
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @php
                        $priceText = $order->completeinfo->totals->final_discount ? 'цена со скидкой' : 'окончательная цена';
                    @endphp
                    <div class="new-price">
                        <div class="col-xs-5 text-right order-details-key m-b-0">{!! $priceText !!}:</div>
                        <div class="col-xs-7">
                            <div class="discount-price">{!! number_format($order->completeinfo->totals->final_price, 0, '', ' ') !!}
                                р.
                            </div>
                        </div>
                    </div>
                    @if($order->completeinfo->totals->final_discount)
                        <div>
                            <div class="col-xs-5 text-right order-details-key m-b-0">Скидка:</div>
                            <div class="col-xs-7">
                                <div class="discount">{!! number_format($order->completeinfo->totals->final_discount, 0, '', ' ') !!}
                                    р.
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xs-12 m-b-10">
            @if(!$order->status)
                <div class="invisible-delimeter"></div>
                <div class="invisible-delimeter"></div>
                <div class="text-right">
                    @if($allowChangeOrder)
                    <div class="inline-block">
                            <a href="/order/change/{!! $order->hash !!}" class="green-btn btn-xl w-auto inline-block link-align-button">Изменить</a>
                    </div>
                    @endif
                    <button type="button" class="green-btn btn-xl w-auto inline-block" onclick="makePayment()">Оплатить</button>
                </div>
            @endif
        </div>
    </div>

    <div class="panel-border m-b-25">
        <div class="col-xs-12">
            <div class="order-details-title">Информация о доставке:</div>
        </div>
        <div class="col-xs-12 col-md-8">
            <div class="row">
                @php
                    $info = [
                        (object)['key' => 'Получатель', 'value' => $order->userinfo->last_name . ' '. $order->userinfo->first_name . ' ' .$order->userinfo->third_name],
                        (object)['key' => 'Телефон для связи', 'value' => '+7 '.$order->userinfo->phone],
                        (object)['key' => 'Почтовый индекс', 'value' => $order->userinfo->post_index],
                    ];
                    foreach($info as $x) {
                @endphp
                @include('shop.orders.info-row')
                @php } @endphp
                <div class="invisible-delimeter"></div>
                @php
                    $info = [];
                    $info[] = (object)['key' => 'Доставить в город', 'value' => $order->city_to->title];
                    if($order->userinfo->address) {
                         $info[] = (object)['key' => 'Доставить по адресу', 'value' => $order->userinfo->address];
                    }
                    $info[] = (object)['key' => 'Способ доставки', 'value' => $order->deliveryinfo];
                foreach($info as $x) {
                @endphp
                @include('shop.orders.info-row')
                @php } @endphp
                @if($order->userinfo->user_type == 1)
                    <div class="invisible-delimeter"></div>

                @endif
                <div class="invisible-delimeter"></div>


            </div>
        </div>
    </div>


    <div class="panel-border">
        <div class="col-xs-12">
            <div class="invisible-delimeter"></div>
        </div>
        <div class="col-xs-12">
            <table class="grid-table">
                <thead>
                <th class="checkout-avatar">Товар</th>
                <th class="checkout-title">Название</th>
                <th class="checkout-quantity">Кол-во</th>
                <th class="checkout-price">Цена</th>
                <th class="checkout-total">Сумма</th>
                </thead>
                <tbody id="cart-products-table">
            <?php
            $totalGifts = 0;
            $totalInstances = count($order->completeinfo->instances);
            for($i = 0; $i < $totalInstances; $i++) {
            if (!empty($order->completeinfo->instances[$i]->gifts)) {
                foreach ($order->completeinfo->instances[$i]->gifts as &$x) {
                    $x->avatar = \Html::productThumbnailPath($x->avatar);
                    handleProductLink($x);
                    calculateProductPrice($x);
                }
            }
            if(@$order->completeinfo->instances[$i]->t == 'c') {
            ?>
            @include('shop.orders.complex-row', ['instance' => $order->completeinfo->instances[$i]])
            <?php continue; } ?>
            @include('shop.orders.product-row', ['instance' => $order->completeinfo->instances[$i]])
            <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12">
            <div class="fl w-100">
                @if(!$order->status)
                    <div class="text-right">
                        <button type="button" class="green-btn btn-xl w-auto inline-block" onclick="makePayment()" id="make-payment">Оплатить</button>
                    </div>
                @endif
            </div>
            <div class="invisible-delimeter"></div>
        </div>
    </div>
    @if(!$order->status)
        <script>
            var orderPaymentUrl;
            var newWindow;
            function getPaymentUrl(callback) {
                $.ajax({
                    url: '/order/getPaymentUrl',
                    type: 'post',
                    data: { hash: '<?php echo $order->hash; ?>' },
                    dataType: 'json',
                    success: function(db) {
                        if(db.result) {
                            orderPaymentUrl = db.link;
                            if(callback) { callback(db.link); }
                        } else {
                            Actions.error(db.display);
                        }
                    }
                });
            }
            function makePayment() {
                newWindow = window.open('', '_blank');
                newWindow.document.title = 'Оплата заказа на сайте <?php echo $website->title; ?>';
                getPaymentUrl(redirectToPayment);
            }

            function redirectToPayment(link) {
               newWindow.location.href = link;
               // a.remove();
            }
        </script>
        @endif
@endsection
