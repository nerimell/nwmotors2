@php
    $discount = number_format(@$instance->priceinfo->discount_value*@$instance->q);
@endphp
<tr  id="instance-{!! $instance->product_id !!}" data-id="{!! $instance->product_id !!}" data-type="p"  @if(!empty($instance->gifts)) data-gifts="1" @else data-gifts="0" @endif>
    <td class="checkout-avatar">
        <input type="hidden" class="product-title-input" value="{!! $instance->title !!}" />
        <a href="{!! @$instance->link !!}" class="complex-product-avatar-link">{!! \Html::productThumbnail(@$instance->avatar) !!}</a>
        <span class="hidden-all product-data-json">{!! \json_encode(@$instance->priceinfo) !!}</span>
    </td>
    <td class="checkout-title">
        <p><a class="product_title" href="{!! @$instance->link !!}">{!! @$instance->title !!}</a></p>
        @if($instance->gifts)
            <span class="hidden-all product-gifts-json"><?php echo \json_encode(@$instance->gifts); ?></span>
            <span class="hidden-all product-gifts-values-json">{!! \json_encode($instance->cartGifts) !!}</span>
            <div class="checkout-product-gifts-info"><span class="checkout-products-gifts-count">{!! $instance->q !!}</span> <span class="checkout-products-gifts-word">{!! rusificate($instance->q, 'подарок', 'подарка', 'подарков') !!}</span></div>
        @php $totalGifts+= $instance->q; @endphp
        @endif
    </td>
    <td class="checkout-quantity">
        <div class="quantity">
            <span class="">{!! @$instance->q !!}</span>
        </div>
    </td>
    <td class="checkout-price">
        <div class="discount-info-row" @if(!$discount) style="display: none; "@endif>
        <p>
            <span class="product-total-no-discount product-base-price no-discount-line">
                {!! number_format(@$instance->priceinfo->price) !!}
            </span> р.
        </p>
        </div>
        <span class="final-price">{!! @number_format(@$instance->priceinfo->final_price) !!}</span> р.
    </td>
    <td class="checkout-total">
            <div class="discount-info-row"  @if(!$discount) style="display: none;" @endif>
            <p class="no-discount-line">
                <span class="product-total-no-discount total-base-price">
                    {!! number_format(@$instance->priceinfo->price*@$instance->q) !!}
                </span> р.
            </p>
            </div>
            <p><span class="final-price total-final-price">{!! @number_format(@$instance->priceinfo->final_price*@$instance->q) !!}</span> р.</p>
    </td>
</tr>
