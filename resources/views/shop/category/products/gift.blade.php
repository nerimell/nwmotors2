@if(@$product->gifts->maxGiftPrice)
    <div class="category-gift-wrapper">
        <div class="gift-notice">
            <div class="category-gift-notice-text">подарок на</div>
            <div class="category-gift-notice-price-wrapper">
            <span class="category-gift-notice-price">{!! number_format($product->gifts->products[0]->priceinfo['final_price']) !!}</span> р.
            </div>
        </div>
        <div class="category-gift-overlay">
            @if(@$product->gifts->products[0]->avatar)
                {!! \Html::productThumbnail(@$product->gifts->products[0]->avatar) !!}
            @else
                <div class="category-gift-price-wrapper">
                    <span class="category-gift-price">{!! @$product->gifts->products[0]->priceinfo['final_price'] !!}</span> р.
                </div>
            @endif
        </div>
    </div>
@endif