@php
    $titlelength = mb_strlen($product->title);
    if(@$product) {
        calculateProductPrice($product);
    }
@endphp
<div class="product-wrapper">
    @if($product->priceinfo['discount'])
        <div class="discount-corner">-{!! $product->priceinfo['discount_percent'] !!}%</div>
    @endif
    <a href="{!! $product->link !!}" class="category-avatar-link">
        @include('shop.category.products.gift')
        <div class="avatar">
            @if($product->avatar)
            {!! \Html::productThumbnail($product->avatar) !!}
            @else
            <img src="{!! defaultImage !!}" />
            @endif
        </div>
    </a>
    <div class="product-name-wrapper">
    <a href="{!! $product->link !!}">
        @if($titlelength > 65)
            <h2 title="{!! $product->title !!}">{!! mb_substr($product->title, 0, 65) !!} ...</h2>
        @else
            <h2>{!! $product->title !!}</h2>
        @endif
    </a>
    </div>

</div>