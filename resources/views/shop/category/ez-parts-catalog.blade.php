@extends('layouts.front')
@section('page')
   <div id="category-page" class="ez-parts-catalog">
   <h1>{!! $category->title !!}</h1>
   @if($category->description)
   <div class="short-description">{!! $category->description !!}</div>
   @endif
   @if($category->childs)
   <div class="categories-list">
   <div class="row row-eq-height">
         @foreach($category->childs as $x)
            <div class="col-xs-12 col-sm-6 col-md-4">
               @include('shop.category.elements.child-category-mini')
            </div>
         @endforeach
       </div>
       </div>
   @endif
   @if($total)
      <div class="border-row back-row">
         <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
               Всего @if($category->childs) товаров @endif: {!! $total !!}
            </div>
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
               @include('elements.pagination', ['baseUrl' => $category->link])
            </div>
         </div>
      </div>
      <div class="products-list">
         @if($products->count())
         <div class="row row-eq-height">
             <?php include('../resources/views/elements/handleProducts.blade.php'); ?>
         @foreach($products as $product)
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
               @include('elements.shop.product')
            </div>
         @endforeach
         </div>
         @else
         <div class="err">Нет товаров удовлетворяющих вашему запросу</div>
         @endif
      </div>
         <div class="border-row border-top">
            <div class="row">
               <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
                  Всего @if($category->childs) товаров @endif: {!! $total !!}
               </div>
               <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
                  @include('elements.pagination', ['baseUrl' => $category->link])
               </div>
            </div>
         </div>
   @else
    @if(!$category->childs)
      <div class="err">Извените, у нас сейчас нет товаров в этой категории</div>
      @endif
   @endif
   </div>
@endsection
