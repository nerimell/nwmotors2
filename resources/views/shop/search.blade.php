@extends('layouts.front')
@section('page')
   <div id="category-page">
   <h1>Результаты поиска</h1>
   @if($total)
      <div class="border-row back-row">
         <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
               Всего: {!! $total !!}
            </div>
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
               @include('elements.pagination', ['baseUrl' => '/search'])
            </div>
         </div>
      </div>
      <div class="products-list">
         @if($products->count())
              <?php include('../resources/views/elements/handleProducts.blade.php'); ?>
         <div class="row row-eq-height">
         @foreach($products as $product)
            <div class="col-xs-12 col-sm-6 col-md-4">
               @include('elements.shop.product')
            </div>
         @endforeach
         </div>
         @else
         <div class="err">Нет товаров удовлетворяющих вашему запросу</div>
         @endif
      </div>
         <div class="border-row border-top">
            <div class="row">
               <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
                  Всего: {!! $total !!}
               </div>
               <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
                  @include('elements.pagination', ['baseUrl' => '/search'])
               </div>
            </div>
         </div>
   @else
      <div class="err">Не найдено результатов удовлетворяющих вашему запросу</div>
   @endif
   </div>
@endsection
