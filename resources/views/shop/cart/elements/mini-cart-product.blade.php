@php
    $titlelength = mb_strlen(@$x->title);
    if($titlelength > 45) {
        $title = mb_substr(@$x->title, 0, 47).'...';
    } else {
        $title = @$x->title;
    }
@endphp
<div class="cart-product">
    <div class="cart-product-avatar">
        <a class="link" href="{!! @$x->link !!}">{!! \Html::productThumbnail(@$x->avatar) !!}</a>
        @if(@$x->priceinfo['discount_percent'] || $x->gifts)
        @php
            if($x->priceinfo['discount_percent']) {
                $giftTitle = '+ <i class="mdi mdi-gift"></i>';
                $giftClass = '';
            } else {
                $giftTitle = '+ подарок';
                $giftClass = 'mini-cart-gift-icon-2';
            }
        @endphp
            <div class="cart-product-percent-sale">@if($x->priceinfo['discount_percent']) -{!! $x->priceinfo['discount_percent'] !!} % @endif @if($x->gifts) <span class="mini-cart-gift-icon {!! $giftClass !!}">{!! $giftTitle !!}</span> @endif </div>
        @endif
    </div>
    <div class="cart-product-title-wrapper"><a href="{!! @$x->link !!}" class="link cart-product-title">{!! $title !!}</a></div>
    <div class="cart-product-quantity-wrapper">
    <div class="product-quantity-span">
        <div><span class="cart-product-price">{!! number_format(@$x->priceinfo['final_price']) !!}</span> x <span class="cart-product-quantity">{!! @$x->q !!}</span></div>
        <div><span class="cart-product-total">{!! number_format(@$x->priceinfo['final_price']*@$x->q) !!}</span>{!! $cart->currency->sign !!}</div>
    </div>
    </div>
    <div class="cart-product-actions-wrapper">
        <i class="mdi mdi-delete delete-product" onclick="Actions.cart.deleteFromCart(<?php echo @$x->product_id; ?>, '', this)"></i>
    </div>
</div>