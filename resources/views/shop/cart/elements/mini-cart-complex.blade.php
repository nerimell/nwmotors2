<div class="cart-complex {!! $x->productsCount !!}-items">
    <div class="cart-complex-avatar">
        @foreach($x->products as $complexProduct)
            <div class="cart-product-avatar">
                <a class="link" href="{!! $complexProduct->link !!}">{!! \Html::productThumbnail($complexProduct->avatar) !!}</a>
            </div>
        @endforeach
    </div>
    <div class="cart-product-title-wrapper cart-complex-title-wrapper">
        <span class="link cart-product-title"><span class="orange">Комплект: </span>{!! $x->title !!}</span>
    </div>
    <div class="cart-product-quantity-wrapper">
        <div class="product-quantity-span">
            <div><span class="cart-product-price">{!! number_format($x->priceinfo['final_price']) !!}</span> x <span class="cart-product-quantity">{!! $x->q !!}</span></div>
            <div><span class="cart-product-total">{!! number_format($x->priceinfo['final_price']*$x->q) !!}</span>{!! $cart->currency->sign !!}</div>
        </div>
    </div>
    <div class="cart-product-actions-wrapper">
        <i class="mdi mdi-delete delete-product" onclick="Actions.cart.deleteFromCart(<?php echo $x->complex_id; ?>, '<?php echo $x->t; ?>', this)"></i>
    </div>
</div>