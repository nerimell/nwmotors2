@php
    $discount = number_format(@$instance->priceinfo['discount_value']*@$instance->q);
@endphp
<tr id="complex-{!! @$instance->complex_id !!}" data-type="c" data-id="{!! @$instance->complex_id !!}" class="complex-row instance-tr">
    <td class="checkout-avatar">
        <input type="hidden" class="product-title-input" value="{!! $instance->title !!}" />
        @foreach($instance->products as $x)
        <a href="{!! @$x->link !!}">{!! \Html::productThumbnail(@$x->avatar) !!}</a>
        @endforeach
        <span class="hidden-all product-data-json" id="product-data-{!! @$instance->product_id !!}">{!! \json_encode(@$instance->priceinfo) !!}</span>
    </td>
    <td class="checkout-title">
        @foreach($instance->products as $x)
            <a class="product_title complex-checkout-title" href="{!! $x->link !!}">
                {!! $x->title !!}
            </a>
        @endforeach
    </td>
    <td class="checkout-quantity">
        <div class="quantity">
            <input class="quantity-control"
                   onkeypress="return CartObject.changeQuantityAsync(event, this)" data-last="{!! @$instance->q !!}"
                   value="{!! @$instance->q !!}" maxlength="2"
                   type="text"/>
            <i class="mdi mdi-menu-up plus"
               onclick="CartObject.quantityPlus(this);"></i>
            <i class="mdi mdi-menu-down minus"
               onclick="CartObject.quantityMinus(this)"></i>
            <input type="hidden" class="product_id" value="{!! @$instance->product_id !!}"/>
        </div>

    </td>
    <td class="checkout-price">
            <div class="discount-info-row" @if(!$discount) style="display: none;" @endif>
            <p>
                <span class="product-total-no-discount product-base-price no-discount-line">
                    {!! number_format(@$instance->priceinfo['price']) !!}
                </span> р.
            </p>
            </div>
        <span class="final-price">{!! @number_format(@$instance->priceinfo['final_price']) !!}</span> р.
    </td>
    <td class="checkout-total">
            <div class="discount-info-row" @if(!$discount) style="display: none;" @endif>
            <p class="no-discount-line">
                <span class="product-total-no-discount total-base-price">
                    {!! number_format(@$instance->priceinfo['price']*@$instance->q) !!}
                </span> р.
            </p>
            </div>
        <p><span class="final-price total-final-price">{!! @number_format(@$instance->priceinfo['final_price']*@$instance->q) !!}</span> р.</p>
    </td>
    <td class="checkout-delete-col">
        <i class="mdi mdi-close delete-product" onclick="Actions.cart.deleteFromCart(<?php echo @$instance->complex_id; ?>, '<?php echo @$instance->t; ?>', this)"></i>
    </td>
</tr>
