<div class="checkout-deliveries">
    <h4 class="blue-skew">Способы доставки</h4>
    <div id="deliveries">
        <div id="deliveries-inner">
            @include('shop.checkout.deliveries-inner')
        </div>
    </div>
</div>