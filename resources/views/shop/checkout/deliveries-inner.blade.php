<div class="col-xs-12 col-md-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pick.png') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Самовывоз</div>
            <span class="sidebar-delivery-price" id="free-self-pick"></span>
        </div>
        <input type="radio" name="delivery" value="9" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="free-self-pick-input" />
        <label for="free-self-pick-input"></label>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="sidebar-delivery-row">
        <div id="free-delivery-container" style="display: none;">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/free.png') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Бесплатная доставка</div>
            <span class="sidebar-delivery-price" id="free-terminal-truck"></span>
        </div>
        <input type="radio" name="delivery" value="1" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="free-terminal-truck-input" />
        <label for="free-terminal-truck-input"></label>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pecom.jpg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Доставка до терминала</div>
            <span class="sidebar-delivery-price" id="pecom-terminal-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="2" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="pecom-terminal-truck-input" />
        <label for="pecom-terminal-truck-input"></label>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pecom.jpg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Доставка до двери</div>
            <span class="sidebar-delivery-price" id="pecom-door-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="3" onchange="CartObject.changeDelivery(this)" data-requireaddress="1" class="radio-anchor"  id="pecom-door-truck-input" />
        <label for="pecom-door-truck-input"></label>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/pecom.jpg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Авиадоставка до терминала</div>
            <span class="sidebar-delivery-price" id="pecom-terminal-avia"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="4" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="pecom-terminal-avia-input" />
        <label for="pecom-terminal-avia-input"></label>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/jd.jpg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Доставка до терминала</div>
            <span class="sidebar-delivery-price" id="jd-terminal-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="5" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="jd-terminal-truck-input" />
        <label for="jd-terminal-truck-input"></label>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/bl.svg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Доставка до терминала</div>
            <span class="sidebar-delivery-price" id="bl-terminal-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="6" onchange="CartObject.changeDelivery(this)" class="radio-anchor"  id="bl-terminal-truck-input" />
        <label for="bl-terminal-truck-input"></label>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="sidebar-delivery-row">
        <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('deliveries/bl.svg') !!}</div>
        <div class="sidebar-delivery-text">
            <div>Доставка до двери</div>
            <span class="sidebar-delivery-price" id="bl-door-truck"><img src="/images/preloader.gif" /></span><span class="sidebar-delivery-price-row" style="display: none;"> р.</span>
        </div>
        <input type="radio" name="delivery" value="8" onchange="CartObject.changeDelivery(this)" class="radio-anchor" data-requireaddress="1"  id="bl-door-truck-input" />
        <label for="bl-door-truck-input"></label>
    </div>
</div>
<div class="col-xs-12 m-t-15">
    <div class="checkout-delivery-desc">
        <div>Стоимость доставки является ориентировочной. Точную стоимость можно уточнить у нашего менеджера</div>
        @if($website->freeDeliveryLimit)<div>При покупке на сумму <span class="highlight-color">{!! $website->freeDeliveryLimit !!} р.</span> мы доставим товар бесплатно (выберите бесплатный способ доставки)</div>@endif
        <div>Доставка оплачивается отдельно</div>
    </div>
</div>