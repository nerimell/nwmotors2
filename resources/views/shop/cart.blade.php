@extends('layouts.front')
@section('htmlClass') class="no-sidebar-page cart-page" @endsection
@section('page')

    @if(@$allowChangeOrder)
        <input type="hidden" id="order-id" value="{!! $order->hash !!}" />
    @endif
            <div class="row">
                <div id="products-list">
                    <div class="col-xs-12 col-md-8 col-lg-9">
                    <div id="checkout-form">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" id="step-1-btn" href="#products-table-tab"><span class="step-number">1</span>Товары к оплате</a></li>
                            <li><a data-toggle="tab" id="step-2-btn" href="#check-tab"><span class="step-number">2</span>Данные для доставки</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="products-table-tab" class="tab-item tab-pane fade in active">
                                <table class="grid-table">
                                    <thead>
                                    <th class="checkout-avatar">Товар</th>
                                    <th class="checkout-title">Название</th>
                                    <th class="checkout-quantity">Кол-во</th>
                                    <th class="checkout-price">Цена</th>
                                    <th class="checkout-total">Сумма</th>
                                    <th class="checkout-delete-col"></th>
                                    </thead>
                                    <tbody id="cart-products-table">
                                        @include('elements.shop.cart-page-inner')
                                    </tbody>
                                </table>
                                <div class="text-right m-t-30">
                                    <button type="button" class="green-btn btn-xl w-auto inline-block" onclick="CartObject.step('step-2-btn')">Оформить заказ</button>
                                </div>
                            </div>
                            <div id="check-tab" class="tab-item tab-pane fade">
                                <div class="col-xs-12"><h4 class="blue-skew">Персональные данные</h4></div>
                            <div class="userform">
                                <form method="post" id="userform"  onsubmit="return CartObject.checkout(this)" data-valid="0">
                                @include('users.fields')
                                </form>
                                <div class="col-xs-12">
                                    <div><h4 class="blue-skew">Примечания</h4></div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <textarea name="comment" class="form-control" style="height: 122px;"></textarea>
                                        </div>
                                        <div class="invisible-delimeter"></div>
                                        <div class="invisible-delimeter"></div>
                                        <div class="invisible-delimeter"></div>
                                        <div class="invisible-delimeter"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    @include('shop.checkout.deliveries')
                                </div>
                                <div class="col-xs-12">
                                    <div><h4 class="blue-skew">Способ оплаты</h4></div>
                                    <div class="row">
                                    <form onsubmit="return false;" id="select-payment-form">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="sidebar-delivery-row">
                                            <div id="free-delivery-container">
                                                <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('payments/receip-payment.png') !!}</div>
                                                <div class="sidebar-delivery-text">
                                                    <div>Счет квитанция</div>
                                                    <span class="sidebar-delivery-price" id="free-terminal-truck"></span>
                                                </div>
                                                <input type="radio" name="payment" value="1" class="radio-anchor"  id="payment-1" />
                                                <label for="payment-1"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="sidebar-delivery-row">
                                            <div id="free-delivery-container">
                                                <div class="sidebar-delivery-row-avatar">{!! \Html::systemPicture('payments/cards-payment.png') !!}</div>
                                                <div class="sidebar-delivery-text">
                                                    <div>Безналичный рассчет</div>
                                                    <span class="sidebar-delivery-price" id="free-terminal-truck"></span>
                                                </div>
                                                <input type="radio" name="payment" checked value="0"  class="radio-anchor"  id="payment-0" />
                                                <label for="payment-0"></label>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <div class="invisible-delimeter"></div>
                                    <div class="invisible-delimeter"></div>
                                        <div class="invisible-delimeter"></div>
                                        <div class="invisible-delimeter"></div>
                                    </div>
                                </div>

                                <div class="text-right col-xs-12">
                                    <button type="button" class="green-btn btn-xl w-auto inline-block" onclick="CartObject.checkAnOrder()">Перейти к оплате</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <div id="final-checkout" class="checkout-desk sidebar-box silver-border sidebar-box-small">
                            <div class="widget-title-white">
                                <div class="slide-from-right-el">Данные заказа</div>
                            </div>
                            <div class="widget-body">
                            <div id="cart-page-side">
                                @include('elements.shop.cart-page-side')
                            </div>
                                <div class="checkout-cart-payment-methods">
                                    <img src="/images/payments/all-alt-a.png"/>
                                    <div class="checkout-cart-payment-notice checkout-calculator-title f-s-13">комиссии
                                        платежных систем мы оплатим сами!
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


    <div class="prototype">
        <div id="gift-prototype" class="checkout-modal-gift">
            <div class="checkout-modal-gift-avatar"><a
                        class="checkout-modal-gift-link">{!! \Html::productThumbnail(0) !!}</a></div>
            <div class="checkout-modal-gift-title-wrapper">
                <div class="inline-text-wrapper">
                    <a class="checkout-modal-gift-title checkout-modal-gift-link"></a>
                </div>
            </div>
            <div class="checkout-modal-gift-quantity-wrapper">
                <div class="checkout-quantity">
                    <div class="quantity">
                        <input class="quantity-control" disabled onkeypress="return validateNumericInput(event)"
                               onkeyup="CartObject.changeQuantity(event, this)" value="" maxlength="2" type="text">
                        <i class="mdi mdi-menu-up plus" onclick="CartObject.giftQuantityPlus(this)"></i>
                        <i class="mdi mdi-menu-down minus" onclick="CartObject.giftQuantityMinus(this)"></i>
                    </div>
                </div>
            </div>
            <div class="checkout-modal-gift-price">
                <p><span class="product-total-no-discount product-base-price"></span> р.</p>
            </div>
        </div>
    </div>
    <div id="gifts-dialog" class="modal dialog-style-one dialog-style-one-l fade" data-hashid="" data-maxgifts="">
        <div class="modal-dialog">
            <div class="modal-inner">
                <div class="modal-body">
                    <div class="modal-close-btn" data-dismiss="modal"><i class="mdi mdi-close-circle"></i></div>
                    <div class="checkout-modal-gift" id="checkout-modal-product">
                        <div class="checkout-modal-gift-avatar"><a
                                    class="checkout-modal-gift-link">{!! \Html::productThumbnail(0) !!}</a></div>
                        <div class="checkout-modal-gift-title-wrapper">
                            <div class="inline-text-wrapper">
                                <a class="checkout-modal-gift-title checkout-modal-gift-link"></a>
                            </div>
                        </div>
                        <div class="checkout-modal-gift-special-line">Акция! Вы можете выбрать подарки к товару</div>
                    </div>
                    <div class="checkout-modal-gift-desc text-right">Вы можете выбрать <span id="modal-desc-gifts-count"></span></div>
                    <div id="gifts-wrapper">

                    </div>
                    <div class="error-container"></div>
                    <div class="text-right">
                        <button type="button" class="btn-xl w-auto green-btn inline-block" onclick="CartObject.saveGiftQuantity()">изменить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" class="hidden-all" id="order-form" action="/order/formed">
        <input type="hidden" name="order_hash" id="order-hash" />
    </form>
@endsection
@section('post-scripts')
    <script src="/js/checkout.js"></script>
@endsection