@extends('layouts.front')
@section('page')
@php $user->contact = $additionalData; @endphp
    <h1>Личный кабинет</h1>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#general-info">Информация об аккаунте</a></li>
            <li><a data-toggle="tab" href="#contact-info" onclick="clearPwdContainer()">Контактные данные</a></li>
        </ul>
        <div class="tab-content">
            <div id="general-info" class="tab-item tab-pane fade in active">
                <form action="/profile" method="post" class="userform" onsubmit="return saveUserSettings(this)">
            <div class="row profile-row-a">
                <div class="col-xs-12 col-md-6">
                    <div class="input-group with-icon m-b-15">
                        <label class="control-label">Логин</label>
                        <input type="text" name="username" disabled class="form-control" placeholder="" value="{!! $user->username !!}"/>
                    </div>
                    <div class="input-group with-icon m-b-15">
                        <label class="control-label">Емайл</label>
                        <input type="text" name="email" disabled class="form-control" placeholder="" value="{!! $user->email !!}"/>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <button type="submit" class="btn btn-xl green-btn inline-btn w100">Сохранить</button>
                        </div>
                    <div class="col-xs-12 col-md-7" id="pwd-btn-wrapper">
                        <button type="button" data-a="Оставить прежний пароль" id="pwd-btn" data-b="Сменить пароль" class="btn btn-xl gr-blue-bg w100" onclick="togglePassword()">Сменить пароль</button>
                    </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div id="password-placeholder">

                    </div>
                </div>
                </div>
                </form>
            </div>
            <div id="contact-info" class="tab-item tab-pane fade in">
                <form action="/profile" method="post" class="userform" onsubmit="return saveUserSettings(this)">
                <input type="hidden" name="extra" value="1" />
                <div class="row">
                @include('users.fields')
                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-xl green-btn inline-btn w100">Сохранить</button>
                </div>
                </div>
                </form>
            </div>
        </div>


<div id="password-prototype" class="invisible">
    <input type="hidden" name="reset_password" value="1" />
    <div class="input-group with-icon m-b-15">
        <label class="control-label">Старый пароль</label>
        <input type="password" name="old_password" class="form-control" placeholder=""
               value=""/>
    </div>
    <div class="input-group with-icon m-b-15">
        <label class="control-label">Новый пароль</label>
        <input type="password" name="password" class="form-control" placeholder=""
               value=""/>
    </div>
    <div class="input-group with-icon m-b-15">
        <label class="control-label">Подтвердите пароль</label>
        <input type="password" name="password2" class="form-control" placeholder=""
               value=""/>
    </div>
</div>

    <script>
        function saveUserSettings(form) {
            $.ajax({
                url: form.action,
                data: $(form).serialize(),
                type: 'post',
                dataType: 'json',
                success: function (db) {
                    if (db.result) {
                        Actions.message(db.display, 4000, 'success-toast');
                        $('.city-container').html(db.city.title);
                    } else {
                        Actions.error(db.display);
                    }

                }
            });
            return false;
        }


        var passwordState = 0;
        var placeholder = $('#password-placeholder');
        var pwdButton = document.getElementById('pwd-btn');
        function togglePassword() {
            return passwordState ? clearPwdContainer() : addPwdContainer();
        }

        function addPwdContainer() {
            var el = $('#password-prototype').clone();
            el.attr('id', '');
            el.removeClass('invisible');
            placeholder.append(el);
            pwdButton.innerHTML = pwdButton.dataset.a;
            passwordState = 1;
        }

        function clearPwdContainer() {
            placeholder.html('');
            pwdButton.innerHTML = pwdButton.dataset.b;
            passwordState = 0;
        }
    </script>
@stop