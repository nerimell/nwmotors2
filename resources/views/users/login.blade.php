@extends('layouts.front')
@section('page')
    <h1>Авторизация</h1>
    <form method="post" action="/reg" onsubmit="return Actions.user.auth.login(event, this)">
        <div class="row">
            <div class="col-xs-12">
                <div class="input-group with-icon m-b-15">
                    <div class="col-xs-12"><label class="control-label">Логин</label></div>
                    <div class="col-xs-12 col-md-6 col-lg-5">
                        <input type="text" name="username" class="form-control" placeholder=""/>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-6 col-lg-7">

                    </div>
                </div>
                <div class="input-group with-icon m-b-15">
                    <div class="col-xs-12"><label class="control-label">Пароль</label></div>
                    <div class="col-xs-12 col-md-6 col-lg-5">
                        <input type="password" id="pwd" name="password" class="form-control" placeholder=""/>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-6 col-lg-7">

                    </div>
                </div>
                <div class="error-container col-xs-12 col-md-6 col-lg-5"></div>
                <div class="input-group with-icon m-b-15 text-right">
                    <div class="col-xs-12 col-md-6 col-lg-5">
                        <button type="submit" class="btn btn-blue btn-lg w100">Войти</button>
                    </div>
                </div>
                <div class="text-right col-xs-12 col-md-6 col-lg-5 auth-helpers">
                <div>
                    <a href="/restore-login" class="pull-left">Забыли логин?</a>
                    <a class="pull-right" href="/registration">Нет аккаунта?</a>
                </div>
                <div>
                    <a href="/restore-password" class="pull-left">Забыли пароль?</a>
                </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">

            </div>
        </div>
    </form>
@endSection