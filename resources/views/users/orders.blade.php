@extends('layouts.front')
@section('page')
    <h1>Мои заказы</h1>
    @if(!$orders->total)
        <div class="err">У вас пока нет ни одного заказа</div>
    @else
        @if($orders->total > $orders->params['perPage'])
            <div class="border-row back-row">
                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
                        @include('elements.pagination', ['baseUrl' => '/my/orders/', 'perPage' => $orders->params['perPage'], 'total' => $orders->total])
                    </div>
            </div>
         @endif
        @foreach($orders->list as $order)
            <div class="order-container">
                <a href="/order/{!! $order->hash !!}">
                <div class="order-avatar">
                    {!! \Html::productThumbnail($order->avatar); !!}
                </div>
                </a>
                <div class="order-data">
                    <div class="order-title"><a href="/order/{!! $order->hash !!}">Заказ #{!! $order->order_id !!}</a></div>
                    <div class="order-details">
                        <span>
                            <i class="mdi mdi-clock i-right"></i>{!! \DateFormatter::reformatDate($order->created_at) !!}
                        </span>
                    </div>
                    <div class="order-priceinfo">
                        <div class="price">
                            <div class="row">
                                @if($order->completeinfo->totals->final_discount)
                                    <div>
                                        <div class="simple-price">
                                            <div class="discounted">{!! number_format($order->completeinfo->totals->base_price, 0, '', ' ') !!}
                                                    р.
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="new-price">
                                        <div class="discount-price">{!! number_format($order->completeinfo->totals->final_price, 0, '', ' ') !!}
                                            р.
                                        </div>
                                </div>
                                @if($order->completeinfo->totals->final_discount)
                                    <div>
                                            <div class="discount">{!! number_format($order->completeinfo->totals->final_discount, 0, '', ' ') !!}
                                                р.
                                            </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
     @endif
@endSection