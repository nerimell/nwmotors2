
    <div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Фамилия</label>
            <input type="text" name="last_name" class="form-control validator remember" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
                   value="{!! @$userFields->last_name !!}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Имя</label>
            <input type="text" name="first_name" class="form-control validator remember" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
                   value="{!! @$userFields->first_name !!}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Отчество</label>
            <input type="text" name="third_name" class="form-control validator remember" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
                   value="{!! @$userFields->third_name !!}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Телефон</label>
            <div class="phone-wrapper">
            <input type="text" name="phone" class="form-control validator phone remember" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-phone="1"
                   value="{!! @$userFields->phone !!}"/>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Город</label>
            <div>
                <button type="button" class="form-control select-city-form-btn" data-callback="{!! $dialogCallback !!}" onclick="Actions.location.changeLocationInForm(this)">
                    <div class="city-container inline-block">{!! $userFields->city->title !!}</div>
                    <input type="hidden" name="city" class="hidden-city" id="fields-city" value="{!! $userFields->city->city_id !!}" />
                    <i class="mdi mdi-pencil-circle"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Адрес доставки</label>
            <input type="text" name="address" class="form-control validator remember" @if($require) data-required="1" @endif id="address-field" placeholder="" onkeyup="validator.validateField(this)" data-min="7" data-blockeng="1"
                   value="{!! @$userFields->address !!}"/>
        </div>
    </div>
    @if($require)
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Емайл</label>
            <input type="text" name="email" class="form-control validator remember"  data-email="1" onkeyup="validator.validateField(this)" onchange="validator.validateField(this)" @if($require) data-required="1" @endif  data-min="4" data-max="35"  maxlength="35" placeholder=""
                   value="{!! @$userFields->email !!}"/>
        </div>
    </div>
    @endif
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Почтовый индекс</label>
            <input type="text" name="post_index" class="form-control validator remember" onkeypress="return validateNumericInput(event)" @if($require) data-required="1" @endif onkeyup="validator.validateField(this)" data-min="6" data-max="6" data-numeric="1" maxlength="6" placeholder=""
                   value="{!! @$userFields->post_index !!}"/>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Тип аккаунта</label>
            <select name="user_type" class="form-control select-validator remember" id="usertype" onchange="Actions.user.changeUserType(this)">
                @foreach($usertypes as $k => $x)
                    <option value="{!! $k !!}" @if(@$userFields->user_type == $k) selected @endif>{!! $x->title !!}</option>
                @endforeach
            </select>
        </div>
    </div>
    </div>
    @foreach($usertypes as $k => $v)
        <div class="usertype-block @if(@$userFields->user_type == $k) active @endif" id="usertype-{!! $k !!}">
                @if(!empty($usertypes[$k]->fields))
                    @foreach($usertypes[$k]->fields as $field)
                        <div class="{!! $field->wrap !!}">
                            <div class="input-group with-icon m-b-15">
                                <label class="control-label">{!! $field->title !!}</label>
                                @if($field->type == 'text')
                                    <input type="text" @if($require) data-required="1" @endif name="{!! $field->name !!}" class="form-control validator remember"
                                     <?php if(!empty($field->dataset)) { foreach($field->dataset as $k => $dataset) { ?>
                                      data-{!! $k !!}="{!! $dataset !!}"<?php } } ?>
                                     onkeyup="validator.validateField(this)" placeholder=""
                                     <?php if(!empty($field->dataset['numeric'])) { ?> onkeypress="return validateNumericInput(event)" <?php } ?>
                                     <?php if(!empty($field->dataset['max'])) { ?> maxlength="{!! $field->dataset['max'] !!}" <?php } ?>
                                           value="{!! @$userFields->{$field->name} !!}"/>
                                @elseif($field->type == 'select')
                                    <select name="{!! $field->name !!}" class="form-control remember">
                                        @foreach($field->values as $valK => $val)
                                            <option value="{!! $valK !!}">{!! $val !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
        </div>
@endforeach

<script src="/js/masked_input.min.js"></script>
<script>
  $('.phone').mask("(999) 999-99-99");
  $('.validator').each(function () {
    validator.validateField(this);
  });
</script>
