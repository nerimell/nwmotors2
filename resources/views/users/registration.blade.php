@extends('layouts.front')
@section('page')
    <h1>Регистрация</h1>
    <form method="post" action="/reg" onsubmit="return Actions.user.auth.registration(event, this)">
    <div class="row">
    <div class="col-xs-12 col-md-5">
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Логин</label>
            <input type="text" name="username" class="form-control" placeholder="" />
        </div>
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Емайл</label>
            <input type="text" name="email" class="form-control" placeholder="" />
        </div>
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Пароль</label>
            <input type="password" id="pwd" name="password" class="form-control" placeholder="" />
        </div>
        <div class="input-group with-icon m-b-15">
            <label class="control-label">Подтвердите пароль</label>
            <input type="password" id="confirm-pwd" name="password2" class="form-control" placeholder="" />
        </div>

    </div>
    <div class="col-xs-12">
        <div class="input-group m-b-15">
            <input type="checkbox" name="confirm" id="confirm-terms" />
            <label for="confirm-terms"></label>
            <span>Я согласен с <button type="button" class="button-link" onclick="termsOfUse()">правилами пользования</button> сайтом</span>
        </div>
        <div class="error-container"></div>
        <div>
            <button type="submit" class="btn btn-blue btn-lg">Зарегистрироваться</button>
        </div>
    </div>
    </div>
    </form>
    <div id="termsModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-inner">
                <div class="modal-header">Правила пользования сайтом {!! $website->title !!}</div>
                <div class="modal-body">{!! $terms !!}</div>
            </div>
        </div>
    </div>
    <script>
        var termsModal = $('#termsModal');
        function termsOfUse() {
            termsModal.modal('show');
        }
    </script>


@endSection