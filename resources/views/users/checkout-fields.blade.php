<div class="input-group with-icon m-b-15">
    <label class="control-label checkout-calculator-title">Телефон</label>
    <div class="phone-wrapper slight-fc-pre">
        <input type="text" name="phone" class="form-control slight-fc validator phone" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-phone="1"
               value="{!! @$user->phone !!}"/>
    </div>
</div>
<div class="input-group with-icon m-b-15">
    <label class="control-label checkout-calculator-title">Имя</label>
    <input type="text" name="first_name" class="form-control slight-fc validator" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
           value="{!! @$user->first_name !!}"/>
</div>
<div class="input-group with-icon m-b-15">
    <label class="control-label checkout-calculator-title">Отчество</label>
    <input type="text" name="third_name" class="form-control slight-fc validator" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
           value="{!! @$user->contact->third_name !!}"/>
</div>
<div class="input-group with-icon m-b-15">
    <label class="control-label checkout-calculator-title">Фамилия</label>
    <input type="text" name="last_name" class="form-control slight-fc validator" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="2" data-rus="1"
                   value="{!! @$user->last_name !!}"/>
</div>








        <div class="input-group with-icon m-b-15">
            <label class="control-label checkout-calculator-title">Город</label>
            <div>
            <button type="button" class="form-control slight-fc select-city-form-btn" onclick="Actions.location.changeLocationInForm(this)">
            {!! \Html::$city->title !!}
            <i class="mdi mdi-pencil-circle"></i>
            </button>
            </div>
        </div>

        <div class="input-group with-icon m-b-15">
            <label class="control-label checkout-calculator-title">Адрес</label>
            <input type="text" name="address" class="form-control slight-fc validator" @if($require) data-required="1" @endif placeholder="" onkeyup="validator.validateField(this)" data-min="7" data-blockeng="1"
                   value="{!! @$user->contact->address !!}"/>
        </div>


        <div class="input-group with-icon m-b-15">
            <label class="control-label checkout-calculator-title">Почтовый индекс</label>
            <input type="text" name="post_index" class="form-control slight-fc validator" onkeyup="validator.validateField(this)" data-min="6" data-numeric="1" maxlength="7" placeholder=""
                   value="{!! @$user->contact->post_index !!}"/>
        </div>

        <div class="input-group with-icon m-b-15">
            <label class="control-label checkout-calculator-title">Тип аккаунта</label>
            <select name="user_type" class="form-control slight-fc" id="usertype" onchange="Actions.user.changeUserType(this)">
                @foreach($usertypes as $k => $x)
                    <option value="{!! $k !!}" @if(@$user->user_type == $k) selected @endif>{!! $x->title !!}</option>
                @endforeach
            </select>
        </div>

    @foreach($usertypes as $k => $v)
        <div class="usertype-block @if(@$user->user_type == $k) active @endif" id="usertype-{!! $k !!}">
                @if(!empty($usertypes[$k]->fields))
                    @foreach($usertypes[$k]->fields as $field)
                        <div class="{!! $field->wrapMini !!}">
                            <div class="input-group with-icon m-b-15">
                                <label class="control-label checkout-calculator-title">{!! $field->title !!}</label>
                                @if($field->type == 'text')
                                    <input type="text" name="{!! $field->name !!}" class="form-control slight-fc validator" <?php if(!empty($field->dataset)) { foreach($field->dataset as $k => $dataset) { ?> data-{!! $k !!}="{!! $dataset !!}"<?php } } ?> onkeyup="validator.validateField(this)" placeholder=""
                                           value="{!! @$user->contact->{$field->name} !!}"/>
                                @elseif($field->type == 'select')
                                    <select name="{!! $field->name !!}" class="form-control slight-fc">
                                        @foreach($field->values as $valK => $val)
                                            <option value="{!! $valK !!}">{!! $val !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
        </div>
@endforeach
<script src="/js/masked_input.min.js"></script>
<script>
  $('.phone').mask("(999) 999-99-99");
  $('.validator').each(function () {
    validator.validateField(this);
  });
</script>
