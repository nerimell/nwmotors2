<?php
use App\Models\SettingsModel;
$sm = new SettingsModel;
$website = $sm->getSiteData();
$website->meta_title = 404 . ' - ' .$website->title;
?>
@extends('layouts.app')
@section('htmlClass') class="error-page" @endsection
@section('header')
    @parent
    <link rel="stylesheet" href="/css/app.css"/>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/application.min.js"></script>
@stop
@section('body')
    <body>
        @yield('page')
    </body>
@endsection
