<!DOCTYPE html>
<html @yield('htmlClass')>
<head>
  @include('scripts.gtag')
  <meta charset="utf-8">
  <title>{!! $website->meta_title !!}</title>
  <meta name="description" content="{!! $website->meta_description !!}">
  <meta name="keywords" content="{!! $website->meta_keywords !!}">
  <link rel="stylesheet" href="/css/materialdesignicons.min.css" type="text/css">
  <script src="/js/libs/jquery.min.js"></script>
  @yield('header')
</head>
  @yield('body')

</html>
