@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="/css/app.css"/>
    <script src="/js/application.min.js"></script>
@stop
@section('body')
    <body>
    <div class="container content-wrapper">
        <div class="row">
            @include('elements.header')
        </div>
    </div>
    <div class="container content-wrapper" id="page-wrapper">
        <div class="row">
            <div class="col-xs-12  col-lg-3 side-column">
              <div class="offset-group">
                 @include('elements.sidebar')
              </div>
            </div>
            <div class="col-xs-12  col-lg-9 main-column">
              <div class="row">
              @include('elements.menu')
            </div>
                <div id="page">
                    @include('elements.breadcrumbs')
                    @yield('page')
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    @include('elements.footer')
                </div>
            </div>
        </div>
    </div>
    @if(!logged)
    @include('elements.auth-dialog')
    @endif
    @include('elements.city-dialog')
    @yield('post-scripts')
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/libs/toastr.js"></script>
    <script src="/js/libs/jquery-confirm.min.js"></script>
    <div id="global-preloader"><img src="/images/loader-2.gif" /></div>
    @include('scripts.yandex-counter')
    @include('scripts.all')
    <script src="/js/footer.js"></script>
    </body>
@stop
