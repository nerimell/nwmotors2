@extends('layouts.app')
@section('header')
  <link rel="stylesheet" href="/css/admin/app.css" />
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/application.min.js"></script>
@stop

@section('body')
  <body>
  @include('admin.elements.sidebar')
  <div id="page">
    @include('admin.elements.header')
    <div class="container">
      <div class="col-xs-12">
        <div  id="breadcrumbs">
          @include('admin.elements.breadcrumbs')
        </div>
      </div>
      <div id="content">
        page is here
      </div>
    </div>
  </div>
  </body>
@stop

