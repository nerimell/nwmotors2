<div id="city-dialog" class="modal dialog-style-one dialog-style-one-l fade">
    <div class="modal-dialog">
        <div class="modal-inner">
            <div class="modal-header">Выберите ваш город</div>
            <div class="modal-body">
                <div class="cities-list">
                                                                                                <div class="col-xs-12 col-md-4">
                    <ul class="city-column">
                                                <li  class="office"  data-id="1"
                            onclick="Actions.location.selectCityFromList()">
                            Москва
                        </li>
                                                                <li  class="office"  data-id="173"
                            onclick="Actions.location.selectCityFromList()">
                            Санкт-Петербург
                        </li>
                                                                <li  data-id="346"
                            onclick="Actions.location.selectCityFromList()">
                            Астрахань
                        </li>
                                                                <li  data-id="227"
                            onclick="Actions.location.selectCityFromList()">
                            Барнаул
                        </li>
                                                                <li  data-id="543"
                            onclick="Actions.location.selectCityFromList()">
                            Волгоград
                        </li>
                                                                <li  data-id="609"
                            onclick="Actions.location.selectCityFromList()">
                            Воронеж
                        </li>
                                                                <li  data-id="1921"
                            onclick="Actions.location.selectCityFromList()">
                            Екатеринбург
                        </li>
                                                                <li  data-id="2295"
                            onclick="Actions.location.selectCityFromList()">
                            Ижевск
                        </li>
                                                                <li  data-id="733"
                            onclick="Actions.location.selectCityFromList()">
                            Иркутск
                        </li>
                                                                <li  data-id="2090"
                            onclick="Actions.location.selectCityFromList()">
                            Казань
                        </li>
                                                                                </ul>
                </div>
                                <div class="col-xs-12 col-md-4">
                    <ul class="city-column">
                                                <li  data-id="773"
                            onclick="Actions.location.selectCityFromList()">
                            Калининград
                        </li>
                                                                <li  data-id="1042"
                            onclick="Actions.location.selectCityFromList()">
                            Краснодар
                        </li>
                                                                <li  data-id="1116"
                            onclick="Actions.location.selectCityFromList()">
                            Красноярск
                        </li>
                                                                <li  data-id="2102"
                            onclick="Actions.location.selectCityFromList()">
                            Набережные Челны
                        </li>
                                                                <li  data-id="1342"
                            onclick="Actions.location.selectCityFromList()">
                            Нижний Новгород
                        </li>
                                                                <li  data-id="1406"
                            onclick="Actions.location.selectCityFromList()">
                            Новосибирск
                        </li>
                                                                <li  data-id="1438"
                            onclick="Actions.location.selectCityFromList()">
                            Омск
                        </li>
                                                                <li  data-id="1474"
                            onclick="Actions.location.selectCityFromList()">
                            Оренбург
                        </li>
                                                                <li  data-id="1576"
                            onclick="Actions.location.selectCityFromList()">
                            Пермь
                        </li>
                                                                <li  data-id="1705"
                            onclick="Actions.location.selectCityFromList()">
                            Ростов-на-Дону
                        </li>
                                                                                </ul>
                </div>
                                <div class="col-xs-12 col-md-4">
                    <ul class="city-column">
                                                <li  data-id="1775"
                            onclick="Actions.location.selectCityFromList()">
                            Самара
                        </li>
                                                                <li  data-id="1819"
                            onclick="Actions.location.selectCityFromList()">
                            Саратов
                        </li>
                                                                <li  data-id="2039"
                            onclick="Actions.location.selectCityFromList()">
                            Ставрополь
                        </li>
                                                                <li  data-id="1779"
                            onclick="Actions.location.selectCityFromList()">
                            Тольятти
                        </li>
                                                                <li  data-id="2217"
                            onclick="Actions.location.selectCityFromList()">
                            Тула
                        </li>
                                                                <li  data-id="2275"
                            onclick="Actions.location.selectCityFromList()">
                            Тюмень
                        </li>
                                                                <li  data-id="2337"
                            onclick="Actions.location.selectCityFromList()">
                            Ульяновск
                        </li>
                                                                <li  data-id="414"
                            onclick="Actions.location.selectCityFromList()">
                            Уфа
                        </li>
                                                                <li  data-id="2419"
                            onclick="Actions.location.selectCityFromList()">
                            Челябинск
                        </li>
                                                                <li  data-id="2532"
                            onclick="Actions.location.selectCityFromList()">
                            Ярославль
                        </li>
                                    </div>
            </div>
            <div class="cities-description-wrapper">
                <span><i class="mdi mdi-map-marker"></i> маркером отметкой отмечены города с нашими складами</span>
            </div>
            <div class="cities-selector-wrapper">
                <div class="cities-selector">
                    <div class="cities-select-wrapper">
                        <select onchange="Actions.location.changeRegion(this)">
                                                            <option value="1">Московская область</option>
                                                            <option value="2">Ленинградская область</option>
                                                            <option value="3">Адыгея</option>
                                                            <option value="4">Алтайский край</option>
                                                            <option value="5">Амурская область</option>
                                                            <option value="6">Архангельская область</option>
                                                            <option value="7">Астраханская область</option>
                                                            <option value="8">Башкортостан</option>
                                                            <option value="9">Белгородская область</option>
                                                            <option value="10">Брянская область</option>
                                                            <option value="11">Бурятия</option>
                                                            <option value="12">Владимирская область</option>
                                                            <option value="13">Волгоградская область</option>
                                                            <option value="14">Вологодская область</option>
                                                            <option value="15">Воронежская область</option>
                                                            <option value="16">Дагестан</option>
                                                            <option value="17">Еврейская область</option>
                                                            <option value="18">Ивановская область</option>
                                                            <option value="19">Иркутская область</option>
                                                            <option value="20">Кабардино-Балкария</option>
                                                            <option value="21">Калининградская область</option>
                                                            <option value="22">Калмыкия</option>
                                                            <option value="23">Калужская область</option>
                                                            <option value="24">Камчатская область</option>
                                                            <option value="25">Карелия</option>
                                                            <option value="26">Кемеровская область</option>
                                                            <option value="27">Кировская область</option>
                                                            <option value="28">Коми</option>
                                                            <option value="29">Костромская область</option>
                                                            <option value="30">Краснодарский край</option>
                                                            <option value="31">Красноярский край</option>
                                                            <option value="32">Курганская область</option>
                                                            <option value="33">Курская область</option>
                                                            <option value="34">Липецкая область</option>
                                                            <option value="35">Магаданская область</option>
                                                            <option value="36">Марий Эл</option>
                                                            <option value="37">Мордовия</option>
                                                            <option value="38">Мурманская область</option>
                                                            <option value="39">Нижегородская область</option>
                                                            <option value="40">Новгородская область</option>
                                                            <option value="41">Новосибирская область</option>
                                                            <option value="42">Омская область</option>
                                                            <option value="43">Оренбургская область</option>
                                                            <option value="44">Орловская область</option>
                                                            <option value="45">Пензенская область</option>
                                                            <option value="46">Пермский край</option>
                                                            <option value="47">Приморский край</option>
                                                            <option value="48">Псковская область</option>
                                                            <option value="49">Ростовская область</option>
                                                            <option value="50">Рязанская область</option>
                                                            <option value="51">Самарская область</option>
                                                            <option value="52">Саратовская область</option>
                                                            <option value="53">Саха Якутия</option>
                                                            <option value="54">Сахалин</option>
                                                            <option value="55">Свердловская область</option>
                                                            <option value="56">Северная Осетия</option>
                                                            <option value="57">Смоленская область</option>
                                                            <option value="58">Ставропольский край</option>
                                                            <option value="59">Тамбовская область</option>
                                                            <option value="60">Татарстан</option>
                                                            <option value="61">Тверская область</option>
                                                            <option value="62">Томская область</option>
                                                            <option value="63">Тува Тувинская Респ.</option>
                                                            <option value="64">Тульская область</option>
                                                            <option value="65">Тюменская область и Ханты-Мансийский АО</option>
                                                            <option value="66">Удмуртия</option>
                                                            <option value="67">Ульяновская область</option>
                                                            <option value="68">Уральская область</option>
                                                            <option value="69">Хабаровский край</option>
                                                            <option value="70">Хакасия</option>
                                                            <option value="71">Челябинская область</option>
                                                            <option value="72">Чечено-Ингушетия</option>
                                                            <option value="73">Читинская область</option>
                                                            <option value="74">Чувашия</option>
                                                            <option value="75">Чукотский АО</option>
                                                            <option value="76">Ямало-Ненецкий АО</option>
                                                            <option value="77">Ярославская область</option>
                                                            <option value="78">Карачаево-Черкесская Республика</option>
                                                    </select>
                    </div>
                    <div class="cities-select-wrapper">
                        <select id="cities-list-select">
                            <option value="1">Москва</option>
    <option value="2">Абрамцево</option>
    <option value="3">Алабино</option>
    <option value="4">Апрелевка</option>
    <option value="5">Архангельское</option>
    <option value="6">Ашитково</option>
    <option value="7">Байконур</option>
    <option value="8">Бакшеево</option>
    <option value="9">Балашиха</option>
    <option value="10">Барыбино</option>
    <option value="11">Белозёрский</option>
    <option value="12">Белоомут</option>
    <option value="13">Белые Столбы</option>
    <option value="14">Бородино</option>
    <option value="15">Бронницы</option>
    <option value="16">Быково</option>
    <option value="17">Валуево</option>
    <option value="18">Вербилки</option>
    <option value="19">Верея</option>
    <option value="20">Видное</option>
    <option value="21">Внуково</option>
    <option value="23">Волоколамск</option>
    <option value="24">Вороново</option>
    <option value="25">Воскресенск</option>
    <option value="26">Восточный</option>
    <option value="27">Востряково</option>
    <option value="28">Высоковск</option>
    <option value="29">Голицыно</option>
    <option value="30">Деденево</option>
    <option value="31">Дедовск</option>
    <option value="32">Дзержинский</option>
    <option value="33">Дмитров</option>
    <option value="34">Долгопрудный</option>
    <option value="35">Домодедово</option>
    <option value="36">Дорохово</option>
    <option value="37">Дрезна</option>
    <option value="38">Дубки</option>
    <option value="39">Дубна</option>
    <option value="40">Егорьевск</option>
    <option value="41">Железнодорожный</option>
    <option value="42">Жилево</option>
    <option value="43">Жуковка</option>
    <option value="44">Жуковский</option>
    <option value="45">Загорск</option>
    <option value="46">Загорянский</option>
    <option value="47">Запрудная</option>
    <option value="48">Зарайск</option>
    <option value="49">Звенигород</option>
    <option value="50">Зеленоград</option>
    <option value="51">Ивантеевка</option>
    <option value="52">Икша</option>
    <option value="53">Ильинский</option>
    <option value="54">Истра</option>
    <option value="55">Калининец</option>
    <option value="56">Кашира</option>
    <option value="57">Керва</option>
    <option value="58">Климовск</option>
    <option value="59">Клин</option>
    <option value="60">Клязьма</option>
    <option value="61">Кожино</option>
    <option value="62">Кокошкино</option>
    <option value="63">Коломна</option>
    <option value="64">Колюбакино</option>
    <option value="65">Королев</option>
    <option value="66">Косино</option>
    <option value="67">Котельники</option>
    <option value="68">Красково</option>
    <option value="69">Красноармейск</option>
    <option value="70">Красногорск</option>
    <option value="71">Краснозаводск</option>
    <option value="72">Краснознаменск</option>
    <option value="73">Красный Ткач</option>
    <option value="74">Крюково</option>
    <option value="75">Кубинка</option>
    <option value="76">Купавна</option>
    <option value="77">Куровское</option>
    <option value="78">Лесной Городок</option>
    <option value="79">Ликино-Дулево</option>
    <option value="80">Лобня</option>
    <option value="81">Лопатинский</option>
    <option value="82">Лосино-Петровский</option>
    <option value="83">Лотошино</option>
    <option value="84">Лукино</option>
    <option value="85">Луховицы</option>
    <option value="86">Лыткарино</option>
    <option value="87">Львовский</option>
    <option value="88">Люберцы</option>
    <option value="89">Малаховка</option>
    <option value="90">Михайловское</option>
    <option value="91">Михнево</option>
    <option value="92">Можайск</option>
    <option value="93">Монино</option>
    <option value="94">Московский</option>
    <option value="95">Муханово</option>
    <option value="96">Мытищи</option>
    <option value="97">Нарофоминск</option>
    <option value="98">Нахабино</option>
    <option value="99">Некрасовка</option>
    <option value="100">Немчиновка</option>
    <option value="101">Новобратцевский</option>
    <option value="102">Новоподрезково</option>
    <option value="103">Ногинск</option>
    <option value="104">Обухово</option>
    <option value="105">Одинцово</option>
    <option value="106">Ожерелье</option>
    <option value="107">Озеры</option>
    <option value="108">Октябрьский</option>
    <option value="109">Опалиха</option>
    <option value="110">Орехово-Зуево</option>
    <option value="111">Павловский Посад</option>
    <option value="112">Первомайский</option>
    <option value="113">Пески</option>
    <option value="114">Пироговский</option>
    <option value="115">Подольск</option>
    <option value="116">Полушкино</option>
    <option value="117">Правдинский</option>
    <option value="118">Привокзальный</option>
    <option value="119">Пролетарский</option>
    <option value="120">Протвино</option>
    <option value="121">Пушкино</option>
    <option value="122">Пущино</option>
    <option value="123">Раменское</option>
    <option value="124">Реутов</option>
    <option value="125">Решетниково</option>
    <option value="126">Родники</option>
    <option value="127">Рошаль</option>
    <option value="128">Рублево</option>
    <option value="129">Руза</option>
    <option value="130">Салтыковка</option>
    <option value="131">Северный</option>
    <option value="132">Сергиев Посад</option>
    <option value="133">Серебряные Пруды</option>
    <option value="134">Серпухов</option>
    <option value="135">Солнечногорск</option>
    <option value="136">Солнцево</option>
    <option value="137">Софрино</option>
    <option value="138">Старая Купавна</option>
    <option value="139">Старбеево</option>
    <option value="140">Ступино</option>
    <option value="141">Сходня</option>
    <option value="142">Талдом</option>
    <option value="143">Текстильщик</option>
    <option value="144">Темпы</option>
    <option value="145">Томилино</option>
    <option value="146">Троицк</option>
    <option value="147">Туголесский Бор</option>
    <option value="148">Тучково</option>
    <option value="149">Уваровка</option>
    <option value="150">Удельная</option>
    <option value="151">Успенское</option>
    <option value="152">Фирсановка</option>
    <option value="153">Фрязино</option>
    <option value="154">Фряново</option>
    <option value="155">Химки</option>
    <option value="156">Хотьково</option>
    <option value="157">Черкизово</option>
    <option value="158">Черноголовка</option>
    <option value="159">Черусти</option>
    <option value="160">Чехов</option>
    <option value="161">Шарапово</option>
    <option value="162">Шатура</option>
    <option value="163">Шатурторф</option>
    <option value="164">Шаховская</option>
    <option value="165">Шереметьевский</option>
    <option value="166">Щелково</option>
    <option value="167">Щербинка</option>
    <option value="168">Электрогорск</option>
    <option value="169">Электросталь</option>
    <option value="170">Электроугли</option>
    <option value="171">Юбилейный</option>
    <option value="172">Яхрома</option>
                        </select>
                    </div>
                    <button onclick="Actions.location.selectCity()" class="green-btn">Выбрать</button>
                </div>
            </div>
        </div>
    </div>
</div>